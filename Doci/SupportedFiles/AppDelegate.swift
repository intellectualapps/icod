//
//  AppDelegate.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import UIKit
let credentials = (
    appId: "rkDipdlRUCTl5Cru0IwX",
    appCode: "4uuzygisJUBFzh6hyJRBTg",
    licenseKey: "",
    freeRouteToolApiKey: "5b3ce3597851110001cf624800bdcbd07b0b45e187c8db2b1266b93a",
    googleDirectionApiKey: "AIzaSyAce1gsm9DAaFRtznEh4uB2fkIUalN09Rg",
    googlePlaceApiKey: "AIzaSyDv2bsKb1yERTyjf6KzVI1MtKT6xqV7Nzo"
)
let googleClientId = "205038902925-f4t2u0e597ibavrcke6mh0h630untnju.apps.googleusercontent.com"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?
    var services: Any?
    var loggedUser: Variable<User?> = Variable(nil)

    static let shared = UIApplication.shared.delegate as! AppDelegate
    /*
     Google client id: 205038902925-f4t2u0e597ibavrcke6mh0h630untnju.apps.googleusercontent.com
     */
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print(String.documentFolder())
        Defaults.alertSettingCount.set(value: 0)
        //Load custom font
        UIFont.loadCustomFont()
        GMSServices.provideAPIKey(credentials.googlePlaceApiKey)
        GMSPlacesClient.provideAPIKey(credentials.googlePlaceApiKey)
        services = GMSServices.sharedServices()
        //enable database
        DatabaseManager.shared.prepare()
        GIDSignIn.sharedInstance().clientID = googleClientId
        GIDSignIn.sharedInstance().delegate = self
        checkUserAndShowCorrectScreen()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        showMainScreen()
        loggedUser.value = User(name: "", email: "", password: "", token: "")
        CoreLocation.shared.currentLocation.asObservable()
            .subscribe(onNext: {[weak self] (location) in
                if let location = location {
                    self?.loggedUser.value?.location = location
                }
            }).disposed(by: rx.disposeBag)
        return true
    }

    func simulateAppRestart() {

    }
    
    func getStart() {

    }

    func logout() {
        self.loggedUser.value?.token = ""
        self.loggedUser.value = nil
        window?.rootViewController?.dismiss(animated: false, completion: nil)
        SideMenuManager.default.leftMenuNavigationController?.dismiss(animated: false, completion: nil)
        showAuthorize()
    }

    func checkUserAndShowCorrectScreen() {
        showMainScreen()
    }

    func showAuthorize() {
        let storyboard = UIStoryboard.authen()
        if let root = storyboard.instantiateInitialViewController() {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = root
            window?.makeKeyAndVisible()
        }
    }

    func showMainScreen() {
        CoreLocation.shared.getCurrentLocation {[weak self] (location) in
            if let location = location {
                self?.loggedUser.value?.location = location
            }
        }
        let storyboard = UIStoryboard.main()
        if let controller = storyboard.instantiateInitialViewController() {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let fb = ApplicationDelegate.shared.application(app, open: url, options: options) ?? true
        let gg = GIDSignIn.sharedInstance().handle(url as URL?,
                                                   sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                   annotation: options[UIApplication.OpenURLOptionsKey.annotation])

        return fb && gg
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            // ...
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {

    }
}

