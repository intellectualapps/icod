//
//  MonthInfo.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DayInfo;

@interface MonthInfo : NSObject
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSDate *monthDate;
@property (nonatomic, assign) NSInteger numberOfWeaks;
- (void)addDay:(DayInfo *)info;
- (NSArray *)listOfDays;
- (void)resetSelection;
- (void)resetContent;
- (void)autoSelectDates:(NSArray *)dates;
- (DayInfo *)firstDayInInMonth;
@end
