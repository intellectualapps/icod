//
//  DhtMessageNotificationView.h
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationProtocol.h"

@interface DhtMessageNotificationView : UIView
{
    
}
@property (nonatomic, strong) id <NotificationProtocol> object;
- (void) setMessageInfo:(id<NotificationProtocol>)object;
@end
