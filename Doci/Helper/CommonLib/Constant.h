//
//  Constant.h
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 9/7/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#define __SYNCHRONIZED_DEFINE(lock) pthread_mutex_t ____SYNCHRONIZED_##lock
#define __SYNCHRONIZED_INIT(lock) pthread_mutex_init(&____SYNCHRONIZED_##lock, NULL)
#define __SYNCHRONIZED_BEGIN(lock) pthread_mutex_lock(&____SYNCHRONIZED_##lock);
#define __SYNCHRONIZED_END(lock) pthread_mutex_unlock(&____SYNCHRONIZED_##lock);
#define __SYNCHRONIZED_INIT(lock) pthread_mutex_init(&____SYNCHRONIZED_##lock, NULL)
#define UIColorRGB(rgb) ([[UIColor alloc] initWithRed:(((rgb >> 16) & 0xff) / 255.0f) green:(((rgb >> 8) & 0xff) / 255.0f) blue:(((rgb) & 0xff) / 255.0f) alpha:1.0f])
#define UIColorRGBA(rgb,a) ([[UIColor alloc] initWithRed:(((rgb >> 16) & 0xff) / 255.0f) green:(((rgb >> 8) & 0xff) / 255.0f) blue:(((rgb) & 0xff) / 255.0f) alpha:a])
#define CGFloor floorf
#define UIColorRGB(rgb) ([[UIColor alloc] initWithRed:(((rgb >> 16) & 0xff) / 255.0f) green:(((rgb >> 8) & 0xff) / 255.0f) blue:(((rgb) & 0xff) / 255.0f) alpha:1.0f])
#define UIColorRGBA(rgb,a) ([[UIColor alloc] initWithRed:(((rgb >> 16) & 0xff) / 255.0f) green:(((rgb >> 8) & 0xff) / 255.0f) blue:(((rgb) & 0xff) / 255.0f) alpha:a])
#define CLCOORDINATES_EQUAL( coord1, coord2 ) (coord1.latitude == coord2.latitude && coord1.longitude == coord2.longitude)

//from driver
#define KNotificationWillGotoSettingAppFromLocationPermission @"KNotificationWillGotoSettingAppFromLocationPermission"
#define KNotificationtDidChangeMapDisplayType @"KNotificationtDidChangeMapDisplayType"
#define KNotificationDidSendLocationFailed @"KNotificationDidSendLocationFailed"
#define KNotificationDidCancelOrder @"KNotificationDidCancelOrder"
#define KNotificationDidUpdateLocation @"KNotificationDidUpdateLocation"
#define KNotificationDidUpdateLocationHeading @"KNotificationDidUpdateLocationHeading"
#define KNotificationUpdateOrderStatus @"KNotificationUpdateOrderStatus"
#define KNotificationNoteDidReceiveNewest @"KNotificationNoteDidReceiveNewest"

#define KNotificationDidUpdateProfile @"KNotificationDidUpdateProfile"
#define kNotificationEstimateOrderCompleted @"kNotificationEstimateOrderCompleted"
#define KNotificationDidCreateInqueryTopic @"KNotificationDidCreateInqueryTopic"
#define KNotificationUpdateInquiryUnreadCount @"KNotificationUpdateInquiryUnreadCount"
#define kNotiticationEditOrderCompleted @"kNotiticationEditOrderCompleted"
#define KNotificationShowErrorTypeVerification @"KNotificationShowErrorTypeVerification"

#define KNotificationDidReadInquiryChat @"KNotificationDidReadInquiryChat"
#define KNotificationCloseScreenIfNeed @"KNotificationCloseScreenIfNeed"
#define KNotificationUpdateImage @"KNotificationUpdateImage"
#define KNotificationNeedReloadUnavailableOrderList @"KNotificationNeedReloadUnavailableOrderList"
#define KNotificationNeedUpdateCalendar @"KNotificationNeedUpdateCalendar"
#define KNotificationChangeCalendarMode @"KNotificationChangeCalendarMode"
#define KNotificationNetworkDidChangeStatus @"KNotificationNetworkDidChangeStatus"
#define KNotificationSendUnreadPushCount @"KNotificationSendUnreadPushCount"
#define KNotificationUpdateUnreadMessageCount @"KNotificationUpdateUnreadMessageCount"
#define KNotificationNoteDidReceiveNewest @"KNotificationNoteDidReceiveNewest"
#define KNotificationStartSendLocation @"KNotificationStartSendLocation"
#define KNotificationStopSendLocation @"KNotificationStopSendLocation"
#define KNotificationDidReceiveMessagePush @"KNotificationDidReceiveMessagePush"
#define kNotificationDeleteOrderCompleted @"kNotificationDeleteOrderCompleted"
#define kNotificationLoginChangeState @"kNotificationLoginChangeState"
#define kNotificationDidReceiveRemoteNotificateion @"kNotificationDidReceiveRemoteNotificateion"
#define NotificationEstimateInfoDidChange @"NotificationEstimateInfoDidChange"
#define NotificationWillChangeCalendarMonthView @"NotificationWillChangeCalendarMonthView"
#define PasswordRegexAccept @"ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~\"#$%^'()=~|-\\@[;:],./`{+*}>?_ "
#define KatakanaFullSizeRegexAccept @"ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲンヴヵヶー"
#define KatakanaHalftSizeRegexAccept @"ｰ｡｢｣､･ｦｧｨｩｪｫｬｭｮｯﾀｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾐﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜﾝﾞﾟ"

#define default_avatar_witdh 300.0
