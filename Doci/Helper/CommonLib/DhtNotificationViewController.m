//
//  DhtNotificationViewController.m
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtNotificationViewController.h"
#import "DhtNotificationView.h"
#import <objc/runtime.h>
@interface DhtNotificationViewController ()
{
    
}

@end

@implementation DhtNotificationViewController

- (void) loadView {
    [super loadView];
    _notificationView = [[DhtNotificationView alloc]init];
    _notificationView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _notificationView.weakWindow = _weakWindow;
    [self.view addSubview:_notificationView];
}

- (DhtNotificationView *)notificationView {
    if (![self isViewLoaded]) {
        [self loadView];
    }
    return _notificationView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
