//
//  Message+CoreDataProperties.m
//  kids_taxi
//
//  Created by Nguyen Van Dung on 5/5/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Message+CoreDataProperties.h"

@implementation Message (CoreDataProperties)

@dynamic avatarUrlString;
@dynamic content;
@dynamic conversationId;
@dynamic createdDate;
@dynamic incoming;
@dynamic localIdentifier;
@dynamic messageId;
@dynamic modifiedDate;
@dynamic status;
@dynamic uid;
@dynamic firstName;
@dynamic lastName;
@dynamic orderId;
@end
