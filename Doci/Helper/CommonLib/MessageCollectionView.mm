//
//  MessageCollectionView.m
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "MessageCollectionView.h"
#import "DhtViewStorage.h"
#import <objc/message.h>
#import <map>
#import <set>
#import <algorithm>
#import "CollectionCell.h"
#import "MessageCollectionViewLayout.h"
#import "BaseView.h"
#import "ChatUtilities.h"
#import "DhtDoubleTapGesture.h"
#import "DhtDateHeaderView.h"

@interface MessageCollectionView()
{
    bool _delayVisibleItemsUpdate;
    float _lastRelativeBoundsReport;
    
    bool _disableDecorationViewUpdates;
    
    float _indicatorInset;
    DhtViewStorage      *_viewStorage;
    
    std::map<NSInteger, UIView<BaseViewProtocol> *> _currentVisibleDecorationViews;
}

@end

@implementation MessageCollectionView

- (id) initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        _indicatorInset = iosMajorVersion() >=7? (IsRetina() ? 7.5f : 8.0f) : 9.0f;
        _viewStorage = [[DhtViewStorage alloc] init];
        UIEdgeInsets contentInset = self.contentInset;
        _lastRelativeBoundsReport = FLT_MAX;
        self.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, contentInset.bottom, frame.size.width - _indicatorInset);
        //true : mean the receiver to block the delivery of touch events to other views in the same window
        self.exclusiveTouch = true;
    }
    return self;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view {
    for (id gestureRecognizer in view.gestureRecognizers) {
        if ([gestureRecognizer isKindOfClass:[DhtDoubleTapGesture class]]) {
            if (![(DhtDoubleTapGesture *)gestureRecognizer canScrollViewStealTouches])
                return false;
        }
    }
    return true;
}

- (void) dealloc {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [_viewStorage clear];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    UIEdgeInsets contentInset = self.contentInset;
    self.scrollIndicatorInsets = UIEdgeInsetsMake(contentInset.top, 0, contentInset.bottom, frame.size.width - _indicatorInset);
}

- (void)setContentInset:(UIEdgeInsets)contentInset {
    [super setContentInset:contentInset];
    self.scrollIndicatorInsets = UIEdgeInsetsMake(contentInset.top, 0, contentInset.bottom, self.frame.size.width - _indicatorInset);
    [self updateVisibleDecorationViews];
}


- (void)updateVisibleDecorationViews {
    std::set<int> currentIndices;
    std::vector<DhtDecorationViewAttrubutes> *pAttributes = [(MessageCollectionViewLayout *)self.collectionViewLayout allDecorationViewAttributes];
    if (pAttributes == NULL){
        return;
    }
    
    CGRect bounds = self.bounds;
    DhtDecorationViewAttrubutes lowerAttributes = {.index = 0, .frame = CGRectMake(0, bounds.origin.y - 30.0f, 0.0f, 0.0f)};
    DhtDecorationViewAttrubutes upperAttributes = {.index = 0, .frame = CGRectMake(0, bounds.origin.y + bounds.size.height, 0.0f, 0.0f)};
    
    auto lowerIt = std::lower_bound(pAttributes->begin(), pAttributes->end(), lowerAttributes, DhtDecorationViewAttrubutesComparator());
    auto upperIt = std::upper_bound(pAttributes->begin(), pAttributes->end(), upperAttributes, DhtDecorationViewAttrubutesComparator());
    
    if (lowerIt != pAttributes->end()) {
        for (auto it = lowerIt; it != upperIt; it++) {
            currentIndices.insert(it->index);
            auto viewIt = _currentVisibleDecorationViews.find(it->index);
            if (viewIt == _currentVisibleDecorationViews.end()) {
                [UIView performWithoutAnimation:^
                 {
                     
                     if (it->index != INT_MIN)
                     {
                         DhtDateHeaderView *view = (DhtDateHeaderView *)[_viewStorage dequeueViewWithIdentifier:@"_headerDate" viewStateIdentifier:[[NSString alloc] initWithFormat:@"date/%d", it->index]];
                         if (view == nil)
                             view = [[DhtDateHeaderView alloc] initWithFrame:it->frame];
                         view.frame = it->frame;
                         view.alpha = 1.0f;
                         [view setDate:it->index isShowFull:it->isShowFull];
                         _currentVisibleDecorationViews[it->index] = view;
                         [self addSubview:view];
                         view.layer.opacity =1;
                         [view layoutSubviews];
                     }
                 }];
            }
            else if (!CGRectEqualToRect(it->frame, viewIt->second.frame))
                viewIt->second.frame = it->frame;
        }
    }
    
    for (auto it = _currentVisibleDecorationViews.begin(); it != _currentVisibleDecorationViews.end(); ) {
        if (currentIndices.find((const int)it->first) == currentIndices.end()) {
            [it->second removeFromSuperview];
            [_viewStorage enqueueView:it->second];
            _currentVisibleDecorationViews.erase(it++);
        }
        else
            ++it;
    }
}

- (void)performBatchUpdates:(void (^)(void))updates completion:(void (^)(BOOL))completion {
    [self performBatchUpdates:updates completion:completion beforeDecorations:nil animated:true animationFactor:0.7f];
}

- (bool)performBatchUpdates:(void (^)(void))updates completion:(void (^)(BOOL))completion beforeDecorations:(void (^)())beforeDecorations animated:(bool)animated animationFactor:(float)animationFactor {
    if ([NSThread isMainThread]) {
        NSLog(@"is performing batch updates in main thread");
    }
    std::map<int, CGRect> previousDecorationViewFrames;
    NSMutableDictionary *removedViews = [[NSMutableDictionary alloc] init];
    
    if (animated) {
        std::vector<DhtDecorationViewAttrubutes> *pAttributes = [(MessageCollectionViewLayout *)self.collectionViewLayout allDecorationViewAttributes];
        if (pAttributes != NULL) {
            for (auto it = pAttributes->begin(); it != pAttributes->end(); it++) {
                previousDecorationViewFrames[it->index] = it->frame;
            }
        }
        
        for (auto it = _currentVisibleDecorationViews.begin(); it != _currentVisibleDecorationViews.end(); it++) {
            removedViews[@(it->first)] = it->second;
        }
        
    }
    
    
    
    bool decorationUpdatesWereDisabled = _disableDecorationViewUpdates;
    _disableDecorationViewUpdates = true;
    
    UIEdgeInsets previousInset = self.contentInset;
    
    ((MessageCollectionViewLayout *)self.collectionViewLayout).animateLayout = true;
    
    [super performBatchUpdates:updates completion:completion];
    
    ((MessageCollectionViewLayout *)self.collectionViewLayout).animateLayout = false;
    
    if (beforeDecorations)
        beforeDecorations();
    
    _disableDecorationViewUpdates = decorationUpdatesWereDisabled;
    
    if (animated) {
        [self updateVisibleDecorationViews];
        
        std::map<int, CGRect> currentDecorationViewFrames;
        
        std::vector<DhtDecorationViewAttrubutes> *pAttributes = [(MessageCollectionViewLayout *)self.collectionViewLayout allDecorationViewAttributes];
        if (pAttributes != NULL) {
            for (auto it = pAttributes->begin(); it != pAttributes->end(); it++)
            {
                currentDecorationViewFrames[it->index] = it->frame;
            }
        }
        
        CGFloat insetDifference = self.contentInset.top - previousInset.top;
        
        for (auto it = _currentVisibleDecorationViews.begin(); it != _currentVisibleDecorationViews.end(); it++) {
            [removedViews enumerateKeysAndObjectsUsingBlock:^(id key, UIView *obj, BOOL *stop)
             {
                 if (it->second == obj)
                 {
                     [removedViews removeObjectForKey:key];
                     *stop = true;
                 }
             }];
            
            auto previousIt = previousDecorationViewFrames.find((const int)it->first);
            auto currentIt = currentDecorationViewFrames.find((const int)it->first);
            
            if (previousIt != previousDecorationViewFrames.end())
                it->second.frame = previousIt->second;
            else if (currentIt != currentDecorationViewFrames.end())
            {
                it->second.frame = CGRectMake(0, currentIt->second.origin.y - currentIt->second.size.height + insetDifference, currentIt->second.size.width, currentIt->second.size.height);
                it->second.alpha = 0.0f;
            }
            
            [it->second layoutSubviews];
        }
        
        [removedViews enumerateKeysAndObjectsUsingBlock:^(id key, UIView *view, __unused BOOL *stop)
         {
             if (previousDecorationViewFrames.find([key intValue]) != previousDecorationViewFrames.end())
             {
                 if ([key intValue] == INT_MIN)
                 {
                     if (view == [_viewStorage dequeueViewWithIdentifier:@"_unread" viewStateIdentifier:nil])
                     {
                         [self insertSubview:view atIndex:0];
                     }
                 }
                 else
                 {
                     if (view == [_viewStorage dequeueViewWithIdentifier:@"_date" viewStateIdentifier:[[NSString alloc] initWithFormat:@"date/%d", [key intValue]]])
                     {
                         [self insertSubview:view atIndex:0];
                     }
                 }
             }
         }];
        
        [UIView animateWithDuration:iosMajorVersion() >= 7 ? 0.3 : 0.3 * animationFactor delay:0 options:0 animations:^
         {
             for (auto it = _currentVisibleDecorationViews.begin(); it != _currentVisibleDecorationViews.end(); it++)
             {
                 auto currentIt = currentDecorationViewFrames.find((const int)it->first);
                 
                 if (currentIt != currentDecorationViewFrames.end())
                     it->second.frame = currentIt->second;
                 it->second.alpha = 1.0f;
                 
                 [it->second layoutSubviews];
             }
             
             [removedViews enumerateKeysAndObjectsUsingBlock:^(id key, UIView *view, __unused BOOL *stop)
              {
                  auto currentIt = currentDecorationViewFrames.find([key intValue]);
                  
                  if (currentIt == currentDecorationViewFrames.end())
                      view.alpha = 0.0f;
                  else
                      view.frame = currentIt->second;
                  
                  [view layoutSubviews];
              }];
         } completion:^(__unused BOOL finished)
         {
             [removedViews enumerateKeysAndObjectsUsingBlock:^(__unused id key, UIView *view, __unused BOOL *stop)
              {
                  [_viewStorage enqueueView: (UIView<BaseViewProtocol> *)view];
                  [view removeFromSuperview];
              }];
         }];
    }
    
    return true;
}

- (void)stopScrollingAnimation {
    UIView *superview = self.superview;
    NSUInteger index = [self.superview.subviews indexOfObject:self];
    [self removeFromSuperview];
    [superview insertSubview:self atIndex:index];
}


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(touchedTableBackground)])
        [self.delegate performSelector:@selector(touchedTableBackground)];
}
#pragma clang diagnostic pop
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
}

- (void)setDelayVisibleItemsUpdate:(bool)delay {
    _delayVisibleItemsUpdate = delay;
}

- (void)updateVisibleItemsNow {
    CGRect bounds = self.bounds;
    [self setBounds:CGRectOffset(bounds, 0.0f, 0.1f)];
    [self setBounds:bounds];
    [self layoutSubviews];
}

- (void)setBounds:(CGRect)bounds {
    if (!CGSizeEqualToSize(bounds.size, self.bounds.size))
        _lastRelativeBoundsReport = FLT_MAX;
    [super setBounds:bounds];
}

- (void)reloadData {
    _lastRelativeBoundsReport = FLT_MAX;
    [super reloadData];
    [self updateVisibleItemsNow];
}

- (void)scrollToTopIfNeeded {
    if (self.contentSize.height > self.frame.size.height - self.contentInset.top - self.contentInset.bottom) {
        [self setContentOffset:CGPointMake(0.0f, MIN(self.contentOffset.y + self.bounds.size.height * 3.0f, self.contentSize.height + self.contentInset.bottom - self.bounds.size.height)) animated:true];
    }
}

- (void) scrollToBottom:(BOOL)animated {
    if (animated) {
        [self setContentOffset:CGPointMake(0.0f, -self.contentInset.top) animated:true];
    } else {
        [self setContentOffset:CGPointMake(0.0f, -self.contentInset.top) animated:false];
    }
}

- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated {
    [super setContentOffset:contentOffset animated:animated];
}

- (void)setDisableDecorationViewUpdates:(bool)disableDecorationViewUpdates {
    _disableDecorationViewUpdates = disableDecorationViewUpdates;
}

- (bool)disableDecorationViewUpdates {
    return _disableDecorationViewUpdates;
}

- (UIView *)viewForDecorationAtIndex:(int)index {
    auto it = _currentVisibleDecorationViews.find(index);
    if (it != _currentVisibleDecorationViews.end())
        return it->second;
    return nil;
}

- (NSArray *)visibleDecorations {
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:_currentVisibleDecorationViews.size()];
    for (auto it : _currentVisibleDecorationViews) {
        [array addObject:it.second];
    }
    return array;
}

- (void)updateDecorationAssets {
    for (auto it : _currentVisibleDecorationViews) {
        [((DhtDateHeaderView *)it.second) updateAssets];
    }
    [_viewStorage clear];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect bounds = self.bounds;
    if (ABS(bounds.origin.y - _lastRelativeBoundsReport) > 5.0f) {
        _lastRelativeBoundsReport = bounds.origin.y;
        [self updateRelativeBounds];
    }
    
    [self updateVisibleDecorationViews];
}

- (void)updateRelativeBounds {
    Class cellClass = [CollectionCell class];
    CGRect bounds = self.bounds;
    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:cellClass]) {
            CGPoint subviewPosition = subview.frame.origin;
            [(CollectionCell *)subview relativeBoundsUpdated:CGRectOffset(bounds, -subviewPosition.x, -subviewPosition.y)];
        }
    }
}

@end
