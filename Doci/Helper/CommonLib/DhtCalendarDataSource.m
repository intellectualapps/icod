//
//  DhtCalendarDataSource.m
//  kids_taxi_driver
//
//  Created by Nguyen Van Dung on 7/25/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "DhtCalendarDataSource.h"
#import "MonthInfo.h"
#import "DayInfo.h"
#import "NSDate+Extension.h"

@implementation DhtCalendarDataSource
+ (MonthInfo *)getMonthDaysInfoForDate:(NSDate *)date
                            starterDay:(NSInteger)starterWeekday
                       includingDayOut:(BOOL)includeDayOut {
    date = [[date dateAtStartOfMonth] dateByAddingDays:15];
    NSDate *firstDate = date;
    MonthInfo *minfo = [[MonthInfo alloc] init];
    MonthDateRange *range =  [firstDate monthDateRange: starterWeekday];
    NSInteger countOfWeaks = range.countOfWeaks;
    NSDate *firstMonthDateIn = range.startDate;
    NSDate *lastMonthDateIn = range.endDate;
    DateRange *dateRange = [lastMonthDateIn dateRange];
    NSInteger countOfDaysIn = dateRange.day;

    NSCalendar *calendar = [NSCalendar currentCalendar];
    //find all dates in
    NSMutableArray *datesIn = [NSMutableArray array];
    for (int day = 1; day < countOfDaysIn ; day++) {
        NSDateComponents *components = [firstMonthDateIn componentsForDate];
        components.day = day;
        NSDate* date = [calendar dateFromComponents: components];
        if (date) {
            [datesIn addObject: date];
        }
    }

    //find day out

    NSDate *firstMonthDateOut = nil;
    NSInteger firstMonthDateInWeekday = [DhtCalendarDataSource weekdayForDate:firstMonthDateIn];
    if (firstMonthDateInWeekday == starterWeekday) {
        firstMonthDateOut = firstMonthDateIn;
    } else {
        NSDateComponents *component = [firstMonthDateIn componentsForDate];
        for (int i = 1; i <= 7; i++ ) {
            component.day -= 1;
            NSDate *updateDate  = [[NSCalendar currentCalendar] dateFromComponents:component];
            NSInteger updatedDateWeekday = [DhtCalendarDataSource weekdayForDate:updateDate];
            if (updatedDateWeekday == starterWeekday) {
                firstMonthDateOut = updateDate;
                break;
            }
        }
        
        if (firstMonthDateOut == nil) {
            NSInteger diff = 7 - firstMonthDateInWeekday;
            for (NSInteger j = diff; j < 7; j++) {
                component.day += 1;
                NSDate *updateDate  = [[NSCalendar currentCalendar] dateFromComponents:component];
                NSInteger updatedDateWeekday = [self weekdayForDate:updateDate];
                if (updatedDateWeekday == starterWeekday) {
                    firstMonthDateOut = updateDate;
                    break;
                }
            }
        }
    }
    
    NSMutableArray *firstWeekDates = [[NSMutableArray alloc] init];
    NSMutableArray *lastWeekDates = [[NSMutableArray alloc] init];
    
    NSDate *firstWeekDate = (firstMonthDateOut != nil) ? firstMonthDateOut : firstMonthDateIn;
    NSDateComponents *component = [firstWeekDate componentsForDate];
    component.day += 6;
    NSDate * lastWeekDate = [[NSCalendar currentCalendar] dateFromComponents:component];
    
    for (NSInteger weekIndex = 1; weekIndex <= countOfWeaks; weekIndex ++) {
        [firstWeekDates addObject:firstWeekDate];
        [lastWeekDates addObject:lastWeekDate];
        firstWeekDate = [DhtCalendarDataSource nextWeekDateFromDate:firstWeekDate];
        lastWeekDate = [DhtCalendarDataSource nextWeekDateFromDate:lastWeekDate];
    }
    
    NSInteger count = firstWeekDates.count;
    for (int i = 0; i < count; i++) {
        NSDate *firstWeekDate = firstWeekDates[i];
        NSDateComponents *fComponent = [firstWeekDate componentsForDate];
        for (int weekday = 1; weekday <=7 ; weekday++) {
            NSDate *weekdate = [[NSCalendar currentCalendar] dateFromComponents:fComponent];
            fComponent.day += 1;
            NSInteger day = [weekdate dateRange].day;
            DayInfo* dInfo = [[DayInfo alloc] initWithDate: weekdate];
            if (i == 0  && day > 20) {
                dInfo.isDayIn = false;
            } else if (i == countOfWeaks -1 && day < 10) {
                dInfo.isDayIn = false;
            } else {
                dInfo.isDayIn = true;
            }
            if (dInfo.isDayIn || includeDayOut) {
                [minfo addDay:dInfo];
            }
        }
    }
    minfo.numberOfWeaks = range.countOfWeaks;
    return minfo;
}

+ (NSDate *)nextWeekDateFromDate:(NSDate *)date {
    NSDateComponents *component = [date componentsForDate];
    component.day += 7;
    return  [[NSCalendar currentCalendar] dateFromComponents:component];
}

+ (NSInteger)weekdayForDate:(NSDate *)date {
    NSCalendarUnit unit = NSCalendarUnitWeekday;
    NSDateComponents *component = [[NSCalendar currentCalendar] components:unit fromDate:date];
    return component.weekday;
}
@end
