//
//  ConversationInputTextView.h
//  ChatPro
//
//  Created by dungnv9 on 2/18/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConversationInputView.h"
@class ConversationInputTextView;
@class HPGrowingTextView;


@protocol ConversationInputTextPanelDelegate <ConversationInputPanelDelegate>

- (void)inputPanelTextChanged:(ConversationInputTextView *)inputTextPanel text:(NSString *)text;
- (void)inputTextPanelHasIndicatedTypingActivity:(ConversationInputTextView *)inputTextPanel;
- (void)inputTextPanelHasCancelledTypingActivity:(ConversationInputTextView *)inputTextPanel;
- (void)inputPanelRequestedSendMessage:(ConversationInputTextView *)inputTextPanel text:(NSString *)text;
- (void)inputPanelRequestAttachImage:(ConversationInputTextView *)inputTextPanel;


@end
@interface ConversationInputTextView : ConversationInputView
{
    
}
@property (nonatomic, strong) UIButton  *sendButton;
@property (nonatomic, strong) HPGrowingTextView *inputField;
@property (nonatomic) UIInterfaceOrientation interfaceOrientation;
- (HPGrowingTextView *)maybeInputField;
- (void) setPlaceHolderString:(NSString *)str;
- (instancetype)initWithFrame:(CGRect)frame placeholder:(NSString *)placeholder allowsChat:(BOOL)allows;
- (void)addButtonWithtitle:(NSString *)title titleColor:(UIColor *)color image:(UIImage *)img selectedImage:(UIImage *)selectedImg target:(id)target action:(SEL)action;
@end
