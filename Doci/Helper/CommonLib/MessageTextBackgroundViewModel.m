//
//  MessageTextBackgroundViewModel.m
//  MtoM
//
//  Created by nguyen van dung on 12/15/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "MessageTextBackgroundViewModel.h"
#import "Utils.h"
#import "DhtImageView.h"

#define TextMessageBackgroundImageDef(name, incoming, filePhone, filePad) \
static UIImage *name() \
{ \
static UIImage *image = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^ \
{ \
CGSize screenSize = ScreenSize(); \
CGFloat screenSide = MAX(screenSize.width, screenSize.height); \
bool isLarge = (IsPad() || (screenSide >= 667.0f - FLT_EPSILON)); \
image = [[UIImage imageNamed:!isLarge ? filePhone : filePad] stretchableImageWithLeftCapWidth:incoming ? 23 : (40 - 23) topCapHeight:16]; \
}); \
return image; \
}

TextMessageBackgroundImageDef(incomingImage, true, @"IncomingFull.png", @"IncomingFullPad.png")
TextMessageBackgroundImageDef(incomingImageHighlighted, true, @"IncomingFullHighlighted.png", @"IncomingFullHighlightedPad.png")

TextMessageBackgroundImageDef(outgoingImage, false, @"OutgoingFull.png", @"OutgoingFullPad.png")
TextMessageBackgroundImageDef(outgoingImageHighlighted, false, @"OutgoingFullHighlighted.png", @"OutgoingFullHighlightedPad.png")

@interface DhtBgImageView : DhtImageView

@end

@implementation DhtBgImageView



@end
@interface MessageTextBackgroundViewModel() {
    TextMessageBackgroundType _type;
    bool _imageIsValid;
    bool _highlighted;
}
@end

@implementation MessageTextBackgroundViewModel

- (instancetype)initWithType:(TextMessageBackgroundType)type {
    self = [super initWithImage:nil];
    if (self != nil) {
        _type = type;
    }
    return self;
}

- (Class)viewClass {
    return [DhtBgImageView class];
}

- (void)bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage {
    _highlighted = false;
    if (!_imageIsValid) {
        _imageIsValid = true;
        self.image = _type == TextMessageBackgroundIncoming ? incomingImage() : outgoingImage();
    }
    [super bindViewToContainer:container viewStorage:viewStorage];
}

- (void)setPartialMode:(bool)partialMode {
    if (_partialMode != partialMode) {
        bool wasPartial = _partialMode;
        _partialMode = partialMode;
        _imageIsValid = false;
        
        if ([self boundView] != nil) {
            UIImageView *boundView = (UIImageView *)[self boundView];
            
            _imageIsValid = !_highlighted;
            UIImage *previousImage = self.image;
            UIImage *newImage = [self currentImage];
            self.image = newImage;
            
            if (previousImage != nil && iosMajorVersion() >= 7) {
                UIImageView *overlayImageView = [[UIImageView alloc] initWithFrame:boundView.bounds];
                
                if (!wasPartial) {
                    boundView.image = newImage;
                    overlayImageView.image = previousImage;
                } else {
                    boundView.image = previousImage;
                    overlayImageView.image = newImage;
                    overlayImageView.alpha = 0.0f;
                }
                
                [boundView addSubview:overlayImageView];
                
                [UIView animateWithDuration:0.3 * 0.7 animations:^{
                     overlayImageView.alpha = wasPartial ? 1.0f : 0.0f;
                } completion:^(__unused BOOL finished) {
                     if (finished && wasPartial)
                         boundView.image = newImage;
                     [overlayImageView removeFromSuperview];
                }];
            } else {
                [(UIImageView *)[self boundView] setImage:self.image];
            }
        }
    }
}

- (UIImage *)currentImage {
    UIImage *newImage = nil;
    if (_type == TextMessageBackgroundIncoming) {
        newImage = _highlighted ? incomingImageHighlighted() : incomingImage();
    } else {
        newImage = _highlighted ? outgoingImageHighlighted() : outgoingImage();
    }
    return newImage;
}

- (void)addScaleAnimationToLayer:(CALayer *)layer from:(CGSize)fromScale to:(CGSize)toScale duration:(NSTimeInterval)duration {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(fromScale.width, fromScale.height, 1.0f)];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(toScale.width, toScale.height, 1.0f)];
    animation.duration = duration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.removedOnCompletion = true;
    animation.fillMode = kCAFillModeForwards;
    [layer addAnimation:animation forKey:@"transform"];
}

- (void)clearHighlight {
    if (_highlighted) {
        _highlighted = false;
        if ([self boundView] != nil) {
            UIImage *previousImage = self.image;
            UIImage *newImage = [self currentImage];
            self.image = newImage;
            _imageIsValid = true;
            
            ((UIImageView *)[self boundView]).image = self.image;
            if (previousImage != nil && iosMajorVersion() >= 7) {
                UIImageView *overlayImageView = [[UIImageView alloc] initWithFrame:[self boundView].frame];
                overlayImageView.image = previousImage;
                
                [[self boundView].superview insertSubview:overlayImageView aboveSubview:[self boundView]];
                
                CGSize frameSize = [self boundView].frame.size;
                [self addScaleAnimationToLayer:[self boundView].layer from:CGSizeMake((frameSize.width - 2.0f) / frameSize.width, (frameSize.height - 2.0f) / frameSize.height) to:CGSizeMake(1.0f, 1.0f) duration:0.2];
                
                [UIView animateWithDuration:0.4 animations:^{
                     overlayImageView.alpha = 0.0f;
                } completion:^(__unused BOOL finished) {
                     [overlayImageView removeFromSuperview];
                }];
            }
        }
    }
}

- (void)setHighlightedIfBound {
    if ([self boundView] != nil && !_highlighted) {
        _highlighted = true;
        _imageIsValid = false;
        self.image = [self currentImage];
        ((UIImageView *)[self boundView]).image = self.image;
    }
}
@end
