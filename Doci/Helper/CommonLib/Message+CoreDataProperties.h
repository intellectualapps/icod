//
//  Message+CoreDataProperties.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 5/5/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Message.h"

NS_ASSUME_NONNULL_BEGIN

@interface Message (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *avatarUrlString;
@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSString *conversationId;
@property (nonatomic) double createdDate;
@property (nonatomic) BOOL incoming;
@property (nullable, nonatomic, retain) NSString *localIdentifier;
@property (nullable, nonatomic, retain) NSString *messageId;
@property (nonatomic) double modifiedDate;
@property (nonatomic) int16_t status;
@property (nonatomic) int32_t orderId;
@property (nullable, nonatomic, retain) NSString *uid;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;

@end

NS_ASSUME_NONNULL_END
