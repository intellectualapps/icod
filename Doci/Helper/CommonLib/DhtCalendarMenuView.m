//
//  DhtCalendarMenuView.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtCalendarMenuView.h"
@interface DhtCalendarMenuItemView: UIView {
    
}
@property (nonatomic, strong) UILabel   *label;
- (void)setTitle:(NSString *)title font:(UIFont *)font backgroundColor:(UIColor *)color textColor:(UIColor *)textColor;
@end


@implementation DhtCalendarMenuItemView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if (self) {
        _label = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _label.font = [UIFont systemFontOfSize:20.0];
        _label.textColor = [UIColor blueColor];
        _label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_label];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.label.frame = self.bounds;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    _label.frame = self.bounds;
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    _label.frame = self.bounds;
}

- (void)setTitle:(NSString *)title font:(UIFont *)font backgroundColor:(UIColor *)color textColor:(UIColor *)textColor {
    _label.text = title;
    _label.font = font;
    _label.textColor = textColor;
    self.backgroundColor = color;
}

@end

@interface DhtCalendarMenuView() {
    
}
@property (nonatomic, strong) NSMutableArray *mewnuItems;
@end

@implementation DhtCalendarMenuView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if (self) {
    }
    return self;
}

- (void)setup {
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self addMenuItems];
}

- (void)addMenuItems {
    _mewnuItems = [[NSMutableArray alloc] init];
    NSArray * symbols = @[
                    NSLocalizedString(@"day.sunday", comment: ""),
                    NSLocalizedString(@"day.monday", comment: ""),
                    NSLocalizedString(@"day.tuesday", comment: ""),
                    NSLocalizedString(@"day.wednesday", comment: ""),
                    NSLocalizedString(@"day.thursday", comment: ""),
                    NSLocalizedString(@"day.friday", comment: ""),
                    NSLocalizedString(@"day.saturday", comment: ""),
                    ];
    if (self.delegate && [self.delegate respondsToSelector:@selector(calendarMenuSymbols)]) {
        symbols = [self.delegate calendarMenuSymbols];
    }
    
    UIColor *textColor = [UIColor blackColor];
    if (self.delegate && [self.delegate respondsToSelector:@selector(menuItemTextColor)]) {
        textColor = [self.delegate menuItemTextColor];
    }
    
    NSArray *backgroundColors = nil;
    if (self.delegate && [self.delegate respondsToSelector:@selector(calendarMenuBackgroundColors)]) {
        backgroundColors  = [self.delegate calendarMenuBackgroundColors];
    }
    
    UIFont *font = [UIFont systemFontOfSize:20];
    if (self.delegate && [self.delegate respondsToSelector:@selector(menuItemFont)]) {
        font = [self.delegate menuItemFont];
    }
    
    for (int i = 0; i < symbols.count; i ++) {
        DhtCalendarMenuItemView *itemView = [[DhtCalendarMenuItemView alloc] initWithFrame:CGRectZero];
        NSString *title = [symbols objectAtIndex:i];
        UIColor *bgColor = nil;
        if (i < [backgroundColors count]) {
            bgColor = [backgroundColors objectAtIndex:i];
        }
        [itemView setTitle:title font:font backgroundColor:bgColor textColor:textColor];
        [_mewnuItems addObject:itemView];
        [self addSubview:itemView];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect itemRect = CGRectZero;
    itemRect.size = CGSizeMake(self.bounds.size.width / 7, self.bounds.size.height);
    itemRect.origin.y = 0;
    NSInteger index = 0;
    for (DhtCalendarMenuItemView *itemView in self.mewnuItems) {
        itemRect.origin.x = index * itemRect.size.width;
        itemView.frame = itemRect;
        index += 1;
    }
}
@end
