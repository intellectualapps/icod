//
//  DhtCalendarUtils.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

//Define direction of calenader view
#import <UIKit/UIKit.h>
@class DhtCalendarContentView;
@class DhtCalendarDayCell;

typedef NS_ENUM(NSInteger, DhtCalendarDirection) {
    DhtCalendarDirectionHorizontal = 0,
    DhtCalendarDirectionVertical
} ;

@protocol DhtCalendarViewDelegate<NSObject>
@optional
- (CGFloat)lineSpacingForCalendar;
- (CGFloat)horizontalSpacingForCalendar;
- (BOOL)allowsCalendarMultiSelection;
- (void)didSelectedMonthView:(DhtCalendarContentView *)monthView;
- (void)didSelectDayView:(DhtCalendarDayCell *)dayView;
- (void)didRenderDayView:(DhtCalendarContentView *)monthView;
- (void)calendarWillBeginDragging;
- (UIView *)supplementViewForBound:(CGRect)bound;
- (UIColor *)dayInPastBackgroundColor;
@end


@interface DhtCalendarUtils : NSObject
+ (CGSize) sizeForText:(NSString *)text
boundSize:(CGSize)bsize
option:(NSStringDrawingOptions)option
lineBreakMode:(NSLineBreakMode)lineBreakMode
                  font:(UIFont *)font;
@end