//
//  GenericInfo.h
//  kids_taxi_driver
//
//  Created by Nguyen Van Dung on 5/23/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"

@interface GenericInfo : NSObject
@property (nonatomic, copy) NSString *localIdentifier;
@property (nonatomic, copy) NSString *objectId;
@property (nonatomic, copy) NSString *content;
@property (nonatomic) double createdDate;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic) BOOL incoming;
@property (nonatomic, assign) MessageStatus status;
@property (nonatomic, assign) BOOL deleted;
- (NSString *)getIdentifier;
- (NSString *)authorName;

@end
