//
//  DhtTextViewModel.h
//  ChatPro
//
//  Created by dungnv9 on 2/25/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//
#import <CoreText/CoreText.h>
#import "DhtViewModel.h"
#import "ReusableLabel.h"

typedef void (^TextViewTapAction) ();

@interface DhtTextViewModel : DhtViewModel
{
    
}

@property (nonatomic, strong) NSString *text;
@property (nonatomic) CTFontRef font;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *bgColor;
@property (nonatomic) NSInteger lineCount;
@property (nonatomic, assign) CGFloat maxWidth;
@property (nonatomic, strong) NSArray *textCheckingResults;
@property (nonatomic) NSTextAlignment alignment;
@property (nonatomic) ReusableLabelLayout layoutFlags;//ReusableLabelLayout
@property (nonatomic) CGFloat additionalTrailingWidth;
@property (nonatomic, strong) NSArray *additionalAttributes;
@property (nonatomic, readonly) bool isRTL;
@property (nonatomic) BOOL needTruncateLastLine;
@property (nonatomic) BOOL userTextView;
@property (nonatomic, copy) TextViewTapAction tapAction;
- (instancetype)initWithText:(NSString *)text
                 layoutFlags:(int)flag
                        font:(CTFontRef)font
                   textColor:(UIColor *)color
                     maxLine:(NSInteger)maxline
            willTruncateTail:(BOOL) willTruncateTail;

- (bool)layoutNeedsUpdatingForContainerSize:(CGSize)containerSize;
- (void)layoutForContainerSize:(CGSize)containerSize;
- (NSString *)linkAtPoint:(CGPoint)point regionData:(__autoreleasing NSArray **)regionData;
- (void) preparingFrame:(CGFloat) maxWidth;
- (NSInteger) numberOfTextLine;
@end
