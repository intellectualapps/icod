//
//  Cache.m
//  Cosplay No.1
//
//  Created by nguyenvandung on 10/7/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "Cache.h"
#import <objc/runtime.h>
#import <CommonCrypto/CommonDigest.h>
#import <ImageIO/ImageIO.h>
#include <sys/sysctl.h>
#include <mach/mach.h>
#include <mach/mach_time.h>
#import <pthread.h>
#import "DhtUtility.h"
#import "Utils.h"

static NSString *md5String(NSString *string) {
    const char *ptr = [string UTF8String];
    unsigned char md5Buffer[16];
    
    CC_MD5(ptr, (uint32_t)[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding], md5Buffer);
    NSString *output = [[NSString alloc] initWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", md5Buffer[0], md5Buffer[1], md5Buffer[2], md5Buffer[3], md5Buffer[4], md5Buffer[5], md5Buffer[6], md5Buffer[7], md5Buffer[8], md5Buffer[9], md5Buffer[10], md5Buffer[11], md5Buffer[12], md5Buffer[13], md5Buffer[14], md5Buffer[15]];
    return output;
}

int deviceMemorySize() {
    static int memorySize = 0;
    if (memorySize == 0) {
        size_t len;
        __int64_t nmem;
        len = sizeof(nmem);
        sysctlbyname("hw.memsize", &nmem, &len, NULL, 0);
        memorySize = (int)(nmem / (1024 * 1024));
    }
    return memorySize;
}

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight) {
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

UIImage *__ScaleImage(UIImage *image, CGSize size) {
    return __ScaleAndRoundCornersWithOffsetAndFlags(image, size, CGPointZero, size, 0, nil, true, nil, 0);
}

UIImage *__ScaleAndRoundCornersWithOffsetAndFlags(UIImage *image, CGSize size, CGPoint offset, CGSize imageSize, int radius, UIImage *overlay, bool opaque, UIColor *backgroundColor, int flags) {
    if (CGSizeEqualToSize(imageSize, CGSizeZero))
        imageSize = size;
    
    float scale = 1.0f;
    if (IsRetina()) {
        scale = 2.0f;
        size.width *= 2;
        size.height *= 2;
        imageSize.width *= 2;
        imageSize.height *= 2;
        radius *= 2;
    }
    
    UIGraphicsBeginImageContextWithOptions(imageSize, opaque, 1.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();

    if (overlay != nil)
        CGContextSaveGState(context);
    
    if (backgroundColor != nil) {
        CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
        CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    } else if (opaque) {
        static UIColor *whiteColor = nil;
        if (whiteColor == nil)
            whiteColor = [UIColor whiteColor];
        CGContextSetFillColorWithColor(context, whiteColor.CGColor);
        CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    }
    
    if (radius > 0) {
        CGContextBeginPath(context);
        CGRect rect = (flags & ScaleImageRoundCornersByOuterBounds) ? CGRectMake(offset.x * scale, offset.y * scale, imageSize.width, imageSize.height) : CGRectMake(offset.x * scale, offset.y * scale, size.width, size.height);
        addRoundedRectToPath(context, rect, radius, radius);
        CGContextClosePath(context);
        CGContextClip(context);
    }
    
    CGPoint actualOffset = CGPointEqualToPoint(offset, CGPointZero) ? CGPointMake((int)((imageSize.width - size.width) / 2), (int)((imageSize.height - size.height) / 2)) : CGPointMake(offset.x * scale, offset.y * scale);
    if (flags & ScaleImageFlipVerical) {
        CGContextTranslateCTM(context, actualOffset.x + size.width / 2, actualOffset.y + size.height / 2);
        CGContextScaleCTM(context, 1.0f, -1.0f);
        CGContextTranslateCTM(context, -actualOffset.x - size.width / 2, -actualOffset.y - size.height / 2);
    }
    [image drawInRect:CGRectMake(actualOffset.x, actualOffset.y, size.width, size.height) blendMode:kCGBlendModeCopy alpha:1.0f];
    
    if (overlay != nil) {
        CGContextRestoreGState(context);
        if (flags & ScaleImageScaleOverlay) {
            CGContextScaleCTM(context, scale, scale);
            [overlay drawInRect:CGRectMake(0, 0, imageSize.width / scale, imageSize.height / scale)];
        } else {
            [overlay drawInRect:CGRectMake(0, 0, overlay.size.width * scale, overlay.size.height * scale)];
        }
    }
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

@interface CacheObject: NSObject

@property (nonatomic) NSTimeInterval date;
@property (nonatomic, strong) id object;
@property (nonatomic) NSUInteger size;

- (id)initWithObject:(id)object size:(NSUInteger)size;

@end

@implementation CacheObject

@synthesize date = _date;
@synthesize object = _object;
@synthesize size = _size;

- (id)initWithObject:(id)object size:(NSUInteger)size {
    self = [super init];
    if (self != nil) {
        _object = object;
        _date = CFAbsoluteTimeGetCurrent();
        _size = size;
    }
    return self;
}

- (void) dealloc {
}
@end


static NSFileManager *cacheFileManager = nil;
@interface Cache() {
    __SYNCHRONIZED_DEFINE(_dataMemoryCache);
}

@property (nonatomic, strong) NSMutableArray *temporaryCachedImagesSources;
@property (nonatomic, strong) NSMutableDictionary *memoryCache;
@property (nonatomic) int imageMemoryLimit;
@property (nonatomic) int imageMemoryLimitInternal;
@property (nonatomic) int memoryCacheSize;
@property (nonatomic) int dataMemoryLimit;
@property (nonatomic) int dataMemoryLimitInternal;
@property (nonatomic) int thumbnailMemoryLimit;
@property (nonatomic) int thumbnailMemoryLimitInternal;
@property (nonatomic, strong) NSMutableDictionary *dataMemoryCache;
@property (nonatomic) int dataMemoryCacheSize;
@property (nonatomic, strong, readonly) NSString *diskCachePath;
@property (nonatomic, strong) NSMutableDictionary *thumbnailCache;
@property (nonatomic) int thumbnailCacheSize;
@property (nonatomic) int memoryWarningBaseline;
@property (nonatomic) int backgroundBaseline;

@end

@implementation Cache

+ (Cache *) shareInstance {
    static Cache *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Cache alloc] init];
    });
    return instance;
}
    
+ (dispatch_queue_t)diskCacheQueue {
    static dispatch_queue_t queue = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      queue = dispatch_queue_create("com.cache.diskcache", 0);
                      if (cacheFileManager == nil)
                          cacheFileManager = [[NSFileManager alloc] init];
                  });
    return queue;
}

+ (NSFileManager *)diskFileManager {
    if (cacheFileManager == nil)
        cacheFileManager = [[NSFileManager alloc] init];
    
    return cacheFileManager;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        __SYNCHRONIZED_INIT(_dataMemoryCache);
        
        _temporaryCachedImagesSources = [[NSMutableArray alloc] init];
        
        _imageMemoryLimit = deviceMemorySize() > 300 ? (int)(50 * 1024 * 1024) : (int)(11 * 1024 * 1024);
        _imageMemoryLimitInternal = deviceMemorySize() > 300 ? 1024 * 1024 : 812 * 1024;
        
        _dataMemoryLimit = deviceMemorySize() > 300 ? (int)(3 * 1024 * 1024) : (int)(0.6 * 1024 * 1024);
        _dataMemoryLimitInternal = cpuCoreCount() > 1 ? (int)(0.4 * 1024 * 1024) : (int)(0.25 * 1024 * 1024);
        
        _thumbnailMemoryLimit = deviceMemorySize() > 300 ? (int)(15 * 1024 * 1024) : (int)(4 * 1024 * 1024);
        _thumbnailMemoryLimitInternal = cpuCoreCount() > 1 ? (int)(1.5 * 1024 * 1024) : (int)(1 * 1024 * 1024);
        
        _memoryWarningBaseline = deviceMemorySize() > 300 ? (int)(1.5 * 1024 * 1024) : (int)(1.1 * 1024 * 1024);
        _memoryCache = [[NSMutableDictionary alloc] init];
       _backgroundBaseline = deviceMemorySize() > 300 ? (int)(5.8 * 1024 * 1024) : (int)(2.8 * 1024 * 1024);
        self.memoryCacheSize = 0;
        _thumbnailCache = [[NSMutableDictionary alloc] init];
        _thumbnailCacheSize = 0;
        
        _dataMemoryCache = [[NSMutableDictionary alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cachesPath = [paths objectAtIndex:0];
        
        _diskCachePath = cachesPath;
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        if (![fileManager fileExistsAtPath:_diskCachePath])
            [fileManager createDirectoryAtPath:_diskCachePath withIntermediateDirectories:YES attributes:nil error:NULL];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning:) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)didReceiveMemoryWarning:(NSNotification *)__unused notification {
    [self freeMemoryCache:_memoryWarningBaseline];
}

- (void)didEnterBackground:(NSNotification *)__unused notification {
    [self freeMemoryCache:_backgroundBaseline];
}

- (void)diskCacheContains:(NSString *)url1 completion:(void (^)(bool cached))completion {
    dispatch_async([Cache diskCacheQueue], ^{
       bool cached = [cacheFileManager fileExistsAtPath:[_diskCachePath stringByAppendingPathComponent:md5String(url1)]];
        if (completion)
            completion(cached);
    });
}

- (void)freeMemoryCache:(NSUInteger)targetSize {
    dispatch_block_t block = ^{
        if (self.memoryCacheSize > targetSize) {
            __unused int sizeBefore = self.memoryCacheSize;
            NSArray *sortedKeys = [_memoryCache keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2)
                                   {
                                       return (((CacheObject *)obj1).date < ((CacheObject *)obj2).date) ? NSOrderedAscending : NSOrderedDescending;
                                   }];
            for (int i = 0; i < sortedKeys.count && self.memoryCacheSize > targetSize; i++) {
                NSString *key = [sortedKeys objectAtIndex:i];
                CacheObject *record = [_memoryCache objectForKey:key];
                self.memoryCacheSize -= (int)record.size;
                if (self.memoryCacheSize < 0)
                    self.memoryCacheSize = 0;
                [_memoryCache removeObjectForKey:key];
            }
            
            __block int currentCacheSize = 0;
            [_memoryCache enumerateKeysAndObjectsUsingBlock:^(__unused NSString *key, CacheObject *record, __unused BOOL *stop) {
                 currentCacheSize += record.size;
             }];
            
            self.memoryCacheSize = currentCacheSize;
        }
    };
    if ([NSThread isMainThread])
        block();
    else
        dispatch_async(dispatch_get_main_queue(), block);
}

- (void)freeCompressedMemoryCache:(NSUInteger)targetSize reentrant:(bool)reentrant {
    if (!reentrant) {
         __SYNCHRONIZED_BEGIN(_dataMemoryCache);
    }
    
    if (_dataMemoryCacheSize > targetSize) {
        __unused int sizeBefore = _dataMemoryCacheSize;
        NSArray *sortedKeys = [_dataMemoryCache keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2)
                               {
                                   return (((CacheObject *)obj1).date < ((CacheObject *)obj2).date) ? NSOrderedAscending : NSOrderedDescending;
                               }];
        for (int i = 0; i < sortedKeys.count && _dataMemoryCacheSize > targetSize; i++) {
            NSString *key = [sortedKeys objectAtIndex:i];
            CacheObject *record = [_dataMemoryCache objectForKey:key];
            _dataMemoryCacheSize -= record.size;
            if (_dataMemoryCacheSize < 0)
                _dataMemoryCacheSize = 0;
            [_dataMemoryCache removeObjectForKey:key];
        }
    }
    if (!reentrant) {
        __SYNCHRONIZED_END(_dataMemoryCache);
    }
}

- (void)cacheCompressedObject:(NSData *)data url:(NSString *)url reentrant:(bool)reentrant {
    if (!reentrant) {
        __SYNCHRONIZED_BEGIN(_dataMemoryCache);
    }
    CacheObject *cacheRecord = [_dataMemoryCache objectForKey:url];
    if (cacheRecord != nil) {
        _dataMemoryCacheSize -= cacheRecord.size;
        cacheRecord.date = CFAbsoluteTimeGetCurrent();
        cacheRecord.object = data;
        cacheRecord.size = data.length;
        _dataMemoryCacheSize += cacheRecord.size;
    } else {
        [_dataMemoryCache setObject:[[CacheObject alloc] initWithObject:data size:data.length] forKey:url];
        _dataMemoryCacheSize += data.length;
    }
    
    if (_dataMemoryCacheSize >= _dataMemoryLimit + _dataMemoryLimitInternal) {
        [self freeCompressedMemoryCache:_dataMemoryLimit reentrant:true];
    }
    
    if (!reentrant) {
        __SYNCHRONIZED_END(_dataMemoryCache);
    }
}

- (void)cacheImage:(UIImage *)image withData:(NSData *)data url:(NSString *)url cacheLocation:(NSInteger)location {
    if (image != nil && (location & CacheMemory)) {
        int size = (int)(image.size.width * image.size.height * 4 * image.scale);
        dispatch_block_t block  = ^{
            CacheObject *obj =  [_memoryCache objectForKey:url];
            if (obj != nil) {
                _memoryCacheSize -= obj.size;
                obj.date  = CFAbsoluteTimeGetCurrent();
                obj.object = image;
                obj.size = size;
                self.memoryCacheSize += size;
            } else {
                [_memoryCache setObject:[[CacheObject alloc] initWithObject:image size:size] forKey:url];
                self.memoryCacheSize += size;
            }
            
            if (self.memoryCacheSize >= self.imageMemoryLimit + _imageMemoryLimitInternal) {
                [self freeMemoryCache:self.imageMemoryLimit];
            }
        };
        
        if ([NSThread isMainThread])
            block();
        else
            dispatch_async(dispatch_get_main_queue(), block);
    }
    
    if ((data != nil || image != nil) && (location & CacheDisk) && url != nil) {
        dispatch_async([Cache diskCacheQueue], ^{
                           if (data != nil)
                           {
                               [self cacheCompressedObject:data url:url reentrant:false];
                               
                               [data writeToFile:[_diskCachePath stringByAppendingPathComponent:md5String(url)] atomically:true];
                           }
                       });
    }
}

- (UIImage *)cachedImage:(NSString *)url cacheLocation:(NSInteger)location {
    UIImage *image = nil;
    
    if (location & CacheMemory) {
        __block UIImage *blockImage = nil;
        dispatch_block_t block = ^{
            CacheObject *obj = [_memoryCache objectForKey:url];
            if (obj != nil) {
                obj.date = CFAbsoluteTimeGetCurrent();
                blockImage = obj.object;
            } else if (_temporaryCachedImagesSources.count != 0) {
                for (NSDictionary *dict in _temporaryCachedImagesSources) {
                    UIImage *image = [dict objectForKey:url];
                    if (image != nil) {
                        blockImage = image;
                        break;
                    }
                }
            }
        };
        if ([NSThread isMainThread])
            block();
        else
            dispatch_sync(dispatch_get_main_queue(), block);
        image = blockImage;
    }
    
    if (image != nil)
        return image;
    
    if (location & CacheDisk) {
        UIImage *dataImage = nil;
        
        __SYNCHRONIZED_BEGIN(_dataMemoryCache);
        {
            CacheObject *obj = [_dataMemoryCache objectForKey:url];
            if (obj != nil) {
                obj.date = CFAbsoluteTimeGetCurrent();
                
                if (false) {
                    dataImage = [[UIImage alloc] initWithData:obj.object];
                } else {
                    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:true] forKey:(id)kCGImageSourceShouldCache];
                    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)obj.object, nil);
                    if (source != nil) {
                        CGImageRef cgImage = CGImageSourceCreateImageAtIndex(source, 0, (__bridge CFDictionaryRef)dict);
                        dataImage = [[UIImage alloc] initWithCGImage:cgImage];
                        CGImageRelease(cgImage);
                        CFRelease(source);
                    }
                }
                
                if (dataImage != nil && (location & CacheMemory))
                    [self cacheImage:dataImage withData:nil url:url cacheLocation:CacheMemory];
            }
        }
        __SYNCHRONIZED_END(_dataMemoryCache);
        
        if (dataImage != nil)
            return dataImage;
        
        __block UIImage *diskImageResult = nil;
        dispatch_sync([Cache diskCacheQueue], ^ {
                          NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:true] forKey:(id)kCGImageSourceShouldCache];
                          
                          NSURL *realUrl = [[NSURL alloc] initFileURLWithPath:[_diskCachePath stringByAppendingPathComponent:md5String(url)]];
                          CGImageSourceRef source = CGImageSourceCreateWithURL((__bridge CFURLRef)realUrl, NULL);
                          if (source != nil) {
                              CGImageRef cgImage = CGImageSourceCreateImageAtIndex(source, 0, (__bridge CFDictionaryRef)dict);
                              
                              UIImage *diskImage = [UIImage imageWithCGImage:cgImage];
                              
                              if (diskImage != nil && (location & CacheMemory)) {
                                  [self cacheImage:diskImage withData:nil url:url cacheLocation:CacheMemory];
                              }
                              
                              if (diskImage != nil) {
                                  diskImageResult = diskImage;
                                  
                                  if (diskImage != nil) {
                                      dispatch_async([Cache diskCacheQueue], ^{
                                                         NSData *data = [[NSData alloc] initWithContentsOfURL:realUrl];
                                                         if (data != nil) {
                                                             [self cacheCompressedObject:data url:url reentrant:false];
                                                         }
                                                     });
                                  }
                              }
                              
                              CGImageRelease(cgImage);
                              CFRelease(source);
                          }
                      });
        image = diskImageResult;
        return image;
    }
    
    return nil;
}

- (void) clearDiskCache {
    dispatch_async([Cache diskCacheQueue], ^{
                       NSDirectoryEnumerator* en = [cacheFileManager enumeratorAtPath:_diskCachePath];
                       NSError* error = nil;
                       
                       int removedCount = 0;
                       int failedCount = 0;
                       
                       NSString* file;
                       while (file = [en nextObject]) {
                           if ([cacheFileManager removeItemAtPath:[_diskCachePath stringByAppendingPathComponent:file] error:&error])
                               removedCount++;
                           else
                               failedCount++;
                       }
                       
                   });

}

- (UIImage *)cacheThumbnail:(UIImage *)image url:(NSString *)url cacheSizeType:(CGSize)cacheSize {
 
    if (image != nil && image.size.height >0 ) {
        int sideWidth = 32;
        int sideHeight = 32;
        if (CGSizeEqualToSize(CGSizeZero, cacheSize) == false){
            sideWidth = cacheSize.width;
            sideHeight = cacheSize.height;
        } else {
            float ratio = image.size.width / image.size.height;
            sideHeight = sideWidth / ratio;
        }
        
        
        if (!IsRetina()) {
            sideWidth *= 1.5;
            sideHeight *= 1.5;
        }
        
        int size = (int)(sideWidth * sideHeight * 4);
        if (image.size.width > sideWidth || image.size.height > sideHeight) {
            image = __ScaleImage(image, CGSizeMake(sideWidth, sideHeight));
        }

        dispatch_block_t block = ^{
            
            CacheObject *obj = [_thumbnailCache objectForKey:url];
            if (obj != nil) {
                _thumbnailCacheSize -= obj.size;
                obj.date = CFAbsoluteTimeGetCurrent();
                obj.object = image;
                obj.size = size;
                _thumbnailCacheSize += size;
            } else {
                [_thumbnailCache setObject:[[CacheObject alloc] initWithObject:image size:size] forKey:url];
                _thumbnailCacheSize += size;
            }
            
            if (_thumbnailCacheSize >= _thumbnailMemoryLimit + _thumbnailMemoryLimitInternal)
                [self freeThumbnailCache:_thumbnailMemoryLimit];
        };
        if ([NSThread isMainThread])
            block();
        else
            dispatch_async(dispatch_get_main_queue(), block);
    }
    return  image;
}

- (UIImage *)cachedThumbnail:(NSString *)url {
    UIImage *image = nil;
    __block UIImage *blockImage = nil;
    dispatch_block_t block = ^{
        CacheObject *obj = [_thumbnailCache objectForKey:url];
        if (obj != nil) {
            obj.date = CFAbsoluteTimeGetCurrent();
            blockImage = obj.object;
        }
    };
    if ([NSThread isMainThread])
        block();
    else
        dispatch_async(dispatch_get_main_queue(), block);
    image = blockImage;
    if (image != nil)
        return image;
    
    return nil;
}

- (void) clearCacheForUrl:(NSString *)url {
    [self removeFromMemoryCache:url matchEnd:true];
    [self removeFromDiskCache:url];
}

- (void) clearThumbCache {
    dispatch_block_t block = ^{
        [_thumbnailCache removeAllObjects];
        self.thumbnailCacheSize = 0;
    };
    
    if ([NSThread isMainThread])
        block();
    else
        dispatch_async(dispatch_get_main_queue(), block);
    __SYNCHRONIZED_BEGIN(_dataMemoryCache);
    [_dataMemoryCache removeAllObjects];
    _dataMemoryCacheSize = 0;
    __SYNCHRONIZED_END(_dataMemoryCache);
}

- (void)freeThumbnailCache:(NSUInteger)targetSize {
    dispatch_block_t block = ^{
        if (_thumbnailCacheSize > targetSize) {
            //int sizeBefore = _thumbnailCacheSize;
            NSArray *sortedKeys = [_thumbnailCache keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2)
                                   {
                                       return (((CacheObject *)obj1).date < ((CacheObject *)obj2).date) ? NSOrderedAscending : NSOrderedDescending;
                                   }];
            for (int i = 0; i < sortedKeys.count && _thumbnailCacheSize > targetSize; i++) {
                NSString *key = [sortedKeys objectAtIndex:i];
                CacheObject *obj = [_thumbnailCache objectForKey:key];
                _thumbnailCacheSize -= obj.size;
                if (_thumbnailCacheSize < 0)
                    _thumbnailCacheSize = 0;
                [_thumbnailCache removeObjectForKey:key];
            }
        }
    };
    if ([NSThread isMainThread])
        block();
    else
        dispatch_async(dispatch_get_main_queue(), block);
}

- (void)removeFromMemoryCache:(NSString *)url matchEnd:(bool)matchEnd {
    if (url == nil)
        return;
    
    dispatch_block_t block = ^{
        CacheObject *obj = [_memoryCache objectForKey:url];
        if (obj != nil) {
            self.memoryCacheSize -= (int)obj.size;
            if (self.memoryCacheSize < 0)
                self.memoryCacheSize = 0;
            [_memoryCache removeObjectForKey:url];
        }
        
        if (matchEnd) {
            NSMutableArray *removeKeys = [[NSMutableArray alloc] init];
            
            [_memoryCache enumerateKeysAndObjectsUsingBlock:^(NSString *key, __unused id obj, __unused BOOL *stop) {
                 if ([key hasSuffix:url])
                     [removeKeys addObject:key];
             }];
            
            [_memoryCache removeObjectsForKeys:removeKeys];
        }
        
        CacheObject *dataCacheObj = [_dataMemoryCache objectForKey:url];
        if (dataCacheObj != nil) {
            _dataMemoryCacheSize -= dataCacheObj.size;
            if (_dataMemoryCacheSize < 0)
                _dataMemoryCacheSize = 0;
            [_dataMemoryCache removeObjectForKey:url];
        }
    };
    if ([NSThread isMainThread])
        block();
    else
        dispatch_sync(dispatch_get_main_queue(), block);
}

- (void)removeFromDiskCache:(NSString *)url {
    dispatch_async([Cache diskCacheQueue], ^{
                       [cacheFileManager removeItemAtPath:[_diskCachePath stringByAppendingPathComponent:md5String(url)] error:nil];
                   });
}

- (void) clearMemoryCache {
    dispatch_block_t block = ^{
        [_memoryCache removeAllObjects];
        self.memoryCacheSize = 0;
    };
    if ([NSThread isMainThread])
        block();
    else
        dispatch_async(dispatch_get_main_queue(), block);
    
    __SYNCHRONIZED_BEGIN(_dataMemoryCache);
    [_dataMemoryCache removeAllObjects];
    _dataMemoryCacheSize = 0;
    __SYNCHRONIZED_END(_dataMemoryCache);
    [self clearThumbCache];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
