//
//  CollectionCell.h
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DhtViewModel;
@class DhtViewStorage;
@protocol CollectionRelativeBoundsObserver <NSObject>

- (void)relativeBoundsUpdated:(id)cell bounds:(CGRect)bounds;

@end
@interface CollectionCell : UICollectionViewCell
{
@public
    bool _needsRelativeBoundsUpdateNotifications;
}

@property (nonatomic, strong) id boundItem;
- (void)relativeBoundsUpdated:(CGRect)bounds;
- (void)setEditing:(bool)editing animated:(bool)__unused animated viewStorage:(DhtViewStorage *)__unused viewStorage;
- (UIView *)contentViewForBinding;
@end
