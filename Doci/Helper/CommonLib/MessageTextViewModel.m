//
//  MessageTextViewModel.m
//  MtoM
//
//  Created by nguyen van dung on 12/15/15.
//  Copyright © 2015 Framgia. All rights reserved.
//


#import "MessageTextViewModel.h"
#import "DhtDoubleTapGesture.h"
#import "CommonAsset.h"
#import "DhtImageViewModel.h"
#import "DhtFlatteningViewModel.h"
#import "DhtFlatteningView.h"
#import "DhtTextViewModel.h"
#import "DhtTextViewActionViewModel.h"
#import "DhtButtonViewModel.h"
#import "DhtButtonView.h"
#import "MessageClone.h"
#import "Utils.h"
#import "DhtActivityViewModel.h"
#import "DhtActivityView.h"
#import "MessageTextView.h"
#import "ReusableLabel.h"
#import "DhtDoubleTapGesture.h"
#import "DhtImageView.h"

@interface MessageTextViewModel()<DhtDoubleTapGestureDelegate, UIGestureRecognizerDelegate> {
    DhtTextViewModel *_authorNameViewModel;
    DhtImageViewModel   *_avatarViewModel;
    DhtTextViewModel  *_timeViewModel;
    DhtTextViewModel  *_statusViewModel;
    DhtButtonViewModel  *_sendFailButtonViewModel;
    DhtFlatteningViewModel  *_flattenModel;
    DhtActivityViewModel *_activityModel;
    NSString *_avatarUrl;
    bool _incoming;
    bool _editing;
    MessageStatus _status;
    DhtDoubleTapGesture *_boundDoubleTapRecognizer;
    NSArray *_currentLinkSelectionViews;
    NSString *linkAtPoint;
}
@end
@implementation MessageTextViewModel

- (instancetype) initWithMessage:(MessageClone *)msg context:(DhtViewContext *)context {
    self = [super init];
    if (self) {
        self.viewContext = context;
        [self setHasNoView];
        _status = msg.status;
        _incoming = msg.incoming;
        _avatarUrl = context.needShowRealAvatar ? msg.avatarUrl : @"";
        _localIdentifier = msg.localIdentifier;
        //background model
        _backgroundViewModel = [[MessageTextBackgroundViewModel alloc] initWithType:(msg.incoming)? TextMessageBackgroundIncoming : TextMessageBackgroundOutgoing];
        _backgroundViewModel.blendMode = kCGBlendModeNormal;
        _backgroundViewModel.skipDrawInContext = true;
        //text container model
        _flattenModel = [[DhtFlatteningViewModel alloc] initWithContext:context];
        [self addSubmodel:_backgroundViewModel];
        [self addSubmodel:_flattenModel];
        ReusableLabelLayout flag = ReusableLabelLayoutMultiline | ReusableLabelLayoutHighlightLinks;
        //add author name
        NSString *authorname = [msg authorName];
        if ([authorname length] > 0) {
            _authorNameViewModel = [[DhtTextViewModel alloc] initWithText:authorname
                                                        layoutFlags: flag
                                                               font:(CTFontRef)[UIFont systemFontOfSize:13]
                                                          textColor:[UIColor blackColor]
                                                            maxLine:0 willTruncateTail:NO];
            [self addSubmodel:_authorNameViewModel];
        }
        //Add avatar model
        if (_incoming) {
            _avatarViewModel = [[DhtImageViewModel alloc] initWithImage:[self defaultAvatar]];
            _avatarViewModel.skipDrawInContext = false;
            [self addSubmodel:_avatarViewModel];
        } else {
            [self addStatusViewModel:[msg statuString]];
        }
        if ([msg timeString].length > 0) {
            //time view model
            _timeViewModel = [[DhtTextViewModel alloc] initWithText:[msg timeString]
                                                        layoutFlags: flag
                                                               font:(CTFontRef)[UIFont systemFontOfSize:12]
                                                          textColor:[UIColor blackColor]
                                                            maxLine:0 willTruncateTail:NO];
            [self addSubmodel:_timeViewModel];
        }
        
        if ([msg.content length] > 0){
            _textViewModel = [[DhtTextViewModel alloc] initWithText:msg.content
                                                        layoutFlags: flag
                                                               font:(CTFontRef)[UIFont systemFontOfSize:14]
                                                          textColor:(_incoming) ? [UIColor blackColor] :[UIColor whiteColor]
                                                            maxLine:0 willTruncateTail:NO];
            
            [_flattenModel addSubmodel:_textViewModel];
        }
        
        //For checking state of owner message
        if (!_incoming) {
            if (_status == MessageStatusFailed) {
                [self addSubmodel: [self sendFailModel]];
            } else if (_status == MessageStatusSending) {;
                [self  addSubmodel:[self activityModel]];
            }
        }
    }
    return self;
}

- (DhtButtonViewModel *) sendFailModel {
    if (_sendFailButtonViewModel == nil) {
        _sendFailButtonViewModel = [[DhtButtonViewModel alloc] init];
        UIImage *failImage = [[CommonAsset instance] sentFailButtonImage];
        _sendFailButtonViewModel.backgroundImage = failImage;
        _sendFailButtonViewModel.skipDrawInContext = false;
        _sendFailButtonViewModel.extendedEdges = UIEdgeInsetsMake(30, 30, 30, 30);
    }
    return _sendFailButtonViewModel;
}

- (void) removeActivityModelIfNeed:(DhtViewStorage *)viewStorage {
    if (_activityModel) {
        [self removeSubmodel:_activityModel viewStorage:viewStorage];
        _activityModel = nil;
    }
}

- (void) removeFailButtonIfNeed:(DhtViewStorage *)viewStorage {
    if (_sendFailButtonViewModel) {
        [self removeSubmodel:_sendFailButtonViewModel viewStorage:viewStorage];
        _sendFailButtonViewModel = nil;
    }
}

- (DhtActivityViewModel *)activityModel {
    if (!_activityModel) {
        _activityModel = [[DhtActivityViewModel alloc] init];
        _activityModel.frame = CGRectMake(0, 0, 30, 30);
        _activityModel.skipDrawInContext = false;
    }
    return _activityModel;
}

- (void) relativeBoundsUpdated:(CGRect)bounds {
    [super relativeBoundsUpdated:bounds];
    [_flattenModel updateSubmodelContentsForVisibleRect:CGRectOffset(bounds, -_flattenModel.frame.origin.x, -_flattenModel.frame.origin.y)];
}


- (UIImage *)defaultAvatar {
    return self.viewContext.defaultAvatar;
}

- (void)setupBackground {
    if (_backgroundViewModel.boundView) {
        _backgroundViewModel.boundView.alpha = 1.0;
        _backgroundViewModel.boundView.userInteractionEnabled = true;
        [_backgroundViewModel.boundView addGestureRecognizer:_boundDoubleTapRecognizer];
        [(UIImageView *)_backgroundViewModel.boundView setImage:_backgroundViewModel.image];
    }
}

- (void)addActionToBackgroundView {
    _boundDoubleTapRecognizer = [[DhtDoubleTapGesture alloc] initWithTarget:self action:@selector(messageDoubleTapGesture:)];
    _boundDoubleTapRecognizer.delegate = self;
    UIView *backgroundView = [_backgroundViewModel boundView];
    backgroundView.userInteractionEnabled = true;
    [backgroundView addGestureRecognizer:_boundDoubleTapRecognizer];
}

- (void) bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage {
    container.userInteractionEnabled = true;
    [super  bindViewToContainer:container viewStorage:viewStorage];
    if (_avatarViewModel.boundView) {
        DhtImageView *imageView = (DhtImageView *)_avatarViewModel.boundView;
        imageView.layer.borderWidth = 0;
        imageView.layer.cornerRadius = _avatarViewModel.frame.size.width / 2;
        imageView.layer.masksToBounds = true;
        if (_incoming) {
            if ([_avatarUrl length] > 0 ) {
                [imageView loadImageAtPath:_avatarUrl placeholder:[self defaultAvatar]];
            } else {
                imageView.image = [self defaultAvatar];
            }
        } else {
            imageView.image = nil;
        }
    }
    [self addActionToBackgroundView];
    [self setupBackground];
    [self addFailButtonAction];
}

- (void)messageDoubleTapGesture:(DhtDoubleTapGesture *)recognizer {
    if (recognizer.state != UIGestureRecognizerStateBegan) {
        [self clearLinkSelection];
        if (recognizer.state == UIGestureRecognizerStateRecognized) {
            if (recognizer.longTapped) {
                if (linkAtPoint != nil) {
                    if (self.viewContext && self.viewContext.openURlAction) {
                        self.viewContext.openURlAction(linkAtPoint);
                    }
                }  else if (self.viewContext.showMenuAction) {
                        self.viewContext.showMenuAction(_localIdentifier);
                }
            }
            else if (recognizer.doubleTapped) {
                if (self.viewContext.showMenuAction) {
                    self.viewContext.showMenuAction(_localIdentifier);
                }
            }
            else if (linkAtPoint != nil) {
                if (self.viewContext && self.viewContext.openURlAction) {
                    self.viewContext.openURlAction(linkAtPoint);
                }
            }
        }
    }
}

- (void) removeStatusViewIfNeed:(DhtViewStorage *)viewStorage {
    if (_statusViewModel) {
        [self removeSubmodel:_statusViewModel viewStorage:viewStorage];
        _statusViewModel = nil;
    }
}

- (void)addStatusViewModel:(NSString *)status {
    if (!_incoming) {
        if (status.length > 0) {
            ReusableLabelLayout flag = ReusableLabelLayoutMultiline | ReusableLabelLayoutHighlightLinks;
            _statusViewModel = [[DhtTextViewModel alloc] initWithText:status
                                                          layoutFlags: flag
                                                                 font:(CTFontRef)[UIFont systemFontOfSize:12]
                                                            textColor:[UIColor blackColor]
                                                              maxLine:0 willTruncateTail:NO];
            [self addSubmodel:_statusViewModel];
        }
    }
}

- (void) addFailButtonAction {
    if(_sendFailButtonViewModel.boundView) {
        _sendFailButtonViewModel.boundView.userInteractionEnabled = true;
        [(UIButton *)_sendFailButtonViewModel.boundView addTarget: self action: @selector(onClickFailButton) forControlEvents: UIControlEventTouchUpInside];
    }
}

- (void) removeFailButtonAction {
    if(_sendFailButtonViewModel.boundView) {
        [(UIButton *)_sendFailButtonViewModel.boundView removeTarget: self action: @selector(onClickFailButton) forControlEvents: UIControlEventTouchUpInside];
    }
}

- (void) onClickFailButton {
    if(self.viewContext && self.viewContext.failAction) {
        self.viewContext.failAction(_localIdentifier);
    }
}


- (void) unbindView:(DhtViewStorage *)viewStorage {
    [self removeFailButtonAction];
    [self clearLinkSelection];
    UIView *backgroundView = [_backgroundViewModel boundView];
    [backgroundView removeGestureRecognizer:_boundDoubleTapRecognizer];
    _boundDoubleTapRecognizer.delegate = nil;
    _boundDoubleTapRecognizer = nil;
    [super unbindView:viewStorage];
}

- (void)updateItemInfo:(GenericInfo *)info viewStorage:(DhtViewStorage *)viewStorage {
    MessageClone *msg = (MessageClone *)info;
    if (msg != nil) {
        _localIdentifier = msg.localIdentifier;
        if (_status != msg.status) {
            _status = msg.status;
            //sent fail
            if (_status == MessageStatusFailed) {
                // need remove activity
                [self removeActivityModelIfNeed:viewStorage];
                //add send fiel button
                if (_sendFailButtonViewModel == nil) {
                    [self addSubmodel:[self sendFailModel]];
                    [_sendFailButtonViewModel bindViewToContainer:self.boundView viewStorage:viewStorage];
                    [self addFailButtonAction];
                }
            } else if (_status == MessageStatusSending) {
                //remove send failt button
                [self removeFailButtonIfNeed:viewStorage];
                if (_activityModel == nil) {
                    [self addSubmodel:[self activityModel]];
                    [_activityModel bindViewToContainer:self.boundView viewStorage:viewStorage];
                }
            } else {
                //1. remove activity if need
                [self removeActivityModelIfNeed:viewStorage];
                //2. remove fail button if need
                [self removeFailButtonIfNeed:viewStorage];
                [self removeStatusViewIfNeed:viewStorage];
                [self addStatusViewModel:[msg statuString]];
                [_statusViewModel bindViewToContainer:self.boundView viewStorage:viewStorage];
            }
            [self layoutForContainerSize:CGSizeMake(self.frame.size.width, 0.0f)];
        }
        [_flattenModel setNeedsSubmodelContentsUpdate];
        [_flattenModel updateSubmodelContentsIfNeeded];
    }
}

- (void)setTemporaryHighlighted:(bool)temporaryHighlighted viewStorage:(DhtViewStorage *)__unused viewStorage {
    if (temporaryHighlighted)
        _backgroundViewModel.boundView.alpha = 0.8;
    else
        _backgroundViewModel.boundView.alpha = 1.0;
}

- (CGRect)effectiveContentFrame {
    return _backgroundViewModel.frame;
}

- (CGSize)contentSizeForContainerSize:(CGSize)containerSize needsContentsUpdate:(bool *)needsContentsUpdate
{
    bool updateContents = [_textViewModel layoutNeedsUpdatingForContainerSize:containerSize];
    if (updateContents)
        [_textViewModel layoutForContainerSize:containerSize];
    
    if (needsContentsUpdate != NULL)
        *needsContentsUpdate = updateContents;
    
    return _textViewModel.frame.size;
    
}

- (void) layoutForContainerSize:(CGSize)containerSize {
    //Padding left and right
    CGFloat paddingLeft = 12.0f;
    //default avatar size with
    CGFloat avatarWidth = 38.0f;
    CGFloat topSpacing = 4.0f;
    CGFloat bottomSpacing = 4.0f;
    CGFloat stateViewSize = 15.0f;
    CGFloat statusWidth = 50.0f;
    //Calculate content text size
    CGSize contentContainerSize = CGSizeZero;
    //If incomming we will have avatar
    if (_incoming) {
        contentContainerSize = CGSizeMake(containerSize.width - avatarWidth - 50 - paddingLeft * 2 - statusWidth,
                                          0);
    } else {
        contentContainerSize = CGSizeMake(containerSize.width - paddingLeft * 2 - 50 - statusWidth,
                                          0);
    }
    bool updateContents = false;
    CGSize contentSize = CGSizeZero;
    if (_textViewModel) {
        contentSize =  [self contentSizeForContainerSize:contentContainerSize needsContentsUpdate:&updateContents];
    }
    
    //time
    CGRect authorRect =  CGRectZero;
    if (_authorNameViewModel) {
        [_authorNameViewModel layoutForContainerSize:containerSize];
        authorRect = [_authorNameViewModel frame];
        authorRect.size.width += 10.0;
    }
    contentSize.height += authorRect.size.height;
    CGFloat paddingRight = paddingLeft;
    if (!_incoming && (_status == MessageStatusFailed)) {
        paddingRight += stateViewSize;
    }
    

    //Apply frame for background view model
    CGFloat backgroundWidth = MAX(60.0f, contentSize.width  + 25.0f);
    CGFloat backgroundHeight = MAX(44.0f, contentSize.height + topSpacing + bottomSpacing);
    
    CGRect backgroundFrame = CGRectMake(
                                        _incoming ? avatarWidth + 2 * paddingLeft : ( containerSize.width - backgroundWidth - paddingRight),
                                        topSpacing + authorRect.size.height ,
                                        backgroundWidth,
                                        backgroundHeight
                                        );
    _backgroundViewModel.frame = backgroundFrame;
    //text contents
    CGFloat contentOriginX = backgroundFrame.origin.x + (_incoming ? 14 : 8);
    CGFloat contentOriginY = authorRect.size.height;
    contentOriginY +=  (backgroundHeight == 44.0f) ? (backgroundHeight / 2 - contentSize.height / 2) : (topSpacing + 2);
    
    CGFloat contentWidth = MAX(32.0f, contentSize.width + 2 + (_incoming ? 0.0f : 5.0f));
    CGRect contentRect = CGRectMake(
                                    contentOriginX ,
                                    contentOriginY ,
                                    MAX(contentWidth, authorRect.size.width), MAX(contentSize.height + 5, 30.0f)
                                    );
    
    _flattenModel.frame = contentRect;
    
    CGFloat originY = 0;
    CGRect timeRect = CGRectZero;
    //time
    if (_timeViewModel) {
        [_timeViewModel layoutForContainerSize:containerSize];
        timeRect = _timeViewModel.frame;
        if (_incoming) {
            timeRect.origin.x = backgroundFrame.origin.x + backgroundFrame.size.width + 5.0f;
        } else {
            timeRect.origin.x = backgroundFrame.origin.x - _timeViewModel.frame.size.width - 5.0f;
        }
        if (originY == 0) {
            originY = (backgroundFrame.origin.y + backgroundFrame.size.height- timeRect.size.height);
        }
        timeRect.origin.y = originY;
        timeRect.size.width += 10;
        _timeViewModel.frame = timeRect;
    }
    
    if (_statusViewModel) {
        [_statusViewModel layoutForContainerSize:containerSize];
        CGRect statusRect = _statusViewModel.frame;
        if (_incoming) {
            statusRect.origin.x = backgroundFrame.origin.x + backgroundFrame.size.width + 5.0f;
        } else {
            statusRect.origin.x = backgroundFrame.origin.x - _statusViewModel.frame.size.width - 5.0f;
        }
        statusRect.origin.y = timeRect.origin.y - statusRect.size.height;
        statusRect.size.width += 10;
        _statusViewModel.frame = statusRect;
    }
    
    self.frame = CGRectMake(0, 0, containerSize.width, backgroundFrame.size.height + topSpacing + 16 + authorRect.size.height);
    
    CGRect stateRect = CGRectMake(self.frame.size.width - stateViewSize - paddingLeft / 2,
                                  contentRect.origin.y + contentRect.size.height / 2 - stateViewSize / 2,
                                  stateViewSize,
                                  stateViewSize);
    authorRect.origin.x = (_incoming) ? contentOriginX - 10 : (containerSize.width - paddingRight - authorRect.size.width);
    if (_sendFailButtonViewModel) {
        _sendFailButtonViewModel.frame = stateRect;
        authorRect.origin.x -= stateRect.size.width;
    }
    
    authorRect.origin.y = 0;
    [_authorNameViewModel setFrame:authorRect];
    if (_activityModel) {
        _activityModel.frame = CGRectMake(paddingLeft / 2,
                                          backgroundFrame.origin.y + backgroundFrame.size.height/ 2 - stateViewSize / 2,
                                          stateViewSize,
                                          stateViewSize);;
    }
//    //avatar
    CGRect avatarRect = CGRectZero;
    if (_avatarViewModel) {
        avatarRect = CGRectMake(paddingLeft, (backgroundFrame.origin.y + backgroundFrame.size.height) - avatarWidth, avatarWidth, avatarWidth);
        _avatarViewModel.frame =  avatarRect;
    }
    [super layoutForContainerSize:containerSize];
}

#pragma --mark
- (void)clearLinkSelection {
    for (UIView *linkView in _currentLinkSelectionViews) {
        [linkView removeFromSuperview];
    }
    _currentLinkSelectionViews = nil;
}

- (void)updateLinkSelection:(CGPoint)point
{
    if ([_flattenModel boundView] != nil)
    {
        [self clearLinkSelection];
        
        NSArray *regionData = nil;
        NSString *link = [_textViewModel linkAtPoint:CGPointMake(point.x - _textViewModel.frame.origin.x, point.y - _textViewModel.frame.origin.y) regionData:&regionData];
        linkAtPoint = link;
        if (link != nil)
        {
            CGRect topRegion = regionData.count > 0 ? [regionData[0] CGRectValue] : CGRectZero;
            CGRect middleRegion = regionData.count > 1 ? [regionData[1] CGRectValue] : CGRectZero;
            CGRect bottomRegion = regionData.count > 2 ? [regionData[2] CGRectValue] : CGRectZero;
            
            UIImageView *topView = nil;
            UIImageView *middleView = nil;
            UIImageView *bottomView = nil;
            
            UIImageView *topCornerLeft = nil;
            UIImageView *topCornerRight = nil;
            UIImageView *bottomCornerLeft = nil;
            UIImageView *bottomCornerRight = nil;
            
            NSMutableArray *linkHighlightedViews = [[NSMutableArray alloc] init];
            
            topView = [[UIImageView alloc] init];
            middleView = [[UIImageView alloc] init];
            bottomView = [[UIImageView alloc] init];
            
            topCornerLeft = [[UIImageView alloc] init];
            topCornerRight = [[UIImageView alloc] init];
            bottomCornerLeft = [[UIImageView alloc] init];
            bottomCornerRight = [[UIImageView alloc] init];
            
            if (topRegion.size.height != 0)
            {
                topView.hidden = false;
                topView.frame = topRegion;
                topView.image = [[CommonAsset instance] messageLinkFull];
            }
            else
            {
                topView.hidden = true;
                topView.frame = CGRectZero;
            }
            
            if (middleRegion.size.height != 0)
            {
                middleView.hidden = false;
                middleView.frame = middleRegion;
                middleView.image = [[CommonAsset instance] messageLinkFull];
            }
            else
            {
                middleView.hidden = true;
                middleView.frame = CGRectZero;
            }
            
            if (bottomRegion.size.height != 0)
            {
                bottomView.hidden = false;
                bottomView.frame = bottomRegion;
                bottomView.image = [[CommonAsset instance] messageLinkFull];
            }
            else
            {
                bottomView.hidden = true;
                bottomView.frame = CGRectZero;
            }
            
            topCornerLeft.hidden = true;
            topCornerRight.hidden = true;
            bottomCornerLeft.hidden = true;
            bottomCornerRight.hidden = true;
            
            if (topRegion.size.height != 0 && middleRegion.size.height != 0)
            {
                if (topRegion.origin.x == middleRegion.origin.x)
                {
                    topCornerLeft.hidden = false;
                    topCornerLeft.image = [[CommonAsset instance] messageLinkCornerLR];
                    topCornerLeft.frame = CGRectMake(topRegion.origin.x, topRegion.origin.y + topRegion.size.height - 3.5f, 4, 7);
                }
                else if (topRegion.origin.x < middleRegion.origin.x + middleRegion.size.width - 3.5f)
                {
                    topCornerLeft.hidden = false;
                    topCornerLeft.image = [[CommonAsset instance] messageLinkCornerBT];
                    topCornerLeft.frame = CGRectMake(topRegion.origin.x - 3.5f, topRegion.origin.y + topRegion.size.height - 4, 7, 4);
                }
                
                if (topRegion.origin.x + topRegion.size.width == middleRegion.origin.x + middleRegion.size.width)
                {
                    topCornerRight.hidden = false;
                    topCornerRight.image = [[CommonAsset instance] messageLinkCornerRL];
                    topCornerRight.frame = CGRectMake(topRegion.origin.x + topRegion.size.width - 4, topRegion.origin.y + topRegion.size.height - 3.5f, 4, 7);
                }
                else if (topRegion.origin.x + topRegion.size.width < middleRegion.origin.x + middleRegion.size.width - 3.5f)
                {
                    topCornerRight.hidden = false;
                    topCornerRight.image = [[CommonAsset instance] messageLinkCornerBT];
                    topCornerRight.frame = CGRectMake(topRegion.origin.x + topRegion.size.width - 3.5f, topRegion.origin.y + topRegion.size.height - 4, 7, 4);
                }
                else if (bottomRegion.size.height == 0 && topRegion.origin.x < middleRegion.origin.x + middleRegion.size.width - 3.5f && topRegion.origin.x + topRegion.size.width > middleRegion.origin.x + middleRegion.size.width + 3.5f)
                {
                    topCornerRight.hidden = false;
                    topCornerRight.image = [[CommonAsset instance] messageLinkCornerTB];
                    topCornerRight.frame = CGRectMake(middleRegion.origin.x + middleRegion.size.width - 3.5f, middleRegion.origin.y, 7, 4);
                }
            }
            
            if (middleRegion.size.height != 0 && bottomRegion.size.height != 0)
            {
                if (middleRegion.origin.x == bottomRegion.origin.x)
                {
                    bottomCornerLeft.hidden = false;
                    bottomCornerLeft.image = [[CommonAsset instance] messageLinkCornerLR];
                    bottomCornerLeft.frame = CGRectMake(middleRegion.origin.x, middleRegion.origin.y + middleRegion.size.height - 3.5f, 4, 7);
                }
                
                if (bottomRegion.origin.x + bottomRegion.size.width < middleRegion.origin.x + middleRegion.size.width - 3.5f)
                {
                    bottomCornerRight.hidden = false;
                    bottomCornerRight.image = [[CommonAsset instance] messageLinkCornerTB];
                    bottomCornerRight.frame = CGRectMake(bottomRegion.origin.x + bottomRegion.size.width - 3.5f, bottomRegion.origin.y, 7, 4);
                }
            }
            
            if (!topView.hidden)
                [linkHighlightedViews addObject:topView];
            if (!middleView.hidden)
                [linkHighlightedViews addObject:middleView];
            if (!bottomView.hidden)
                [linkHighlightedViews addObject:bottomView];
            
            if (!topCornerLeft.hidden)
                [linkHighlightedViews addObject:topCornerLeft];
            if (!topCornerRight.hidden)
                [linkHighlightedViews addObject:topCornerRight];
            if (!bottomCornerLeft.hidden)
                [linkHighlightedViews addObject:bottomCornerLeft];
            if (!bottomCornerRight.hidden)
                [linkHighlightedViews addObject:bottomCornerRight];
            
            for (UIView *partView in linkHighlightedViews)
            {
                CGRect frame = partView.frame;
                frame.size.height += 5;//extense selection
                partView.frame = CGRectOffset(frame, _textViewModel.frame.origin.x, _textViewModel.frame.origin.y - 15);
                [[_flattenModel boundView] addSubview:partView];
            }
            
            _currentLinkSelectionViews = linkHighlightedViews;
        }
    }
}


- (int)gestureRecognizer:(DhtDoubleTapGesture *)recognizer shouldFailTap:(CGPoint)point {
    if ([_textViewModel linkAtPoint:CGPointMake(point.x - _textViewModel.frame.origin.x, point.y - _textViewModel.frame.origin.y) regionData:NULL] != nil )
        return 3;
    return false;
}

- (void)gestureRecognizer:(DhtDoubleTapGesture *)recognizer didBeginAtPoint:(CGPoint)point {
    [self updateLinkSelection:point];
}

- (void)gestureRecognizerDidFail:(DhtDoubleTapGesture *)recognizer {
    [self clearLinkSelection];
}

- (bool)gestureRecognizerShouldHandleLongTap:(DhtDoubleTapGesture *)recognizer {
    return true;
}

- (void)doubleTapGestureRecognizerSingleTapped:(DhtDoubleTapGesture *)recognizer {
    
}

- (bool)gestureRecognizerShouldFailOnMove:(DhtDoubleTapGesture *)recognizer {
    return false;
}

@end
