//
//  NotificationObject.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 4/28/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol NotificationProtocol <NSObject>
@required
- (NSInteger)objectId;
- (NSString *)notifyMessage;
- (NSString *)title;
@end