//
//  BaseView.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/11/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView
@synthesize viewIdentifier = _viewIdentifier;
@synthesize viewStateIdentifier = _viewStateIdentifier;


#pragma --mark Protocol compatible
- (NSString *)viewIdentifier {
    return _viewIdentifier;
}

- (NSString *)viewStateIdentifier {
    return _viewStateIdentifier;
}

- (void)setViewIdentifier:(NSString *)viewIdentifier {
    _viewIdentifier = viewIdentifier;
}

- (void)setViewStateIdentifier:(NSString *)viewStateIdentifier {
    _viewStateIdentifier = viewStateIdentifier;
}

/*
 - Prepare to reuse
 */
- (void)willBecomeRecycled {
    
}
@end
