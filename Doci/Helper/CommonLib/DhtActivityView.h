//
//  DhtActivityView.h
//  Fonext
//
//  Created by dungnv9 on 3/17/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "BaseView.h"

@interface DhtActivityView : BaseView
- (void) startAcitivty;
- (void) stopActivity;
- (void) setAlphaForViews:(CGFloat)alpha;
@end
