//
//  DhtViewStorage.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/12/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewProtocol.h"

@interface DhtViewStorage : NSObject

- (UIView<BaseViewProtocol> *)dequeueViewWithIdentifier:(NSString *)identifier viewStateIdentifier:(NSString *)viewStateIdentifier;
- (void)enqueueView:(UIView<BaseViewProtocol> *)view;
- (void)allowResurrectionForOperations:(dispatch_block_t)block;
- (void) clear;
@end
