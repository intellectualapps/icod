//
//  BaseView.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/11/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewProtocol.h"

@interface BaseView : UIView<BaseViewProtocol>

@property (nonatomic, strong) NSString *viewIdentifier;
@property (nonatomic, strong) NSString *viewStateIdentifier;

@end
