//
//  DHTDateHeaderView.h
//  ChatPro
//
//  Created by dungnv9 on 3/10/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"

@interface DhtDateHeaderView : BaseView<BaseViewProtocol>

- (void)setDate:(int)date isShowFull:(bool)isShowFull;
- (void)updateAssets;
+ (void)drawDate:(int)date forContainerWidth:(CGFloat)containerWidth inContext:(CGContextRef)context andBindBackgroundToContainer:(UIView *)__unused backgroundContainer atPosition:(CGPoint)__unused position;
@end
