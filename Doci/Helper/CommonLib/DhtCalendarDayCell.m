//
//  DhtCalendarDayCell.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtCalendarDayCell.h"
#import "DayInfo.h"
#import "NSDate+Extension.h"
#import "DhtCalendarUtils.h"

@interface DhtCalendarDayCell()
{
}
@property (strong, nonatomic) CALayer *separatorLayer;
@property (strong, nonatomic) CALayer *separatorLayerVertical;
@property (strong, nonatomic) CALayer *bottomLayer;
@property (nonatomic, strong) UILabel   *label;
@property (nonatomic, strong) UIView *selectionView;
@property (nonatomic, strong) UIView *supplementView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIView *todayBgView;
@end
@implementation DhtCalendarDayCell

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = true;
        _backgroundView = [[UIView alloc] initWithFrame: [self backgroundRect]];
        [self insertSubview:_backgroundView atIndex:0];
        _label = [[UILabel alloc] initWithFrame:CGRectZero];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.font = [UIFont systemFontOfSize:13];
        _label.textColor = [UIColor redColor];
        _todayBgView = [[UIView alloc] initWithFrame:CGRectZero];
        _todayBgView.backgroundColor = [UIColor blueColor];
        _todayBgView.layer.cornerRadius = 4.0;
        _todayBgView.layer.masksToBounds = true;
        [self addSubview:_label];
        _separatorLayer = [[CALayer alloc] init];
        _separatorLayer.borderColor = [self selectedBackgroundColor].CGColor;
        _separatorLayer.borderWidth = 1.5;
        [_separatorLayer setBackgroundColor:[UIColor clearColor].CGColor];
        [_separatorLayer setFrame:CGRectMake(0.0, 0.0f, self.bounds.size.width, self.bounds.size.height)];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnContentView)];
        [self addGestureRecognizer:tapGesture];
        _supplementView = [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:_supplementView];
        _selectionView = [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:_selectionView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect bound = self.bounds;
    _backgroundView.frame = [self backgroundRect];
    CGSize size = CGSizeZero;
    if (self.label.text != nil && self.label.text.length > 0) {
        size = [DhtCalendarUtils sizeForText: self.label.text
                                   boundSize:bound.size
                                      option:NSStringDrawingUsesLineFragmentOrigin
                               lineBreakMode: NSLineBreakByClipping
                                        font:[UIFont systemFontOfSize:13]];
    }
    self.label.frame = CGRectMake(5, 5, MAX(15, size.width), size.height);
    _selectionView.frame = bound;
    _supplementView.frame = bound;
    [self roundLabel];
}

- (void)roundLabel {
    self.label.layer.masksToBounds = true;
    [_todayBgView removeFromSuperview];
    self.label.textColor = [UIColor blackColor];
    if([[self.dayInfo.date dateAtStartOfDay] timeIntervalSince1970] == [[[NSDate date] dateAtStartOfDay] timeIntervalSince1970] ) {
        self.label.textColor = [UIColor whiteColor];
        CGRect rect = _label.frame;
        rect.origin.x -= 2;
        rect.origin.y -= 2;
        rect.size.height += 4;
        rect.size.width += 4;
        _todayBgView.frame = rect;
        [self insertSubview:_todayBgView belowSubview:_label];
    }
}


- (UIColor *)selectedBackgroundColor {
    return [UIColor colorWithRed:248.0 / 255.0 green:153.0 / 255.0 blue:5.0 / 255.0 alpha:1.0];
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
}

- (UIView *)contentViewForBinding {
    return nil;
}

- (void)displayContent:(DayInfo *)info {
    _dayInfo = info;
    _dayInfo.cell = self;
    _label.text = [NSString stringWithFormat:@"%ld",(long)[info.date day]];
    _label.textColor = (info.isDayIn) ? [UIColor blackColor] : [UIColor grayColor];
    if (info.isSelected == false) {
        [self removeSelectionView];
    }
    if (info.hasScheduleInfo == NO) {
        [self removeSupplementaryView];
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(supplementViewForBound:)]) {
            UIView *supplementView = [self.delegate supplementViewForBound:self.bounds];
            [self addSupplementaryView:supplementView];
        }
    }
    if (info.isPast || info.isToday) {
        UIColor *bgColor = [UIColor clearColor];
        if (self.delegate && [self.delegate respondsToSelector:@selector(dayInPastBackgroundColor)]) {
            bgColor = [self.delegate dayInPastBackgroundColor];
        }
        _backgroundView.backgroundColor = bgColor;
    } else {
        _backgroundView.backgroundColor = [UIColor clearColor];
    }
    [self setNeedsLayout];
}

- (CGRect)backgroundRect {
    return CGRectMake(1, 1, self.bounds.size.width - 2, self.bounds.size.height - 2);
}

- (void)addSupplementaryView:(UIView *)view {
    if (view) {
        self.dayInfo.hasScheduleInfo = YES;
        view.hidden = false;
        view.frame = _selectionView.bounds;
        [[_supplementView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [_supplementView addSubview:view];
    }
}

- (void)removeSupplementaryView {
    self.dayInfo.hasScheduleInfo = NO;
    [[_supplementView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

- (void)addSelectionView:(UIView *)view {
    if (view) {
        view.hidden = NO;
        view.frame = _selectionView.bounds;
        [[_selectionView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [_selectionView addSubview:view];
    }
}

- (void)removeSelectionView {
    [[_selectionView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

- (void)tappedOnContentView {
    if (self.delegate) {
        [self.delegate didSelectDayView:self];
    }
}
@end
