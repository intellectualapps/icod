//
//  DhtTextViewActionViewModel.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/7/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtTextViewActionViewModel.h"
#import "DhtFlatteningView.h"
#import "DhtFlatteningViewModel.h"
#import "DhtViewContext.h"
#import "DhtTextViewModel.h"
#import "ReusableLabel.h"
#import "DhtDoubleTapGesture.h"
#import "Utils.h"



@interface DhtTextViewActioView : BaseView
{
    
}
@property (nonatomic, strong) UITextView        *textView;
@end


@implementation DhtTextViewActioView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _textView.editable = false;
        _textView.showsHorizontalScrollIndicator = false;
        _textView.showsVerticalScrollIndicator = false;
        _textView.textContainer.lineFragmentPadding = 0;
        _textView.textContainerInset = UIEdgeInsetsZero;
        _textView.backgroundColor = [UIColor clearColor];
        _textView.scrollEnabled = false;
        _textView.contentInset = UIEdgeInsetsZero;
        _textView.scrollIndicatorInsets = UIEdgeInsetsZero;
        _textView.contentOffset = CGPointZero;
        _textView.textContainerInset = UIEdgeInsetsZero;
        _textView.textContainer.lineFragmentPadding = 0;
        _textView.dataDetectorTypes = UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
        NSDictionary *attributes = @{NSForegroundColorAttributeName:[UIColor blueColor],
                                     NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle|NSUnderlinePatternSolid)};
        _textView.linkTextAttributes = attributes;
        [self addSubview:_textView];
    }
    return self;
}

- (void) setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    _textView.frame = CGRectMake(0, 0, bounds.size.width, bounds.size.height);
}

- (void) setFrame:(CGRect)frame {
    [super setFrame:frame];
    _textView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
}

- (void) willBecomeRecycled {
    [super willBecomeRecycled];
    _textView.text  = @"";
    _textView.attributedText = nil;
    _textView.linkTextAttributes = nil;
}

@end

@interface DhtTextViewActionViewModel()
{
    NSString *_description;
    float   _fontSize;
    bool    _isBold;
    UIColor *_textColor;
    NSInteger _maxLine;
    bool   _truncateTail;
    NSArray *_textCheckingReutls;
}
@property (nonatomic, strong) ReusableLabelLayoutData *layoutData;


@end

@implementation DhtTextViewActionViewModel

- (instancetype) initWithText:(NSString *)text
                  viewContext:(DhtViewContext *)context
                     fontSize:(CGFloat)fontSize
                       isBold:(BOOL) isBold
                    textColor:(UIColor *)textColor
                      maxLine:(NSInteger)maxLine
                 truncateTail:(BOOL)isTruncatetail
           textCheckingResult:(NSArray *)textCheckingResults  {
    self = [super init];
    if (self) {
        self.skipDrawInContext = false;
        _description = (text)?text:@"";
        _fontSize = fontSize;
        _isBold = isBold;
        _textColor = textColor;
        _maxLine = maxLine;
        _truncateTail = isTruncatetail;
        _textCheckingReutls = textCheckingResults;
        _modelFlags.viewUserInteractionDisabled = 0;
    }
    return self;
}

- (Class) viewClass {
    return [DhtTextViewActioView class];
}


- (void) bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage {
    [super bindViewToContainer:container viewStorage:viewStorage];
    if ([self boundView]){
        DhtTextViewActioView *actionView = (DhtTextViewActioView *)[self boundView];
        actionView.textView.font = (__bridge UIFont * _Nullable)(CoreTextFontOfNameAndSize(_fontSize, _isBold));
        actionView.textView.text = _description;
        actionView.textView.textColor = _textColor;    }
}

- (void) unbindView:(DhtViewStorage *)viewStorage {
    _layoutData = nil;
    [super unbindView:viewStorage];
}

- (CGSize) calculateSizeForDescription:(CGSize)containerSize{
    _layoutData = [ReusableLabel calculateLayout:_description
                            additionalAttributes:[Utils additionAttributeForUrls:_textCheckingReutls]
                             textCheckingResults:_textCheckingReutls
                                            font:CoreTextFontOfNameAndSize(_fontSize, _isBold)
                                       textColor:_textColor
                                       linkColor: _textColor
                                           frame:CGRectZero
                                      orMaxWidth:containerSize.width
                                           flags:ReusableLabelLayoutMultiline|ReusableLabelLayoutHighlightLinks
                                   textAlignment:NSTextAlignmentLeft
                                        outIsRTL:nil
                         additionalTrailingWidth:0
                                         maxLine:0];
    return CGSizeMake(_layoutData.size.width, _layoutData.size.height);
}


- (void) layoutForContainerSize:(CGSize)containerSize {
    CGSize contentSize = [self calculateSizeForDescription:CGSizeMake(containerSize.width , containerSize.height)];
    self.frame = CGRectMake(0, 0, contentSize.width, contentSize.height);
}

- (NSInteger) numberOfTextLine {
    if (_layoutData) {
        return _layoutData.numberOfLines;
    }
    return 0;
}
@end
