//
//  DhtActivityViewModel.m
//  Fonext
//
//  Created by dungnv9 on 3/17/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtActivityViewModel.h"
#import "DhtActivityView.h"

@implementation DhtActivityViewModel
- (Class) viewClass {
    return [DhtActivityView class];
}

- (void) bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage {
    [super bindViewToContainer:container viewStorage:viewStorage];
    [self startOrStopActivity:false];
}

- (void) unbindView:(DhtViewStorage *)viewStorage {
    [super  unbindView:viewStorage];
}

- (void) setFrame:(CGRect)frame {
    
    [super setFrame:frame];
}

- (void) setAlpha:(float)alpha {
    [super setAlpha:alpha];
    DhtActivityView *boundView = (DhtActivityView *)[self boundView];
    [boundView setAlphaForViews:alpha];
}

- (void) startOrStopActivity:(bool)stop {
    DhtActivityView *boundView = (DhtActivityView *)[self boundView];
    (stop)?[boundView stopActivity]:[boundView startAcitivty];
}
@end
