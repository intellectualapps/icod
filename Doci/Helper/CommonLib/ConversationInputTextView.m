//
//  ConversationInputTextView.m
//  ChatPro
//
//  Created by dungnv9 on 2/18/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "ConversationInputTextView.h"
#import "HPGrowingTextView.h"
#import "HPTextViewInternal.h"
#import "ConversationInputView.h"
#import "Device.h"
#import "Utils.h"


static void setViewFrame(UIView *view, CGRect frame)
{
    CGAffineTransform transform = view.transform;
    view.transform = CGAffineTransformIdentity;
    if (!CGRectEqualToRect(view.frame, frame))
        view.frame = frame;
    view.transform = transform;
}


@interface ConversationInputTextView()
<HPGrowingTextViewDelegate>
{
    CALayer *_stripeLayer;
    CGFloat _sendButtonWidth;
    CGFloat _leftButtonWidth;
    
}
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIView *inputFieldClippingContainer;
@property (nonatomic, strong) UIView *inputFieldPlaceholder;
@property (nonatomic, strong) UIImageView *fieldBackground;

@end
@implementation ConversationInputTextView


- (void) setDelegate:(id<ConversationInputPanelDelegate>)delegate
{
    [super setDelegate:delegate];
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame placeholder:@"" allowsChat:true];
}

- (instancetype)initWithFrame:(CGRect)frame placeholder:(NSString *)placeholder allowsChat:(BOOL)allows {
    self = [super initWithFrame:frame];
    
    if (self){
        _stripeLayer = [[CALayer alloc] init];
        _stripeLayer.backgroundColor = UIColorRGBA(0xb3aab2, 0.4f).CGColor;
        [self.layer addSublayer:_stripeLayer];
        _sendButtonWidth = (allows) ? 50 : -8.0;
        static UIImage *fieldBackgroundImage = nil;
        UIImage *placeholderImage = nil;
        static UIImage *sendButtonActiveImage = nil;
        static UIImage *sendButtonInactiveImage = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^
                      {
                          fieldBackgroundImage = [[UIImage imageNamed:@"ModernConversationInput"] stretchableImageWithLeftCapWidth:8 topCapHeight:8];
                          sendButtonActiveImage = [[UIImage imageNamed:@"send_btn_active"] stretchableImageWithLeftCapWidth:8 topCapHeight:8];
                          sendButtonInactiveImage =  [[UIImage imageNamed:@"send_btn_inActive"] stretchableImageWithLeftCapWidth:8 topCapHeight:8];
                      });
        
        if (placeholderImage == nil){
            NSString *placeholderText = placeholder;
            UIFont *placeholderFont = [UIFont systemFontOfSize:16];
            CGSize placeholderSize = [Utils sizeForText:placeholderText
                                              boundSize:self.frame.size
                                                 option:NSStringDrawingUsesLineFragmentOrigin
                                          lineBreakMode:NSLineBreakByClipping
                                                   font:placeholderFont];
            placeholderSize.width += 2.0f;
            placeholderSize.height += 2.0f;
            UIGraphicsBeginImageContextWithOptions(placeholderSize, false, 0.0f);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetFillColorWithColor(context, UIColorRGB(0xbebec0).CGColor);
            
            NSDictionary *d = [NSDictionary dictionaryWithObject:placeholderFont forKey:NSFontAttributeName];
            [placeholderText drawAtPoint:CGPointMake(1.0f, 4.0f) withAttributes:d];
            placeholderImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
        
        _fieldBackground = [[UIImageView alloc] initWithImage:fieldBackgroundImage];
        setViewFrame(_fieldBackground, CGRectMake(10, 9, self.frame.size.width - 41  - _sendButtonWidth - 1, 28));
        _fieldBackground.userInteractionEnabled = true;
        [_fieldBackground addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fieldBackgroundTapGesture:)]];
        [self addSubview:_fieldBackground];
        
        CGPoint placeholderOffset = [self inputFieldPlaceholderOffset];
        _inputFieldPlaceholder = [[UIImageView alloc]initWithImage:placeholderImage];
        _inputFieldPlaceholder.alpha = 0.5;
        setViewFrame(_inputFieldPlaceholder, CGRectOffset(_inputFieldPlaceholder.frame, placeholderOffset.x, placeholderOffset.y));
        [_fieldBackground addSubview:_inputFieldPlaceholder];
        
        _sendButton = [[UIButton alloc]initWithFrame:CGRectZero];
        _sendButton.exclusiveTouch = true;
        [_sendButton setBackgroundImage:sendButtonActiveImage forState: UIControlStateNormal];
        [_sendButton setBackgroundImage:sendButtonInactiveImage forState: UIControlStateDisabled];
        [_sendButton addTarget:self action:@selector(sendButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sendButton];
        
        
    }
    return self;
}
//定型
- (void)addButtonWithtitle:(NSString *)title titleColor:(UIColor *)color image:(UIImage *)img selectedImage:(UIImage *)selectedImg target:(id)target action:(SEL)action {
    _leftButton = [[UIButton alloc] initWithFrame: CGRectZero];
    [_leftButton setBackgroundImage:img forState:UIControlStateNormal];
    [_leftButton setBackgroundImage:selectedImg forState:UIControlStateSelected];
    [_leftButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [_leftButton setTitle:title forState:UIControlStateNormal];
    [_leftButton setTitleColor: (color) ? color : [UIColor whiteColor] forState:UIControlStateNormal];
    [_leftButton.titleLabel setFont: [UIFont boldSystemFontOfSize:16]];
    _leftButtonWidth = _leftButton.frame.size.width;
    [self addSubview:_leftButton];
    [self setNeedsDisplay];
}

- (void) setPlaceHolderString:(NSString *)placeholderText
{
    if (!placeholderText || placeholderText.length ==0){
        return;
    }
    UIImage *placeholderImage;
    UIFont *placeholderFont = [UIFont systemFontOfSize:16];
    CGSize placeholderSize = [Utils sizeForText:placeholderText
                                      boundSize:self.frame.size
                                         option:NSStringDrawingUsesLineFragmentOrigin
                                  lineBreakMode:NSLineBreakByClipping
                                           font:placeholderFont];
    placeholderSize.width += 2.0f;
    placeholderSize.height += 2.0f;
    UIGraphicsBeginImageContextWithOptions(placeholderSize, false, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, UIColorRGB(0xbebec0).CGColor);
    
    NSDictionary *d = [NSDictionary dictionaryWithObject:placeholderFont forKey:NSFontAttributeName];
    [placeholderText drawAtPoint:CGPointMake(1.0f, 1.0f) withAttributes:d];
    placeholderImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGSize size = placeholderImage.size;
    [(UIImageView *)_inputFieldPlaceholder setImage:placeholderImage];
    _inputFieldPlaceholder.frame = CGRectMake(_inputFieldPlaceholder.frame.origin.x, _inputFieldPlaceholder.frame.origin.y, size.width, size.height);
    
}
- (HPGrowingTextView *)maybeInputField
{
    return _inputField;
}

- (void)updateSendButtonVisibility
{
#if TG_ENABLE_AUDIO_NOTES
    
    bool hidden = _inputField == nil || _inputField.text.length == 0;
    
    if (!hidden)
    {
        NSString *text = _inputField.text;
        NSUInteger length = text.length;
        bool foundNonWhitespace = false;
        for (NSUInteger i = 0; i < length; i++)
        {
            unichar c = [text characterAtIndex:i];
            if (c != ' ')
            {
                foundNonWhitespace = true;
                break;
            }
        }
        
        if (!foundNonWhitespace)
            hidden = true;
    }
    
    _sendButton.hidden = hidden;
#endif
}
- (void) acttachButtonPressed
{
    id<ConversationInputTextPanelDelegate> delegate = (id<ConversationInputTextPanelDelegate>)self.delegate;
    if ([delegate respondsToSelector:@selector(inputPanelRequestAttachImage:)])
        [delegate inputPanelRequestAttachImage:self];
}
- (void)sendButtonPressed
{
    NSMutableString *text = [[NSMutableString alloc] initWithString:_inputField.text];
    int textLength = (int)text.length;
    for (int i = 0; i < textLength; i++)
    {
        unichar c = [text characterAtIndex:i];
        
        if (c == ' ' || c == '\t' || c == '\n')
        {
            [text deleteCharactersInRange:NSMakeRange(i, 1)];
            i--;
            textLength--;
        }
        else
            break;
    }
    
    for (int i = textLength - 1; i >= 0; i--)
    {
        unichar c = [text characterAtIndex:i];
        
        if (c == ' ' || c == '\t' || c == '\n')
        {
            [text deleteCharactersInRange:NSMakeRange(i, 1)];
            textLength--;
        }
        else
            break;
    }
    
    id<ConversationInputTextPanelDelegate> delegate = (id<ConversationInputTextPanelDelegate>)self.delegate;
    if ([delegate respondsToSelector:@selector(inputPanelRequestedSendMessage:text:)])
        [delegate inputPanelRequestedSendMessage:self text:text];
}
- (CGPoint)inputFieldPlaceholderOffset
{
    static CGPoint offset;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                          offset = CGPointMake(4.0f, 3.0f);
                      else
                          offset = CGPointMake(8.0f, 5.0f);
                  });
    
    return offset;
}
- (void)fieldBackgroundTapGesture:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        [[self inputField].internalTextView becomeFirstResponder];
    }
}

- (CGSize)leftButtonTextSize {
    if (_leftButton && _leftButton.titleLabel.text.length > 0 ) {
        CGSize size =  [Utils sizeForText: _leftButton.titleLabel.text
                                boundSize: CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                   option: NSStringDrawingUsesLineFragmentOrigin
                            lineBreakMode: NSLineBreakByClipping
                                     font: [UIFont systemFontOfSize:16]];
        size.width += 20.f;
        return size;
    }
    return CGSizeZero;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.backgroundColor =  [UIColor colorWithRed:217.0 / 255.0 green:217.0 / 255.0 blue:217.0 / 255.0 alpha:1];
    
    CGRect frame = self.frame;
    
    UIEdgeInsets inputFieldInsets = [self inputFieldInsets];
    CGPoint sendButtonOffset = [self sendButtonOffset];
    //left button layout
    CGFloat originX = 0.0f;
    if (_leftButton) {
        originX = 10.0f;
        CGSize btnSize = [self leftButtonTextSize];
        _leftButton.frame = CGRectMake(originX, frame.size.height - [self baseHeight] + btnSize.height / 2 + 5, btnSize.width, btnSize.height);
        originX += btnSize.width;
    }
    inputFieldInsets.left += originX;
    setViewFrame(_fieldBackground, CGRectMake( inputFieldInsets.left,
                                              inputFieldInsets.top,
                                              frame.size.width - inputFieldInsets.left - inputFieldInsets.right  - _sendButtonWidth - sendButtonOffset.x,
                                              frame.size.height - inputFieldInsets.top - inputFieldInsets.bottom));
    
    
    CGRect inputFieldClippingFrame = _fieldBackground.frame;
    setViewFrame(_inputFieldClippingContainer, inputFieldClippingFrame);
    
    setViewFrame(_sendButton, CGRectMake(frame.size.width - _sendButtonWidth - sendButtonOffset.x,
                                         frame.size.height - [self baseHeight] + sendButtonOffset.y,
                                         _sendButtonWidth,
                                         [self baseHeight] - sendButtonOffset.y * 2));
    [self updateFrameForInputField];
}
- (CGFloat)baseHeight
{
    static CGFloat value = 0.0f;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      value = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? default_input_height : 56.0f;
                  });
    
    return value;
}
- (CGPoint)sendButtonOffset
{
    static CGPoint offset;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                          offset = CGPointMake(8.0f, 6.0f);
                      else
                          offset = CGPointMake(11.0f, 6.0f);
                  });
    
    return offset;
}
- (UIEdgeInsets)inputFieldInternalEdgeInsets
{
    static UIEdgeInsets insets;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                          insets = UIEdgeInsetsMake( 0, 0.0f, 0.0f, 0.0f);
                      else
                          insets = UIEdgeInsetsMake(-1 - RetinaPixel, 4.0f, 0.0f, 0.0f);
                  });
    
    return insets;
}


#pragma --mark
- (void)growingTextView:(HPGrowingTextView *)__unused growingTextView willChangeHeight:(float)height duration:(NSTimeInterval)duration animationCurve:(int)animationCurve
{
    UIEdgeInsets inputFieldInsets = [self inputFieldInsets];
    CGFloat inputContainerHeight = MAX([self baseHeight], height - 8 + inputFieldInsets.top + inputFieldInsets.bottom);
    id <ConversationInputTextPanelDelegate> __delegagte = (id<ConversationInputTextPanelDelegate>)self.delegate;
    if (__delegagte && [__delegagte respondsToSelector:@selector(inputPanelWillChangeHeight:height:duration:animationCurve:)]){
        [__delegagte  inputPanelWillChangeHeight:self
                                          height:inputContainerHeight
                                        duration:duration
                                  animationCurve:animationCurve];
    }
}

- (int)_maxNumberOfLinesForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return 5;
}

- (void)updateFrameForInputField {
    UIEdgeInsets inputFieldInternalEdgeInsets = [self inputFieldInternalEdgeInsets];
    CGRect rect = CGRectMake(inputFieldInternalEdgeInsets.left, inputFieldInternalEdgeInsets.top - 2, _inputFieldClippingContainer.frame.size.width - inputFieldInternalEdgeInsets.left, _inputFieldClippingContainer.frame.size.height);
    _inputField.frame = rect;
}

- (HPGrowingTextView *)inputField
{
    if (_inputField == nil)
    {
        CGRect inputFieldClippingFrame = _fieldBackground.frame;
        inputFieldClippingFrame.origin.y +=1;
        _inputFieldClippingContainer = [[UIView alloc] initWithFrame:inputFieldClippingFrame];
        _inputFieldClippingContainer.clipsToBounds = true;
        [self addSubview:_inputFieldClippingContainer];
        UIEdgeInsets inputFieldInternalEdgeInsets = [self inputFieldInternalEdgeInsets];
        
        _inputField = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(inputFieldInternalEdgeInsets.left, inputFieldInternalEdgeInsets.top - 2, _inputFieldClippingContainer.frame.size.width - inputFieldInternalEdgeInsets.left, _inputFieldClippingContainer.frame.size.height)];
        _inputField.placeholderView = _inputFieldPlaceholder;
        _inputField.font = [UIFont systemFontOfSize:16];
        _inputField.maxNumberOfLines = [self _maxNumberOfLinesForInterfaceOrientation:UIInterfaceOrientationPortrait];
        
        _inputField.clipsToBounds = true;
        
        _inputField.backgroundColor = nil;
        _inputField.tintColor = [UIColor blackColor];
        _inputField.opaque = false;
        _inputField.internalTextView.backgroundColor = nil;
        _inputField.internalTextView.opaque = false;
        _inputField.internalTextView.contentMode = UIViewContentModeLeft;
        
        _inputField.delegate = self;
        
        _inputField.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(-inputFieldInternalEdgeInsets.top, 0, 5 - RetinaPixel, 0);
        
        [_inputFieldClippingContainer addSubview:_inputField];
    }
    
    return _inputField;
}
- (void)growingTextViewDidChange:(HPGrowingTextView *)__unused growingTextView afterSetText:(bool)afterSetText
{
    int textLength = (int)growingTextView.text.length;
    _inputFieldPlaceholder.hidden = (textLength > 0);
    NSString *text = growingTextView.text;
    bool hasNonWhitespace = false;
    for (int i = 0; i < textLength; i++)
    {
        unichar c = [text characterAtIndex:i];
        if (c != ' ' && c != '\n' && c != '\t')
        {
            hasNonWhitespace = true;
            break;
        }
    }
    
    
    id<ConversationInputTextPanelDelegate> delegate = (id<ConversationInputTextPanelDelegate>)self.delegate;
    if ([delegate respondsToSelector:@selector(inputPanelTextChanged:text:)])
        [delegate inputPanelTextChanged:self text:text];
    
    [self updateSendButtonVisibility];
    
    if (!afterSetText)
    {
        if (hasNonWhitespace)
        {
            id<ConversationInputTextPanelDelegate> delegate = (id<ConversationInputTextPanelDelegate>)self.delegate;
            if ([delegate respondsToSelector:@selector(inputTextPanelHasIndicatedTypingActivity:)])
                [delegate inputTextPanelHasIndicatedTypingActivity:self];
        }
    }
    
    if (!hasNonWhitespace)
    {
        id<ConversationInputTextPanelDelegate> delegate = (id<ConversationInputTextPanelDelegate>)self.delegate;
        if ([delegate respondsToSelector:@selector(inputTextPanelHasCancelledTypingActivity:)])
            [delegate inputTextPanelHasCancelledTypingActivity:self];
    } else {
        
    }
}

- (BOOL)isIphoneX {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ) {
        CGRect frame = [[UIScreen mainScreen] bounds];
        CGFloat max = MAX(frame.size.width, frame.size.height);
        if (max == 812.0) {
            return true;
        }
    }
    return false;
}

- (void)changeOrientationToOrientation:(UIInterfaceOrientation)__unused orientation keyboardHeight:(float)__unused keyboardHeight duration:(NSTimeInterval)__unused duration
{
    _interfaceOrientation = orientation;
    id<ConversationInputPanelDelegate> delegate = self.delegate;
    CGSize messageAreaSize = [delegate messageAreaSizeForInterfaceOrientation:orientation];

    messageAreaSize.height += 44;
    UIView *inputFieldSnapshotView = nil;
    if (duration > DBL_EPSILON)
    {
        inputFieldSnapshotView = [_inputField.internalTextView snapshotViewAfterScreenUpdates:false];
        inputFieldSnapshotView.frame = CGRectOffset(_inputField.frame, _inputFieldClippingContainer.frame.origin.x, _inputFieldClippingContainer.frame.origin.y);
        [self addSubview:inputFieldSnapshotView];
    }
    
    [UIView performWithoutAnimation:^
     {
         NSRange range = _inputField.internalTextView.selectedRange;
         
         _inputField.delegate = nil;
         
         UIEdgeInsets inputFieldInsets = [self inputFieldInsets];
         UIEdgeInsets inputFieldInternalEdgeInsets = [self inputFieldInternalEdgeInsets];
         
         CGRect inputFieldClippingFrame = CGRectMake(inputFieldInsets.left, inputFieldInsets.top, messageAreaSize.width - inputFieldInsets.left - inputFieldInsets.right - _sendButtonWidth - 1, 0.0f);
         
         CGRect inputFieldFrame = CGRectMake(inputFieldInternalEdgeInsets.left, inputFieldInternalEdgeInsets.top, inputFieldClippingFrame.size.width - inputFieldInternalEdgeInsets.left, 0.0f);
         
         setViewFrame(_inputField, inputFieldFrame);
         [_inputField setMaxNumberOfLines:[self _maxNumberOfLinesForInterfaceOrientation:orientation]];
         [_inputField refreshHeight];
         
         _inputField.internalTextView.selectedRange = range;
         
         _inputField.delegate = self;
     }];
    
    CGFloat inputContainerHeight = [self heightForInputFieldHeight:_inputField.frame.size.height];
    CGRect newInputContainerFrame =CGRectMake(0,messageAreaSize.height - keyboardHeight - inputContainerHeight , messageAreaSize.width, inputContainerHeight);
    
    if (duration > DBL_EPSILON)
    {
        if (inputFieldSnapshotView != nil)
            _inputField.alpha = 0.0f;
        
        [UIView animateWithDuration:duration animations:^
         {
             self.frame = newInputContainerFrame;
             [self layoutSubviews];
             
             if (inputFieldSnapshotView != nil)
             {
                 _inputField.alpha = 1.0f;
                 inputFieldSnapshotView.frame = CGRectOffset(_inputField.frame, _inputFieldClippingContainer.frame.origin.x, _inputFieldClippingContainer.frame.origin.y);
                 inputFieldSnapshotView.alpha = 0.0f;
             }
         } completion:^(__unused BOOL finished)
         {
             [inputFieldSnapshotView removeFromSuperview];
         }];
    }
    else
    {
        self.frame = newInputContainerFrame;
    }
}
- (void)adjustForOrientation:(UIInterfaceOrientation)orientation keyboardHeight:(float)keyboardHeight duration:(NSTimeInterval)duration animationCurve:(int)animationCurve
{
    if (UIInterfaceOrientationIsPortrait(_interfaceOrientation) != UIInterfaceOrientationIsPortrait(orientation))
        [self changeOrientationToOrientation:orientation keyboardHeight:keyboardHeight duration:0.0];
    
    _interfaceOrientation = orientation;
    
    [self _adjustForOrientation:orientation keyboardHeight:keyboardHeight inputFieldHeight:_inputField == nil ? 36.0f : _inputField.frame.size.height duration:duration animationCurve:animationCurve];
}
- (void)_adjustForOrientation:(UIInterfaceOrientation)orientation keyboardHeight:(float)keyboardHeight inputFieldHeight:(float)inputFieldHeight duration:(NSTimeInterval)duration animationCurve:(int)animationCurve
{
    dispatch_block_t block = ^
    {
        id<ConversationInputPanelDelegate> delegate = self.delegate;
        CGSize messageAreaSize = [delegate messageAreaSizeForInterfaceOrientation:orientation];
        
        messageAreaSize.height +=  44;//(keyboardHeight == 0) ? 64.0 : 44.0;
        CGFloat inputContainerHeight = [self heightForInputFieldHeight:inputFieldHeight];
        CGFloat originX = 0;
        CGFloat width = messageAreaSize.width;
        if ([Device isWidescreen] && [self isIphoneX]) {
            if (UIInterfaceOrientationIsLandscape(orientation)) {
                originX = 30;
                width -= 60;
            }
        }
        self.frame = CGRectMake(originX, messageAreaSize.height - keyboardHeight - inputContainerHeight , width, inputContainerHeight);
        [self layoutSubviews];
    };
    
    if (duration > DBL_EPSILON)
        [UIView animateWithDuration:duration delay:0 options:animationCurve << 16 animations:block completion:nil];
    else
        block();
}
- (UIEdgeInsets)inputFieldInsets
{
    static UIEdgeInsets insets;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                          insets = UIEdgeInsetsMake(6.0f, 6.0f, 6.0f, 6.0f);
                      else
                          insets = UIEdgeInsetsMake(IsRetina() ? 12.0f : 12.0f, 58.0f, 12.0f, 21.0f);
                  });
    
    return insets;
}


- (CGFloat)heightForInputFieldHeight:(CGFloat)inputFieldHeight
{
    if (IsPad())
        inputFieldHeight += 4;
    
    UIEdgeInsets inputFieldInsets = [self inputFieldInsets];
    CGFloat height = MAX([self baseHeight], inputFieldHeight - 8 + inputFieldInsets.top + inputFieldInsets.bottom);
    
    return height;
}
@end
