#import "DHTIndexSet.h"

#import <vector>

@interface DHTIndexSet : NSObject

- (int)insertIndex:(int32_t)index;

@end

@interface DHTIndexSet () {
    std::vector<int32_t> _indices;
}

@end

@implementation DHTIndexSet

- (int)insertIndex:(int32_t)index {
    if (index < 0) {
        return -1;
    }
    
    for (auto it = _indices.begin(); it != _indices.end(); it++) {
        if (*it >= index) {
            (*it)++;
        }
    }
    
    auto insertLocation = _indices.end();
    for (auto it = _indices.begin(); it != _indices.end(); it++) {
        if (*it >= index) {
            insertLocation = it;
            break;
        }
    }
    
    int indexLocation = (int)(insertLocation - _indices.begin());
    _indices.insert(insertLocation, index);
    
    return indexLocation;
}

- (NSIndexSet *)indexSet {
    NSMutableIndexSet *result = [[NSMutableIndexSet alloc] init];
    for (auto it = _indices.begin(); it != _indices.end(); it++) {
        [result addIndex:*it];
    }
    return result;
}

@end

@interface MutableArrayWithIndices () {
    DHTIndexSet *_insertIndexSet;
    NSMutableArray *_insertArray;
    NSMutableArray *_array;
}

@end

@implementation MutableArrayWithIndices

- (instancetype)initWithArray:(NSMutableArray *)array {
    self = [super init];
    if (self != nil) {
        _array = array;
        _insertIndexSet = [[DHTIndexSet alloc] init];
        _insertArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)insertObject:(id)object atIndex:(NSUInteger)index {
    [_array insertObject:object atIndex:index];
    int insertIndex = (int)([_insertIndexSet insertIndex:(int)index]);
    if (insertIndex >= 0) {
        [_insertArray insertObject:object atIndex:insertIndex];
    }
}

- (NSArray *)objectsForInsertOperations:(NSIndexSet **)indexSet {
    NSIndexSet *resultIndexSet = [_insertIndexSet indexSet];
    if (indexSet != NULL) {
        *indexSet = resultIndexSet;
    }
    return _insertArray;
}

- (NSIndexSet *)indexSet {
    NSIndexSet *resultIndexSet = [_insertIndexSet indexSet];
    return resultIndexSet;
}
@end
