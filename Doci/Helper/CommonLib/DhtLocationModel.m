//
//  DhtLocationModel.m
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 9/13/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtLocationModel.h"

@implementation DhtLocationModel

+ (id)shareModel {
    static DhtLocationModel *sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[DhtLocationModel alloc] init];
    });
    return sharedMyModel;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

@end
