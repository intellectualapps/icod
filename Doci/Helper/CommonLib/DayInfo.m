//
//  DayInfo.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DayInfo.h"
#import "NSDate+Extension.h"
@implementation DayInfo
- (instancetype)initWithDate:(NSDate *)adate {
    self = [super init];
    if (self) {
        _date = adate;
    }
    return self;
}

- (void)setNumberOfItem:(NSInteger)nb {
    _numberOfItem = nb;
}

- (void)reset {
    _isSelected = false;
}

- (BOOL)isPast {
    if (self.date) {
        NSTimeInterval interval1 = [self.date dateAtStartOfDay].timeIntervalSince1970;
        NSTimeInterval interval2 = [[[NSDate date] dateAtStartOfDay] timeIntervalSince1970];
        if (interval2 - interval1 >= 86400) {
            return true;
        }
    }
    return false;
}

- (BOOL)isToday {
    if (self.date) {
        NSTimeInterval interval1 = [self.date dateAtStartOfDay].timeIntervalSince1970;
        NSTimeInterval interval2 = [[[NSDate date] dateAtStartOfDay] timeIntervalSince1970];
        if (interval2 == interval1) {
            return true;
        }
    }
    return false;
}

- (BOOL)hasScheduleInfo {
    return _numberOfItem > 0 ;
}
@end
