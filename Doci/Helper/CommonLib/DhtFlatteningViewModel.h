//
//  DhtFlatteningViewModel.h
//  ChatPro
//
//  Created by dungnv9 on 2/26/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtViewModel.h"
@class DhtViewContext;
@interface DhtFlatteningViewModel : DhtViewModel
@property (nonatomic, assign) BOOL enableActionHandle;
- (void)setNeedsSubmodelContentsUpdate;
- (void)updateSubmodelContentsIfNeeded;
- (void)updateSubmodelContentsForVisibleRect:(CGRect)rect;

- (instancetype) initWithContext:(DhtViewContext *)viewContext;
@end
