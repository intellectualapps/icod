//
//  GenericItem.h
//  kids_taxi_driver
//
//  Created by Nguyen Van Dung on 5/23/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DhtViewModel.h"
#import "DhtViewContext.h"
#import "DhtViewStorage.h"
#import "GenericInfo.h"
@class GenericItem;
@class CollectionCell;

@interface GenericItem : NSObject
@property (nonatomic, strong, nullable) GenericInfo *contentInfo;
@property (nonatomic) BOOL isShowFull;
@property (nonatomic) int collapseFlags;

- (nullable instancetype) initWithContent:(nullable GenericInfo *)info viewContext:(nullable DhtViewContext *)context;
- (bool)collapseWithItem:(nullable GenericItem *)item forContainerSize:(CGSize)__unused containerSize;
- (CGSize)sizeForContainerSize:(CGSize)containerSize;
- (void)bindCell:(nullable CollectionCell *)cell viewStorage:(nullable DhtViewStorage *)viewStorage;
- (void)unbindCell:(nullable DhtViewStorage *)viewStorage;
- (void)moveToCell:(nullable CollectionCell *)cell;
- (void)updateToItem:(nullable GenericItem *)updatedItem viewStorage:(nullable DhtViewStorage *)viewStorage;
- (void)temporaryMoveToView:(nullable UIView *)view;
- (void)relativeBoundsUpdated:(nullable id)cell bounds:(CGRect)bounds;
- (nullable CollectionCell *)boundCell;
- (nullable DhtViewModel *)viewModel;
- (nullable CollectionCell *)dequeueCollectionCell:(nonnull UICollectionView *)collectionView registeredIdentifiers:(nonnull NSMutableSet *)registeredIdentifiers forIndexPath:(nonnull NSIndexPath *)indexPath;
- (nonnull id)deepCopy;
- (CGRect)effectiveContentFrame;
- (nullable DhtViewModel *)viewModelForContainerSize:(CGSize)containerSize;
- (nullable DhtViewModel *)createViewModel:(nullable GenericItem *)info containerSize:(CGSize)containerSize;
- (void)setTemporaryHighlighted:(bool)temporaryHighlighted viewStorage:(nullable DhtViewStorage *)__unused viewStorage;
- (nonnull DhtViewContext *)viewContext;
- (nullable Class)cellClass;
@end
