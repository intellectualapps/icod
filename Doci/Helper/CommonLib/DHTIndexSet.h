//
//  ChatViewController.m
//  MtoM
//
//  Created by nguyen van dung on 12/15/15.
//  Copyright © 2015 Framgia. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface MutableArrayWithIndices : NSObject

- (instancetype)initWithArray:(NSMutableArray *)array;

- (void)insertObject:(id)object atIndex:(NSUInteger)index;

- (NSArray *)objectsForInsertOperations:(NSIndexSet **)indexSet;
- (NSIndexSet *)indexSet;

@end
