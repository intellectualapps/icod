//
//  DhtFlatteningView.m
//  ChatPro
//
//  Created by dungnv9 on 2/26/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtFlatteningView.h"
@interface FlatteningViewLayer : CALayer

@end

@implementation FlatteningViewLayer

- (id<CAAction>)actionForKey:(NSString *)event
{
    if ([event isEqualToString:@"contents"])
    {
        return nil;
    }
    return [super actionForKey:event];
}
- (void)setShouldRasterize:(BOOL)shouldRasterize
{
    if (shouldRasterize)
        [super setShouldRasterize:false];
}

@end

@interface DhtFlatteningView()
{
    
}
@property (nonatomic, strong) NSString *viewIdentifier;
@property (nonatomic, strong) NSString *viewStateIdentifier;
@end
@implementation DhtFlatteningView
@synthesize viewIdentifier = _viewIdentifier;
@synthesize viewStateIdentifier = _viewStateIdentifier;

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = nil;
        self.opaque = false;
    }
    return self;
}

- (void) setViewIdentifier:(NSString *)viewIdentifier {
    _viewIdentifier = viewIdentifier;
}

- (void) setFrame:(CGRect)frame {
    [super setFrame:frame];
}

+ (Class)layerClass {
    return [FlatteningViewLayer class];
}

- (void)willBecomeRecycled {
   self.layer.contents = nil;
}
@end
