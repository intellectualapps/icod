//
//  DhtTextView.h
//  ChatPro
//
//  Created by dungnv9 on 2/25/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"

@interface DhtTextView : BaseView<BaseViewProtocol>
@property (nonatomic, strong) UILabel       *label;

@end
