//
//  UIScrollView+Hacks.m
//  ChatPro
//
//  Created by dungnv9 on 3/9/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "UIScrollView+Hacks.h"

@implementation UIScrollView(DhtHacks)
- (void)stopScrollingAnimation
{
    UIView *superview = self.superview;
    NSUInteger index = [self.superview.subviews indexOfObject:self];
    [self removeFromSuperview];
    [superview insertSubview:self atIndex:index];
}
@end
