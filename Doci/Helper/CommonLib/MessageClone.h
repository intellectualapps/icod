//
//  MessageClone.h
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"
#import "GenericInfo.h"

@class Message;

@interface MessageClone : GenericInfo
/*
 The convesation id to indicate who you are chatting.
 */
@property (nonatomic, copy) NSString *conversationId;

/*
 - This value will fill from server
 */
@property (nonatomic, assign) NSInteger orderId;
@property (nonatomic) double modifiedDate;
@property (nonatomic, copy) NSString *avatarUrl;

- (instancetype) initWithCoreDataEntity:(Message *)_mo;
- (void)copyContentFromMessageFromSerer:(MessageClone *)clone;
+ (NSString *)localIdentifierForMid:(NSString *)mid;
- (void) saveToCoreData;
- (NSString *)statuString;
- (NSString *)timeString;
@end
