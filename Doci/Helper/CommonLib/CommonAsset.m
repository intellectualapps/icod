//
//  CommonAsset.m
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 9/1/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "CommonAsset.h"
#import "UIImage+Resize.h"
#import "Utils.h"

@implementation CommonAsset

+ (CommonAsset *) instance {
    static CommonAsset *singleton = nil;
    static dispatch_once_t oneToken ;
    dispatch_once(&oneToken, ^{
        singleton = [[CommonAsset alloc]init];
    });
    return singleton;
}

- (UIImage *)systemMessageBackground {
    static int cachedImageColor = -1;
    static UIImage *image = nil;
    if (cachedImageColor != 0x000000 || image == nil) {
        UIImage *rawImage = [CommonAsset generateSystemMessageBackground:0x000000];
        image = [rawImage stretchableImageWithLeftCapWidth:(int)(rawImage.size.width / 2) topCapHeight:(int)(rawImage.size.height / 2)];
        
        cachedImageColor = 0x000000;
    }
    return image;
}

- (UIImage *) sentFailButtonImage {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [UIImage imageNamed:@"UnsentButton_small"];
    });
    return image;
}

- (UIImage *)defaultDriverAvatar {
    static UIImage *image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [UIImage imageNamed:@"icon_user_default"];
    });
    return image;
}

- (UIImage *)messageLinkFull {
    static UIImage *image = nil;
    if (image == nil) {
        UIImage *rawImage = [UIImage imageNamed:@"LinkFull"];
        image = [rawImage stretchableImageWithLeftCapWidth:(int)(rawImage.size.width / 2) topCapHeight:(int)(rawImage.size.height / 2)];
    }
    return image;
}

- (UIImage *)messageLinkCornerLR {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerLR"];
    }
    return image;
}

- (UIImage *)messageLinkCornerBT {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerBT"];
    }
    return image;
}

- (UIImage *)messageLinkCornerRL {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerRL"];
    }
    return image;
}

- (UIImage *)messageLinkCornerTB {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerTB"];
    }
    return image;
}

+ (UIImage *)generateSystemMessageBackground:(int)baseColor {
    float backgroundAlpha = 0.5;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(21, 21), false, 0.0f);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect bounds = CGRectMake(0.5f, 0, 20, 20);
    
    CGFloat radius = 0.5f * CGRectGetHeight(bounds);
    
    CGMutablePathRef visiblePath = CGPathCreateMutable();
    CGRect innerRect = CGRectInset(bounds, radius, radius);
    CGPathMoveToPoint(visiblePath, NULL, innerRect.origin.x, bounds.origin.y);
    CGPathAddLineToPoint(visiblePath, NULL, innerRect.origin.x + innerRect.size.width, bounds.origin.y);
    CGPathAddArcToPoint(visiblePath, NULL, bounds.origin.x + bounds.size.width, bounds.origin.y, bounds.origin.x + bounds.size.width, innerRect.origin.y, radius);
    CGPathAddLineToPoint(visiblePath, NULL, bounds.origin.x + bounds.size.width, innerRect.origin.y + innerRect.size.height);
    CGPathAddArcToPoint(visiblePath, NULL,  bounds.origin.x + bounds.size.width, bounds.origin.y + bounds.size.height, innerRect.origin.x + innerRect.size.width, bounds.origin.y + bounds.size.height, radius);
    CGPathAddLineToPoint(visiblePath, NULL, innerRect.origin.x, bounds.origin.y + bounds.size.height);
    CGPathAddArcToPoint(visiblePath, NULL,  bounds.origin.x, bounds.origin.y + bounds.size.height, bounds.origin.x, innerRect.origin.y + innerRect.size.height, radius);
    CGPathAddLineToPoint(visiblePath, NULL, bounds.origin.x, innerRect.origin.y);
    CGPathAddArcToPoint(visiblePath, NULL,  bounds.origin.x, bounds.origin.y, innerRect.origin.x, bounds.origin.y, radius);
    CGPathCloseSubpath(visiblePath);
    
    CGContextSaveGState(context);
    
    UIColor *color = nil;
    
    //color = UIColorRGBA(0xffffff, 0.4f);
    //CGContextSetShadowWithColor(context, CGSizeMake(0.0f, 1.0f), 0.0f, [color CGColor]);
    
    color = UIColorRGBA(baseColor, backgroundAlpha);
    [color setFill];
    CGContextAddPath(context, visiblePath);
    CGContextFillPath(context);
    
    CGContextRestoreGState(context);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectInset(bounds, -2, -2));
    
    CGPathAddPath(path, NULL, visiblePath);
    CGPathCloseSubpath(path);
    
    CGContextAddPath(context, visiblePath);
    CGContextClip(context);
    
    CGContextSaveGState(context);
    
    //color = UIColorRGBA(baseColor, shadowAlpha);
    //CGContextSetShadowWithColor(context, CGSizeMake(0.0f, 1.0f), 1.0f, [color CGColor]);
    
    [color setFill];
    CGContextAddPath(context, path);
    CGContextEOFillPath(context);
    
    CGContextRestoreGState(context);
    
    CGPathRelease(path);
    CGPathRelease(visiblePath);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *) defaultAvatar {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"icon_user_default"];
    }
    return image;
}

- (UIImage *)iconTaxi {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"icon_taxi"];
    }
    return image;
}

- (UIImage *)unavailableIcon {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"unavailable_icon"];
    }
    return image;
}

- (UIImage *)pendingIcon {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"pending_icon"];
    }
    return image;
}
@end
