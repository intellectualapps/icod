//
//  ConversationInputView.m
//  ChatPro
//
//  Created by dungnv9 on 2/18/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "ConversationInputView.h"

@implementation ConversationInputView

- (void)adjustForOrientation:(UIInterfaceOrientation)__unused orientation keyboardHeight:(float)__unused keyboardHeight duration:(NSTimeInterval)__unused duration animationCurve:(int)__unused animationCurve
{
}
- (void) setDelegate:(id<ConversationInputPanelDelegate>)delegate
{
    _delegate = delegate;
}
- (void)changeOrientationToOrientation:(UIInterfaceOrientation)__unused orientation keyboardHeight:(float)__unused keyboardHeight duration:(NSTimeInterval)__unused duration
{
}
@end
