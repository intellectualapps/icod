//
//  MessageItem.m
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "MessageItem.h"
#import "DhtViewModel.h"
#import "CollectionCell.h"
#import "MessageTextViewModel.h"

@interface MessageItem() {
    
}
@end
@implementation MessageItem
- (DhtViewModel *)createViewModel:(GenericItem *)info containerSize:(CGSize)containerSize {
    return [[MessageTextViewModel alloc] initWithMessage:(MessageClone *)info.contentInfo context:[self viewContext]];
}
@end
