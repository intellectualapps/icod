//
//  CommonAsset.h
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 9/1/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CommonDataSourceAssetProtocol<NSObject>
@required
- (UIImage *)systemMessageBackground;
- (UIImage *) sentFailButtonImage;
- (UIImage *)defaultDriverAvatar;
- (UIImage *)messageLinkFull;
- (UIImage *)messageLinkCornerLR;
- (UIImage *)messageLinkCornerBT;
- (UIImage *)messageLinkCornerRL;
- (UIImage *)messageLinkCornerTB;

- (UIImage *)defaultAvatar;
- (UIImage *)iconTaxi;
- (UIImage *)unavailableIcon;
- (UIImage *)pendingIcon;
@end

@interface CommonAsset : NSObject<CommonDataSourceAssetProtocol>
+ (CommonAsset *) instance;
@end
