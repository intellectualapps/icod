//
//  CollectionCell.m
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "CollectionCell.h"
#import "MessageItem.h"


@interface CollectionCellLayer: CALayer

@end

@implementation CollectionCellLayer

- (void)setShouldRasterize:(BOOL)shouldRasterize
{
    if (shouldRasterize)
        [super setShouldRasterize:false];
}

@end



@interface CollectionCell()
{
    bool _editing;
    UIView *_contentViewForBinding;
}
@end

@implementation CollectionCell

- (void) prepareForReuse {
    [super prepareForReuse];
    if (self.contentViewForBinding) {
        [self.contentViewForBinding.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
}

+ (Class)layerClass {
    return [CollectionCellLayer class];
}

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self contentViewForBinding].transform = CGAffineTransformMakeRotation((float)M_PI);
        self.clipsToBounds = true;
        _contentViewForBinding = [[UIView alloc] initWithFrame:(CGRect){CGPointZero, frame.size}];
        _contentViewForBinding.transform = CGAffineTransformMakeRotation((float)M_PI);
        [self addSubview:_contentViewForBinding];
        
        _needsRelativeBoundsUpdateNotifications =true;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        _contentViewForBinding.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    }
    return self;
}

- (void)relativeBoundsUpdated:(CGRect)bounds {
    id item = _boundItem;
    if (item != nil ) {
        [(MessageItem *)item sizeForContainerSize:bounds.size];
        CGRect convertedBounds = [[self contentViewForBinding] convertRect:bounds fromView:self];
        [item relativeBoundsUpdated:self bounds:convertedBounds];
    }
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    
    _contentViewForBinding.frame = (CGRect){CGPointZero, bounds.size};
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    _contentViewForBinding.frame = (CGRect){CGPointZero, frame.size};
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
}

- (void)setEditing:(bool)editing animated:(bool)__unused animated viewStorage:(DhtViewStorage *)__unused viewStorage {
    if (_editing != editing) {
        _editing = editing;
        [self contentViewForBinding].userInteractionEnabled = !_editing;
    }
}

- (UIView *)contentViewForBinding {
    return _contentViewForBinding;
}

@end
