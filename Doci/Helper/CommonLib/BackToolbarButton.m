//
//  BackToolbarButton.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "BackToolbarButton.h"
#import "Utils.h"
@interface BackToolbarButton () {
    float _labelOffset;;
    bool _animateHighlight;
    UIColor *_titleColor;
    UIImageView *_highlightImageView;
    UIView *_highlightBackgroundView;
}

@property (nonatomic, strong) UIImageView *arrowView;

@end

@implementation BackToolbarButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        if (iosMajorVersion() >= 7) {
            _arrowOffset = 0.0f;
            _labelOffset = 1.0f;
        } else {
            _arrowOffset = -1.0f;
            _labelOffset = 0.0f;
        }
        _arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Back"]];
        [self addSubview:_arrowView];
    }
    return self;
}

- (void)setTitleColor:(UIColor *)color {
    _titleColor = color;

}

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state {
    [super setTitleColor:color forState:state];
    if (state == UIControlStateNormal) {
        _buttonTitleLabel.textColor = color;
    }
}

- (void)setButtonTitle:(NSString *)buttonTitle {
    _buttonTitleLabel.text = buttonTitle;
    [_buttonTitleLabel sizeToFit];
    [self layoutSubviews];
}

- (instancetype)initWithLightModeAndimage:(UIImage *)image {
    self = [self initWithFrame:CGRectZero];
    if (self != nil) {
        [self setTitleColor: UIColorRGB(0xEA6E94)];
        if (image) {
            _arrowView.image = nil;
            [self setImage:image forState:UIControlStateNormal];
        } else {
            _arrowView.image = [UIImage imageNamed:@"btn_nav_cancel"];
        }
        _arrowOffset = -1.0f;
        _labelOffset = 0.0f;
    }
    return self;
}


- (UIEdgeInsets)alignmentRectInsets {
    UIEdgeInsets insets = UIEdgeInsetsZero;
    insets = UIEdgeInsetsMake(0, 8.0f, 0, 0);
    return insets;
}

- (void)sizeToFit {
    [self.buttonTitleLabel sizeToFit];
    CGRect frame = self.frame;
    frame.size.height = _arrowView.frame.size.height;
    frame.size.width = _arrowView.frame.size.width + 7 + self.buttonTitleLabel.frame.size.width;
    self.frame = frame;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect arrowFrame = _arrowView.frame;
    arrowFrame.origin = CGPointMake(_arrowOffset, _arrowOffset + floorf((self.frame.size.height - arrowFrame.size.height) / 2) + 1);
    _arrowView.frame = arrowFrame;
    
    CGRect labelFrame = self.buttonTitleLabel.frame;
    labelFrame.origin = CGPointMake(arrowFrame.origin.x + arrowFrame.size.width + 7, (self.frame.size.height - labelFrame.size.height)/2);
    self.buttonTitleLabel.frame = labelFrame;
}

- (void)setHighlighted:(BOOL)highlighted {
    _arrowView.alpha = highlighted ? 0.4f : 1.0f;
    
    [super setHighlighted:highlighted];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if (self.alpha > FLT_EPSILON && !self.hidden && CGRectContainsPoint(CGRectInset(self.bounds, -5, -5), point))
        return self;
    
    return [super hitTest:point withEvent:event];
}

@end
