//
//  DHTDateHeaderView.m
//  ChatPro
//
//  Created by dungnv9 on 3/10/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtDateHeaderView.h"
#import "CommonAsset.h"
#import "ChatUtilities.h"

@interface DhtDateHeaderView()
{
    UILabel *_dateLabel;
    UIImageView *_backgroundImageView;
    
    int _date;
}

@end
@implementation DhtDateHeaderView

- (void)updateAssets {
    _backgroundImageView.image = [[CommonAsset instance] systemMessageBackground];
}

+ (void)drawDate:(int)date forContainerWidth:(CGFloat)containerWidth inContext:(CGContextRef)context andBindBackgroundToContainer:(UIView *)__unused backgroundContainer atPosition:(CGPoint)__unused position {
    static UIFont *font = nil;
    static CGColorRef color = NULL;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      font = MediumSystemFontOfSize(13.0f);
                      color = CGColorRetain([UIColor whiteColor].CGColor);
                  });
    
    NSString *text = [Utils stringForDialogTime:date * 24 * 60 * 60];
    CGSize textSize ;
    textSize = [Utils sizeForText:text boundSize:backgroundContainer.bounds.size option:NSStringDrawingUsesLineFragmentOrigin lineBreakMode:NSLineBreakByClipping font:font];
    CGPoint textOrigin = CGPointMake(RetinaFloor((containerWidth - textSize.width) / 2), RetinaFloor((27.0f - textSize.height) / 2) + RetinaPixel);
    
    UIImage *backgroundImage = [[CommonAsset instance] systemMessageBackground];
    CGRect backgroundFrame = CGRectMake(textOrigin.x - 10, textOrigin.y - 2, textSize.width + 20, backgroundImage.size.height);
    [backgroundImage drawInRect:backgroundFrame blendMode:kCGBlendModeCopy alpha:1.0f];
    
    CGContextSetFillColorWithColor(context, color);
    [text drawAtPoint:textOrigin withAttributes:@{NSFontAttributeName: font}];
}

- (id )initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setAlpha:1];
        self.layer.opacity =1;
        self.viewIdentifier = @"_headerDate";
        _backgroundImageView = [[UIImageView alloc] initWithImage:[[CommonAsset instance] systemMessageBackground]];
        [self addSubview:_backgroundImageView];
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.transform = CGAffineTransformMakeRotation((float)M_PI);
        _dateLabel.textColor = [UIColor whiteColor];
        _dateLabel.font = [UIFont systemFontOfSize:13];
        _dateLabel.textAlignment = NSTextAlignmentCenter;
        _dateLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_dateLabel];
        _date = INT_MIN;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (void)willBecomeRecycled {
    _date = 0;
    _dateLabel.text = @"";
}

- (void)setDate:(int)date isShowFull:(bool)isShowFull {
    if (_date != date) {
        _dateLabel.text = [Utils stringForDialogTime:date isShowFull:isShowFull];
        _date = date;
        CGRect dateFrame = _dateLabel.frame;
        dateFrame.size = [Utils sizeForText:_dateLabel.text boundSize:self.bounds.size option:NSStringDrawingUsesLineFragmentOrigin lineBreakMode:NSLineBreakByClipping font:_dateLabel.font];
        _dateLabel.frame = dateFrame;
        self.viewStateIdentifier = [[NSString alloc] initWithFormat:@"date/%d", _date];
        [self setNeedsLayout];
    }
}
- (void)layoutSubviews
{
    CGRect bounds = self.bounds;
    
    CGFloat dateOffset = iosMajorVersion() >= 7 ? -1.0f : -RetinaPixel;
    CGRect dateFrame = _dateLabel.frame;
    dateFrame.origin = CGPointMake(RetinaFloor((bounds.size.width - dateFrame.size.width) / 2), RetinaFloor((bounds.size.height - dateFrame.size.height) / 2) + RetinaPixel + dateOffset);
    _dateLabel.frame = dateFrame;
    
    _backgroundImageView.frame = CGRectMake(dateFrame.origin.x - 10, dateFrame.origin.y - 2 + (iosMajorVersion() >= 7 ? 0.0f : RetinaPixel), dateFrame.size.width + 20, _backgroundImageView.frame.size.height);
}

@end
