//
//  KAlertViewController.h
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 3/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KAlertViewController : UIAlertController
+ (UIViewController *)findTopMostPresentedController:(UIViewController *)rootController;
@end
