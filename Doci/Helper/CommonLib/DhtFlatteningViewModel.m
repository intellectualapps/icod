//
//  DhtFlatteningViewModel.m
//  ChatPro
//
//  Created by dungnv9 on 2/26/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtFlatteningViewModel.h"
#import "DhtFlatteningView.h"
#import "DhtViewStorage.h"
#import "Utils.h"

@interface DhtFlatteningViewModel()
{
    bool _needsContentUpdate;
    bool _tiledMode;
    DhtViewStorage      *_viewStorage;
    NSMutableArray *_tiledPartViews;
    NSArray *_currentLinkSelectionViews;
    
}

@end
@implementation DhtFlatteningViewModel

- (instancetype) initWithContext:(DhtViewContext *)viewContext {
    self = [super init];
    if (self) {
        self.alpha =1;
        self->_modelFlags.disableSubmodelAutomaticBinding = 1;
    }
    return self;
}

- (id)init {
    return [self initWithContext:nil];
}

- (Class) viewClass {
    return [DhtFlatteningView class];
}

- (void)clearLinkSelection {
    for (UIView *linkView in _currentLinkSelectionViews) {
        [linkView removeFromSuperview];
    }
    _currentLinkSelectionViews = nil;
}

- (void) updateLinkSelection:(CGPoint)point {
    if ([self boundView]) {
        [self clearLinkSelection];
        
    }
}


- (void)updateSubmodelContentsForVisibleRect:(CGRect)rect
{
    if ([self boundView] != nil) {
        [UIView performWithoutAnimation:^
         {
             DhtFlatteningView *view = (DhtFlatteningView *)[self boundView];
             
             CGSize tiledViewSize = CGSizeMake(view.frame.size.width, 512);
             
             CGRect frame = self.frame;
             for (CGFloat originY = 0.0f; originY < frame.size.height; originY += tiledViewSize.height)
             {
                 NSInteger compareOriginY = (NSInteger)originY;
                 int foundTileIndex = -1;
                 
                 if (_tiledPartViews == nil)
                     _tiledPartViews = [[NSMutableArray alloc] init];
                 
                 int index = -1;
                 for (DhtFlatteningView *tileView in _tiledPartViews)
                 {
                     index++;
                     NSInteger tileOriginY = (NSInteger)tileView.frame.origin.y;
                     if (tileOriginY == compareOriginY)
                     {
                         foundTileIndex = index;
                         break;
                     }
                 }
                 
                 CGRect possibleRect = CGRectMake(rect.origin.x, originY, rect.size.width, tiledViewSize.height);
                 if (!CGRectIsNull(CGRectIntersection(rect, possibleRect)))
                 {
                     if (foundTileIndex == -1)
                     {
                         static NSString *tileViewIdentifier = @"DhtFlatteningView";
                         
                         DhtFlatteningView *tileView = (DhtFlatteningView *)[_viewStorage dequeueViewWithIdentifier:tileViewIdentifier viewStateIdentifier:nil];
                         if (tileView == nil)
                         {
                             tileView = [[DhtFlatteningView alloc] init];
                             tileView.viewIdentifier = tileViewIdentifier;
                         }
                         
                         CGRect tileFrame = CGRectMake(0.0f, originY, tiledViewSize.width, MIN(tiledViewSize.height, frame.size.height - originY));
                         
                         tileView.frame = tileFrame;
                         [_tiledPartViews addObject:tileView];
                         [view addSubview:tileView];
                         
                         [self _updateSubmodelContentsForLayer:tileView.layer visibleRect:tileFrame];
                     }
                 }
                 else if (foundTileIndex != -1)
                 {
                     DhtFlatteningView *tileView = _tiledPartViews[foundTileIndex];
                     [tileView removeFromSuperview];
                     [_viewStorage enqueueView:tileView];
                     [_tiledPartViews removeObjectAtIndex:foundTileIndex];
                 }
             }
         }];
    }
}

- (void)_dropTiledPartViews
{
    if (_tiledPartViews != nil)
    {
        for (DhtFlatteningView *tileView in _tiledPartViews)
        {
            [tileView removeFromSuperview];
            [_viewStorage enqueueView:tileView];
        }
        
        [_tiledPartViews removeAllObjects];
    }
}
- (void) bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage
{
    [super bindViewToContainer:container viewStorage:viewStorage];
    _viewStorage = viewStorage;
    [self _updateSubmodelContents];
    [self boundView].userInteractionEnabled =false;
}

- (void) unbindView:(DhtViewStorage *)viewStorage
{
    [super unbindView:viewStorage];
    [self _dropTiledPartViews];
    _needsContentUpdate =false;
}

- (void)_updateSubmodelContents
{
    DhtFlatteningView *view = (DhtFlatteningView *)[self boundView];
    if (view != nil && !CGRectIsEmpty(self.frame))
    {
        [self _updateSubmodelContentsForLayer:view.layer visibleRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
}

- (void)updateSubmodelContentsIfNeeded
{
    if (_needsContentUpdate)
        [self _updateSubmodelContents];
}

- (void)setNeedsSubmodelContentsUpdate
{
    _needsContentUpdate = true;
}

- (void)_updateSubmodelContentsForLayer:(CALayer *)layer visibleRect:(CGRect)visibleRect
{
    CGRect clipRect = CGRectIntersection(visibleRect, CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height));
    if (clipRect.size.height < FLT_EPSILON)
        return;
    
    CGSize contextSize = clipRect.size;
    CGContextRef context = [DhtFlatteningViewModel _createContentContext:contextSize];
    UIGraphicsPushContext(context);
    
    CGContextTranslateCTM(context, contextSize.width / 2.0f, contextSize.height / 2.0f);
    CGContextScaleCTM(context, 1.0f, -1.0f);
    CGContextTranslateCTM(context, -contextSize.width / 2.0f, -contextSize.height / 2.0f);
    
    if (visibleRect.origin.y > FLT_EPSILON)
        CGContextTranslateCTM(context, 0.0f, -visibleRect.origin.y);

    [self drawSubmodelsInContext:context];
    
    UIGraphicsPopContext();
    
    CGImageRef contextImageRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    layer.contents = (__bridge id)(contextImageRef);
    CGImageRelease(contextImageRef);
}

+ (CGContextRef)_createContentContext:(CGSize)size
{
    CGSize contextSize = size;
    CGFloat scaling = ScreenScaling();
    
    contextSize.width *= scaling;
    contextSize.height *= scaling;
    
    size_t bytesPerRow = 4 * (int)contextSize.width;
    bytesPerRow = (bytesPerRow + 15) & ~15;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Host;
    
    CGContextRef context = CGBitmapContextCreate(NULL, (int)contextSize.width, (int)contextSize.height, 8, bytesPerRow, colorSpace, bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    
    CGContextScaleCTM(context, scaling, scaling);
    
    return context;
}
@end
