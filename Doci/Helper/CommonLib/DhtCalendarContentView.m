


//
//  DhtCalendarContentView.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtCalendarContentView.h"
#import "DhtCalendarDayCell.h"
#import "DhtCalendarUtils.h"
#import "DayInfo.h"
#import "MonthInfo.h"
#import "NSDate+Extension.h"
#import "DhtCalendarDataSource.h"

@interface DhtCalendarContentBackgroundView: UIView {
    
}
@property (nonatomic, strong) UIBezierPath *path;
@end

@implementation DhtCalendarContentBackgroundView

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    if (_path) {
        [[UIColor lightGrayColor] setStroke];
        [_path stroke];
    }
}


@end
static NSString *cellIdentifier = @"dhtCalendarDayCell";

@interface DhtCalendarContentView()<DhtCalendardayCellDelegate>
@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, strong) DhtCalendarContentBackgroundView *backgroundView;
@end

@implementation DhtCalendarContentView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        _backgroundView = [[DhtCalendarContentBackgroundView alloc] initWithFrame:CGRectZero];
        _backgroundView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)updateConstraints {
    [super updateConstraints];
    if (self.superview == nil) {
        return;
    }
    NSMutableArray *constraints = [NSMutableArray array];
    NSLayoutConstraint *constraint =  [NSLayoutConstraint constraintWithItem:self
                                                                   attribute:NSLayoutAttributeTop
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.superview
                                                                   attribute:NSLayoutAttributeTop
                                                                  multiplier:1
                                                                    constant:0];
    [constraints addObject:constraint];
    constraint = [NSLayoutConstraint constraintWithItem:self
                                              attribute:NSLayoutAttributeTrailing
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.superview
                                              attribute:NSLayoutAttributeTrailing
                                             multiplier:1
                                               constant:0];
    [constraints addObject:constraint];
    constraint = [NSLayoutConstraint constraintWithItem:self
                                              attribute:NSLayoutAttributeLeading
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.superview
                                              attribute:NSLayoutAttributeLeading
                                             multiplier:1
                                               constant:0];
    [constraints addObject:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self
                                              attribute:NSLayoutAttributeBottom
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.superview
                                              attribute:NSLayoutAttributeBottom
                                             multiplier:1
                                               constant:0];
    [constraints addObject:constraint];
    [NSLayoutConstraint activateConstraints:constraints];
}

- (void)log {
    for (NSLayoutConstraint *constraint in self.constraints) {
        NSLog(@"constaint %@", constraint);
    }
}

- (NSArray *)listDayInfos {
    return [NSArray arrayWithArray: [_monthInfo listOfDays]];
}

- (void)generatePathToDraw {
    CGSize bounsSize = self.bounds.size;
    bounsSize.height = [self sizeForDayView].height * (self.monthInfo.listOfDays.count) / 7;
    CGSize itemSize = [self sizeForDayView];
    NSInteger numberRows = self.monthInfo.listOfDays.count / 7;
    NSInteger numberColumns = 7;
    UIBezierPath *path = [UIBezierPath bezierPath];
    //start (0,0)
    [path moveToPoint: CGPointMake(0.0, 0.0)];
    [path addLineToPoint: CGPointMake(bounsSize.width, 0.0)];
    [path addLineToPoint: CGPointMake(bounsSize.width, bounsSize.height - 1)];
    [path addLineToPoint: CGPointMake(0.0, bounsSize.height - 1)];
    [path addLineToPoint: CGPointMake(0.0, 0.0)];
    for (int i = 1; i < numberColumns; i++) {
        CGFloat originX = i * itemSize.width;
        [path moveToPoint:CGPointMake(originX, 0.0)];
        [path addLineToPoint:CGPointMake(originX, bounsSize.height)];
    }
    
    for (int i = 1; i < numberRows; i++) {
        CGFloat originY = i * itemSize.height;
        [path moveToPoint:CGPointMake(0.0, originY)];
        [path addLineToPoint:CGPointMake(bounsSize.width, originY)];
    }

    path.lineWidth = 1.0;
    _path = path;
}

- (void)reloadContent:(MonthInfo *)mInfo {
    _monthInfo = mInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self renderView];
        [self draw];
    });
}

- (void)draw {
    [self generatePathToDraw];
    self.backgroundView.path = self.path;
    [self.backgroundView setNeedsDisplay];
}

- (NSDate *)selectedDay {
    if (self.monthInfo.monthDate) {
        return self.monthInfo.monthDate;
    }
    for (DayInfo *info in self.monthInfo.listOfDays) {
        if (info.isSelected) {
            return info.date;
        }
    }
    return nil;
}

- (DhtCalendarDayCell *)dayViewWithDate:(NSDate *)date {
    if (!date) {
        return nil;
    }
    
    for (DhtCalendarDayCell *dView in [self dayViews]) {
        NSDate *dayDate = dView.dayInfo.date;
        if ([dayDate year] == [date year] && [dayDate month] == [date month] && [dayDate day] == [date day]) {
            return dView;
        }
    }
    return nil;
}

#pragma --mark render
- (void)renderView {
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self addSubview:_backgroundView];
    CGSize itemSize = [self sizeForDayView];
    NSInteger numberRows = self.monthInfo.listOfDays.count / 7;
    NSInteger numberColumns = 7;
    CGRect itemRect = CGRectZero;
    itemRect.size = itemSize;
    NSInteger index = 0;
    for (int i = 0; i < numberRows; i++) {
        itemRect.origin.y = i *itemRect.size.height;
        for (int j = 0; j < numberColumns; j++) {
            index = j  + numberColumns *i;
            itemRect.origin.x = j * itemRect.size.width;
            DhtCalendarDayCell *cell = [[DhtCalendarDayCell alloc] initWithFrame:itemRect];
            DayInfo *info = [self.monthInfo.listOfDays objectAtIndex:index];
            [cell displayContent:info];
            cell.delegate = self;
            [self addSubview:cell];
        }
    }
    _backgroundView.frame = CGRectMake(0, 0, self.bounds.size.width, itemRect.size.height * numberRows);
    _backgroundView.backgroundColor = [UIColor whiteColor];
    if (self.delegate) {
        [self.delegate didRenderDayView:self];
    }
}

- (NSInteger)numberOfDaysSelected {
    NSInteger counter = 0;
    for (DayInfo *info in self.monthInfo.listOfDays) {
        if (info.isSelected) {
            counter += 1;
        }
    }
    return counter;
}

- (void)applyMode:(BOOL)isCreateNewCalendar {
    
}

- (NSArray *)dayViews {
    NSArray *subviews = self.subviews;
    NSMutableArray *result = [NSMutableArray array];
    if (subviews.count > 0 ) {
        for (UIView* view in subviews) {
            if ([view isKindOfClass:[DhtCalendarDayCell class]]) {
                [result addObject:view];
            }
        }
    }
    return result;
}

- (void)resetSelections {
    for (UIView *view in [self dayViews]) {
        DhtCalendarDayCell *dView = (DhtCalendarDayCell *)view;
        dView.dayInfo.isSelected = NO;
        [dView removeSelectionView];
    }
}

#pragma MARK calculating
- (CGSize)sizeForDayView {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    NSUInteger numberOfItems = [self numberOfItemPerRow];
    CGFloat maxWidth = screenBounds.size.width;
    CGFloat itemWidth = maxWidth / numberOfItems;
    return CGSizeMake(itemWidth, itemWidth * 0.8);
}

- (NSUInteger)numberOfItemPerRow {
    return 7;
}

- (CGFloat)totalHeight {
    CGSize size = [self sizeForDayView];
    NSInteger numberRows = self.monthInfo.numberOfWeaks;
    return size.height * numberRows;
}

#pragma --mark DayView delegate
- (void)didSelectDayView:(DhtCalendarDayCell *)dayView {
    if (self.delegate) {
        [self.delegate didSelectDayView:dayView];
    }
}

- (UIView *)supplementViewForBound:(CGRect)bound {
    if (self.delegate) {
        return [self.delegate supplementViewForBound:bound];
    }
    return nil;
}

- (UIColor *)dayInPastBackgroundColor {
    if (self.delegate) {
        return [self.delegate dayInPastBackgroundColor];
    }
    return nil;
}
@end
