//
//  MessageItem.h
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MessageClone.h"
#import "DhtViewContext.h"
#import "DhtViewStorage.h"
#import "GenericItem.h"

@class CollectionCell;
@class DhtViewModel;

@interface MessageItem : GenericItem
{

}
@end
