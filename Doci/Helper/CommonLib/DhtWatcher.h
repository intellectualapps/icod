//
//  DhtWatcher.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/31/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DhtHandle;
@protocol DhtWatcher <NSObject>
/*This is require. Any class comfortable with this protocol must define like this*/
@property (nonatomic, strong) DhtHandle  *handler;

@optional
/*  report progress to the destination in case download/upload*/
- (void)actionNotifyProgress:(NSString *)path progress:(float)progress;

/*  when you're working with resource like as file/database. After process done we can notify to dest*/
- (void)actionNotifyResourceDispatched:(NSString *)path resource:(id)resource arguments:(id)arguments;
- (void)actorMessageReceived:(NSString *)path messageType:(NSString *)messageType message:(id)message;
/* Any actor complete (success/fail) also call this*/
- (void)actorProcessCompletedAtpath:(NSString *)path result:(id)result error:(NSError *)error;
- (void)actionStageActionRequested:(NSString *)action options:(id)options;
@end
