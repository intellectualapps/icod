//
//  DhtViewStorage.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/12/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtViewStorage.h"
#import "Utils.h"

@interface DhtViewStorage()
{
    NSMutableDictionary *_viewsByIdentifier;
    NSMutableDictionary *_resurrectionViewsByIdentifier;
    
    bool _resurrectionEnabled;
}
@end

@implementation DhtViewStorage

- (instancetype) init {
    self = [super init];
    if (self) {
        _viewsByIdentifier = [[NSMutableDictionary alloc]init];
        _resurrectionViewsByIdentifier = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)enqueueView:(UIView<BaseViewProtocol> *)view {
    NSString *identifier = [view viewIdentifier];
    if (identifier == nil) {
        [KLogger log: @"***** enqueueView: view doesn't have valid identifier"];
        return;
    }
    
    NSMutableDictionary *concreteQueues = _resurrectionEnabled ? _resurrectionViewsByIdentifier : _viewsByIdentifier;
    NSMutableArray *enqueuedViews = [concreteQueues objectForKey:identifier];
    if (enqueuedViews == nil) {
        enqueuedViews = [[NSMutableArray alloc] init];
        [concreteQueues setObject:enqueuedViews forKey:identifier];
    }
    
    if (!_resurrectionEnabled)
        [view willBecomeRecycled];
    
    [enqueuedViews addObject:view];
}

- (void)allowResurrectionForOperations:(dispatch_block_t)block {
    _resurrectionEnabled = true;
    
    block();
    
    _resurrectionEnabled = false;
    [_resurrectionViewsByIdentifier enumerateKeysAndObjectsUsingBlock:^(NSString *identifier, NSArray *views, __unused BOOL *stop) {
         for (UIView<BaseViewProtocol> *view in views) {
             [view willBecomeRecycled];
         }
         
         NSMutableArray *enqueuedViews = [_viewsByIdentifier objectForKey:identifier];
         if (enqueuedViews == nil) {
             enqueuedViews = [[NSMutableArray alloc] init];
             [_viewsByIdentifier setObject:enqueuedViews forKey:identifier];
         }
         [enqueuedViews addObjectsFromArray:views];
     }];
    
    [_resurrectionViewsByIdentifier removeAllObjects];
}

- (UIView<BaseViewProtocol> *)_dequeueViewFromQueues:(NSMutableDictionary *)queues withIdentifier:(NSString *)identifier viewStateIdentifier:(NSString *)viewStateIdentifier {
    NSMutableArray *enqueuedViews = [queues objectForKey:identifier];
    
    if (enqueuedViews != nil) {
        UIView<BaseViewProtocol> *view = nil;
        if (viewStateIdentifier != nil) {
            for (UIView<BaseViewProtocol> *candidateView in enqueuedViews) {
                if (StringCompare(candidateView.viewStateIdentifier, viewStateIdentifier)) {
                    view = candidateView;
                    break;
                }
            }
        }
        
        if (view == nil)
            view = [enqueuedViews lastObject];
        
        if (view != nil) {
            [enqueuedViews removeObject:view];
            return view;
        }
    }
    return nil;
}

- (UIView<BaseViewProtocol>*)dequeueViewWithIdentifier:(NSString *)identifier viewStateIdentifier:(NSString *)viewStateIdentifier {
    UIView<BaseViewProtocol> *view = nil;
    if (_resurrectionViewsByIdentifier.count > 0) {
        return [self _dequeueViewFromQueues:_resurrectionViewsByIdentifier withIdentifier:identifier viewStateIdentifier:viewStateIdentifier];
    } else {
        return [self _dequeueViewFromQueues:_viewsByIdentifier withIdentifier:identifier viewStateIdentifier:viewStateIdentifier];
    }
    return view;
}

- (void) clear {
    [_viewsByIdentifier removeAllObjects];
    [_resurrectionViewsByIdentifier removeAllObjects];
    _resurrectionEnabled = false;
}

@end
