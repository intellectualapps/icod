//
//  Utils.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 3/3/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>
#import <pthread.h>
#include <sys/sysctl.h>
#import "Constant.h"
#import "KLogger.h"

typedef NS_ENUM(int16_t, MessageStatus) {
    MessageStatusSending = 0,
    MessageStatusFailed = 1,
    MessageStatusUnread = 2,
    MessageStatusRead = 3,
};

typedef NS_ENUM(NSInteger, ReusableLabelLayout) {
    ReusableLabelLayoutMultiline = 1,
    ReusableLabelLayoutHighlightLinks = 2,
    ReusableLabelLayoutDateSpacing = 4,
    ReusableLabelLayoutExtendedDateSpacing = 8
};

#ifdef __cplusplus
extern "C" {
#endif
    extern CGFloat RetinaPixel;
    bool IsRetina();
    int iosMajorVersion();
    bool StringCompare(NSString *s1, NSString *s2);
    CGSize  ScreenSize();
    CGFloat ScreenScaling();
    UIFont *SystemFontOfSize(CGFloat size);
    UIFont *MediumSystemFontOfSize(CGFloat size);
    bool IsPad();
    UIFont *BoldSystemFontOfSize(CGFloat size);
    NSString *appSortVersion();
    NSString *currentLanguage();
    int cpuCoreCount();
    UIColor *AccentColor();
    UIImage *FixOrientationAndCrop(UIImage *source, CGRect cropFrame, CGSize imageSize);
    CTFontRef CoreTextFontOfNameAndSize(CGFloat size,  BOOL isBold);
    CGFloat RetinaFloor(CGFloat value);
#ifdef __cplusplus
}
#endif

@interface Utils : NSObject
+ (UIInterfaceOrientation)statusBarOrientation;
+ (void)dispatchMainSyncSafe:(dispatch_block_t)block;
+ (void)dispatchOnMainAsyncSafe:(dispatch_block_t)block;
+ (CGSize) sizeForText:(NSString *)text
             boundSize:(CGSize)bsize
                option:(NSStringDrawingOptions)option
         lineBreakMode:(NSLineBreakMode)lineBreakMode
                  font:(UIFont *)font;
+ (NSString *)monthAndYearWithTimeInterval:(NSTimeInterval)time;
+ (NSString *)hourAndSecondWithTimeInterval:(long)time;
+ (BOOL)validateEmail:(NSString *)inputText;
+ (NSInteger)numberOfAlphaBetInString:(NSString *)string;
+ (BOOL)allowsAppendString:(NSString *)string;
+ (BOOL)isKatanaValid:(NSString *)input;
+ (UIImage *)cropImageCorrespondingWithCurrentDevice:(UIImage *)originImage;
+ (NSString *)getExpireMonthFromValidityPeriod:(NSString *)validityPeriod;
+ (void)saveAlertSettingCount:(NSInteger)count;
+ (NSString *)formatAmount:(NSInteger)amount;
+ (NSInteger)amountValueFromString:(NSString *)string;
+ (NSString *)replaceString:(NSString *)origin withString:(NSString *)str range:(NSRange)range;
+ (void)saveDeviceToken:(NSString *)token;
+ (NSString *)deviceToken;
+ (NSString *)deviceId;
+ (NSString *)generateUUID;
+ (NSArray *)textCheckingResults:(NSString *)_text;
+ (NSArray *) additionAttributeForUrls:(NSArray *)checkingResult;
+ (NSInteger)yearOfTimeInterval:(NSTimeInterval)time;
+ (NSString *)stringForDialogTime:(int)time;
+ (NSString *)stringForDialogTime:(int)time
                       isShowFull:(bool) isShowFull;
+ (BOOL)is24hTimeFromDate:(NSDate *)date;
+ (UIImage *)imageFromColor:(UIColor *)color;
+ (BOOL)isAMTime:(NSDate *)date;
+ (NSInteger)hourFromDate:(NSDate *)date;
+ (NSInteger)minFromDate:(NSDate *)date;
+ (NSInteger)monthFromDate:(NSDate *)date;
+ (NSInteger)dayFromDate:(NSDate *)date;
+ (NSInteger)yearFromDate:(NSDate *)date;
+ (NSString *)libaryPath;
+ (NSString *)tempPath;
+ (NSString *)recordedPath;
@end
