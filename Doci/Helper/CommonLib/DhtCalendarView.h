//
//  DhtCalendarView.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DhtCalendarUtils.h"
#import "iCarousel.h"

@class MonthInfo;
@interface DhtCalendarView : UIView
@property (nonatomic, weak) id<DhtCalendarViewDelegate> delegate;
@property (nonatomic, readonly) DhtCalendarContentView *currentMonthView;
@property (nonatomic, readonly) NSInteger currentMonthIndex;

- (void)autoSelectCurrentMonthForDate:(NSDate *)date animated:(BOOL)animated;
//days: contains date object
- (void)selectDays:(NSArray *)days ofMonth:(MonthInfo *)monthInfo;
- (MonthInfo *)monthInfoWithDate:(NSDate *)date;
- (void)resetSelection;
- (void)correctFrame;
- (CGFloat)totalHeight;
- (void)allowsScroll:(BOOL)allows;
- (void)commonInit;
@end


