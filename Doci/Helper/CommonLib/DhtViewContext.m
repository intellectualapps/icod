//
//  DhtViewContext.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/31/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtViewContext.h"
#import "CommonAsset.h"

@implementation DhtViewContext
- (instancetype)init {
    self = [super init];
    if (self) {
        _needShowRealAvatar = true;
        _defaultAvatar = [[CommonAsset instance] defaultDriverAvatar];
    }
    return self;
}
@end
