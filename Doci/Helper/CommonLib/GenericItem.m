//
//  GenericItem.m
//  kids_taxi_driver
//
//  Created by Nguyen Van Dung on 5/23/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "GenericItem.h"
#import "CollectionCell.h"

@interface GenericItem() {
    NSString *_viewIdentifier;
    __weak CollectionCell  *_cell;
    DhtViewModel    *_viewModel;
    DhtViewContext  *_viewContext;
}
@end

@implementation GenericItem
- (instancetype) initWithContent:(GenericInfo *)info viewContext:(DhtViewContext *)context {
    self = [super init];
    if (self) {
        _viewContext = context;
        self.contentInfo = info;
    }
    return self;
}

- (DhtViewContext *)viewContext {
    return _viewContext;
}

- (Class)cellClass {
    return [CollectionCell class];
}

- (id)copyWithZone:(NSZone *)__unused zone {
    GenericItem *item = [[GenericItem alloc] initWithContent:_contentInfo viewContext:_viewContext];
    item.isShowFull = self.isShowFull;
    return item;
}

- (id)deepCopy {
    GenericItem *item = [[GenericItem alloc] initWithContent:[_contentInfo copy] viewContext:_viewContext];
    item.isShowFull = self.isShowFull;
    item->_viewModel = self.viewModel;
    return item;
}

- (DhtViewModel *)viewModel {
    return _viewModel;
}

- (CollectionCell *)dequeueCollectionCell:(UICollectionView *)collectionView registeredIdentifiers:(NSMutableSet *)registeredIdentifiers forIndexPath:(NSIndexPath *)indexPath {
    if (_viewIdentifier == nil) {
        _viewIdentifier = [[NSString alloc] initWithFormat:@"View_%@", NSStringFromClass([self cellClass])];
    }
    if (![registeredIdentifiers containsObject:_viewIdentifier]) {
        [collectionView registerClass:[self cellClass] forCellWithReuseIdentifier:_viewIdentifier];
        [registeredIdentifiers addObject:_viewIdentifier];
    }
    return [collectionView dequeueReusableCellWithReuseIdentifier:_viewIdentifier forIndexPath:indexPath];
}

- (CGSize)sizeForContainerSize:(CGSize)containerSize {
    return CGSizeMake(containerSize.width, [self viewModelForContainerSize:containerSize].frame.size.height);
}

- (DhtViewModel *)viewModelForContainerSize:(CGSize)containerSize {
    bool updateCell = false;
    if (_viewModel == nil) {
        _viewModel = [self createViewModel:self containerSize:containerSize];
        updateCell = true;
    }
    [_viewModel layoutForContainerSize:containerSize];
    return _viewModel;
}


- (DhtViewModel *)createViewModel:(GenericItem *)info containerSize:(CGSize)containerSize {
    return nil;
}

- (bool)collapseWithItem:(GenericItem *)item forContainerSize:(CGSize)__unused containerSize
{
    return false;
}

- (void) bindCell:(CollectionCell *)cell viewStorage:(DhtViewStorage *)viewStorage {
    if (cell.boundItem == self) {
        cell.boundItem = nil;
    }
    _cell = cell;
    ((CollectionCell *)_cell).boundItem = self;
    UIView *contentView = (UIView *)[cell contentViewForBinding];
    [_viewModel bindViewToContainer:contentView viewStorage:viewStorage];
}

- (void)unbindCell:(DhtViewStorage *)viewStorage {
    //[_viewModel unbindView:viewStorage];
    
    if (((CollectionCell *)_cell).boundItem == self)
        ((CollectionCell *)_cell).boundItem = nil;
    _cell = nil;
}

- (void) moveToCell:(CollectionCell *)cell {
    if (((CollectionCell *)_cell).boundItem == self)
        ((CollectionCell *)_cell).boundItem = nil;
    _cell = cell;
    ((CollectionCell *)_cell).boundItem = self;
    if ([self boundCell] == cell){
        [_viewModel moveViewToContainer:(UIView *)[cell contentViewForBinding]];
    }
}

- (void)updateToItem:(GenericItem *)updatedItem viewStorage:(DhtViewStorage *)viewStorage {
    if ([updatedItem isKindOfClass:[GenericItem class]]) {
        _contentInfo = updatedItem.contentInfo;
        [self updateMessage:_contentInfo viewStorage:viewStorage];
    }
}

- (void)updateMessage:(GenericInfo *)info viewStorage:(DhtViewStorage *)viewStorage
{
    [_viewModel updateItemInfo:info viewStorage:viewStorage];
}

- (void)temporaryMoveToView:(UIView *)__unused view {
    [_viewModel moveViewToContainer:view];
    if (((CollectionCell *)_cell).boundItem == self)
        ((CollectionCell *)_cell).boundItem = nil;
    _cell = nil;
}

- (void)drawInContext:(CGContextRef)context {
    [_viewModel drawInContext:context];
}

- (CollectionCell *)boundCell {
    return _cell;
}

- (void)relativeBoundsUpdated:(CollectionCell *)cell bounds:(CGRect)bounds {
    if (cell == [self boundCell]) {
        if ([_viewModel needsRelativeBoundsUpdates]) {
            [_viewModel relativeBoundsUpdated:bounds];
        }
    }
}

- (CGRect)effectiveContentFrame {
    return [_viewModel effectiveContentFrame];
}

- (void)setTemporaryHighlighted:(bool)temporaryHighlighted viewStorage:(DhtViewStorage *)viewStorage {
    [_viewModel setTemporaryHighlighted:temporaryHighlighted viewStorage:viewStorage];
}

@end
