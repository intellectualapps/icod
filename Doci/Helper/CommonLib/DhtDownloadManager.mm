//
//  DhtDownloadManager.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtDownloadManager.h"
#import "DhtHandle.h"
#import "DhtActionControl.h"
DhtDownloadManager *downloadManager(){
    static DhtDownloadManager *singleton = nil;
    static dispatch_once_t onceToken;
    /*This will make this class thread safe access*/
    dispatch_once(&onceToken, ^
                  {
                      singleton = [[DhtDownloadManager alloc] init];
                  });
    
    return singleton;
}


@implementation DownloadItem
- (id)copyWithZone:(NSZone *)__unused zone
{
    DownloadItem *newItem = [[DownloadItem alloc] init];
    newItem.compareKey = _compareKey;
    newItem.path = _path;
    newItem.requestDate = _requestDate;
    newItem.progress = _progress;
    newItem.willNotifyToTargetWatcherAfterDownloadFinish = _willNotifyToTargetWatcherAfterDownloadFinish;
    newItem.groupCompareKey = _groupCompareKey;
    return newItem;
}

@end

@interface DhtDownloadManager()
@property (nonatomic, strong) NSMutableDictionary   *itemQueue;
@end

@implementation DhtDownloadManager
- (id) init {
    self = [super init];
    if (self) {
        _itemQueue = [[NSMutableDictionary alloc]init];
        _handler = [[DhtHandle alloc]initWithDelegate:self willReleaseOnMainThread:false];
    }
    return self;
}

- (void) dealloc {
    [_handler reset];
   // [actionManagerInstance() removeWatcher:self];
}

- (void)requestDownloadItem:(NSString *)path
                     object:(id <BaseObject>)info
                   groupKey:(NSString *)groupKey
          willPostToWatcher:(BOOL) willPost
             changePriority:(bool)changePriority
        requestIfNotRunning:(bool)requestIfNotRunning {
    
}

- (void)requestState:(DhtHandle *)watcherHandle {
    
}

- (void)cancelItemWithPath:(NSString *) path {
    
}

- (void)cancelDownloadItem:(id<BaseObject>)msg {
    
}

- (void)cancelItemsWithGroupId:(NSString *)groupId {
    
}

@end
