//
//  DhtCalendarMenuView.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DhtCalendarMenuViewDelegate <NSObject>
@optional
- (NSArray *)calendarMenuSymbols;
- (NSArray *)calendarMenuBackgroundColors;
- (UIFont *)menuItemFont;
- (UIColor *)menuItemTextColor;
@end

@interface DhtCalendarMenuView : UIView
@property (nonatomic, weak) id<DhtCalendarMenuViewDelegate> delegate;
- (void)setup;
@end
