//
//  DhtDownloadManager.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"
#import "DhtWatcher.h"
@class DhtDownloadManager;
#ifdef __cplusplus
extern "C" {
#endif
    
    DhtDownloadManager *downloadManager();
    
#ifdef __cplusplus
}
#endif
@interface DownloadItem : NSObject {
    
}
/*
 This flag detech afer download item success, download manager will notify to target handler or not
 */
@property (nonatomic, assign) BOOL willNotifyToTargetWatcherAfterDownloadFinish;
//This is request path (see actor for more detail)
@property (nonatomic, strong)  NSString   *path;
//download progress
@property (nonatomic, assign) float progress;
@property (nonatomic) NSTimeInterval requestDate;
//use to detect all object has same group id. I help to cancel request all item in group
@property (nonatomic) NSString* groupCompareKey;
//use to detect download item for some purpose such as cancel download.
@property (nonatomic) NSString* compareKey;
@end

@interface DhtDownloadManager : NSObject<DhtWatcher> {
    
}
@property (nonatomic, strong) DhtHandle     *handler;

- (void)requestDownloadItem:(NSString *)path
                      object:(id <BaseObject>)info
                    groupKey:(NSString *)groupKey
           willPostToWatcher:(BOOL) willPost
              changePriority:(bool)changePriority
         requestIfNotRunning:(bool)requestIfNotRunning;

- (void)requestState:(DhtHandle *)watcherHandle;
- (void)cancelItemWithPath:(NSString *) path;
- (void)cancelDownloadItem:(id<BaseObject>)msg;
- (void)cancelItemsWithGroupId:(NSString *)groupId;
@end
