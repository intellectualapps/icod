//
//  HPTextViewInternal.m
//
//  Created by Hans Pinckaers on 29-06-10.
//
//	MIT License
//
//	Copyright (c) 2011 Hans Pinckaers
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.

#import "HPTextViewInternal.h"
#import "Utils.h"
#import <objc/runtime.h>
#import <objc/message.h>
@implementation HPTextViewInternal

@synthesize placeholder;
@synthesize placeholderColor;
@synthesize displayPlaceHolder;
+ (void)addTextViewMethods
{
   
}

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.userInteractionEnabled =true;
    }
    return self;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    textView.inputAccessoryView = nil;
    textView.inputView = nil;
    return true;
}
- (void)textViewEnsureSelectionVisible
{
    CGRect caretFrame = [self caretRectForPosition:self.selectedTextRange.end];
    if (!CGRectIsInfinite(caretFrame) && caretFrame.size.height > 0  && self.contentSize.height > self.bounds.size.height)
    {
        UIEdgeInsets implicitInset = UIEdgeInsetsMake(8, 0, 8, 0);
        
        caretFrame.origin.y -= implicitInset.top;
        caretFrame.size.height += implicitInset.top + implicitInset.bottom;
        caretFrame.origin.y = floorf(caretFrame.origin.y * 2.0f) / 2.0f;
        caretFrame.size.height = floorf(caretFrame.size.height * 2.0f) / 2.0f;
        
        CGFloat frameHeight = self.frame.size.height;
        CGPoint contentOffset = self.contentOffset;
        
        if (caretFrame.origin.y < contentOffset.y)
            contentOffset.y = caretFrame.origin.y;
        if (caretFrame.origin.y + caretFrame.size.height > contentOffset.y + frameHeight)
            contentOffset.y = caretFrame.origin.y + caretFrame.size.height - frameHeight;
        contentOffset.y = MAX(0, contentOffset.y);
        if (!CGPointEqualToPoint(contentOffset, self.contentOffset))
            self.contentOffset = contentOffset;
    }
}

-(void)setText:(NSString *)text
{
    BOOL originalValue = self.scrollEnabled;
    //If one of GrowingTextView's superviews is a scrollView, and self.scrollEnabled == NO,
    //setting the text programatically will cause UIKit to search upwards until it finds a scrollView with scrollEnabled==yes
    //then scroll it erratically. Setting scrollEnabled temporarily to YES prevents this.
    [self setScrollEnabled:YES];
    [super setText:text];
    [self setScrollEnabled:originalValue];
}

- (void)setScrollable:(BOOL)isScrollable
{
    [super setScrollEnabled:isScrollable];
}


- (void)textViewAdjustScrollRange:(NSRange)range animated:(BOOL)animated
{
    static SEL selector = NULL;
    static void (*impl)(id, SEL, NSRange, BOOL) = NULL;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      Method method = class_getInstanceMethod([UITextView class], selector);
                      if (method != NULL)
                          impl = (void (*)(id, SEL, NSRange, BOOL))method_getImplementation(method);
                  });
    
    animated = false;
    
    if (impl != NULL)
        impl(self, selector, range, animated);
}
-(void)setContentOffset:(CGPoint)s
{
	if(self.tracking || self.decelerating){
		//initiated by user...
        
        UIEdgeInsets insets = self.contentInset;
        insets.bottom = 0;
        insets.top = 0;
        self.contentInset = insets;
        
	} else {

		float bottomOffset = (self.contentSize.height - self.frame.size.height + self.contentInset.bottom);
		if(s.y < bottomOffset && self.scrollEnabled){            
            UIEdgeInsets insets = self.contentInset;
            insets.bottom = 8;
            insets.top = 8;
            self.contentInset = insets;            
        }
	}
    
	[super setContentOffset:s];
    
}
- (void)commonInitialiser
{
    
}
- (BOOL)becomeFirstResponder
{
    if (!_enableFirstResponder){
       // return false;
    }
    BOOL result = false;
    result = [super becomeFirstResponder];
    return result;
}
-(void)setContentInset:(UIEdgeInsets)s
{
	UIEdgeInsets insets = s;
	
	if(s.bottom>8) insets.bottom = 0;
	insets.top = 0;

	[super setContentInset:insets];
}
- (void)scrollRectToVisible:(CGRect)__unused rect animated:(BOOL)__unused animated
{

}
-(void)setContentSize:(CGSize)contentSize
{
    // is this an iOS5 bug? Need testing!
    [super setContentSize:contentSize];
    //[self scrollRectToVisible:CGRectMake(0, 0, contentSize.height - self.bounds.size.height + 8, 1) animated:YES];
    [self textViewEnsureSelectionVisible];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (displayPlaceHolder && placeholder && placeholderColor) {
        [placeholderColor set];
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:self.font forKey:NSFontAttributeName];
        [placeholder drawInRect:CGRectMake(8.0f, 8.0f, self.frame.size.width - 16.0f, self.frame.size.height - 16.0f) withAttributes:attributes];

    }
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self resignFirstResponder];
    self.inputAccessoryView = nil;
    self.inputView = nil;
    [self becomeFirstResponder];
    [super touchesBegan:touches withEvent:event];
}


@end
