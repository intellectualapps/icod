//
//  MenuView.h
//  MtoM
//
//  Created by Nguyen Van Dung on 3/31/16.
//  Copyright © 2016 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DhtHandle.h"
@interface MenuView : UIView
- (void)setUserInfo:(NSDictionary *)userInfo;
- (void)setButtonsAndActions:(NSArray *)buttonsAndActions watcherHandle:(DhtHandle *)watcherHandle;
- (void)showInView:(UIView *)view fromRect:(CGRect)rect;
@property (nonatomic, strong) DhtHandle *watcherHandle;
- (void)hide:(dispatch_block_t)completion;
@end
