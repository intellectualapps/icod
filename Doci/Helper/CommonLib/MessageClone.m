
//
//  MessageClone.m
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "MessageClone.h"
#import "Message.h"
@implementation MessageClone
/*
 sending: 送信中
 unread: 未読
 read: 既読
 */
- (NSString *)statuString {
    switch (self.status) {
        case MessageStatusRead:
            return @"既読";
        default:
            return @"";
    }
}

- (NSString *)timeString {
    if (self.createdDate > 0) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.createdDate];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        return [formatter stringFromDate:date];
    }
    return @"";
}

- (id)copyWithZone:(NSZone *)__unused zone
{
    MessageClone *clone = [[MessageClone alloc] init];
    clone.conversationId = self.conversationId;
    clone.localIdentifier = self.localIdentifier;
    clone.objectId = self.objectId;
    clone.content = self.content;
    clone.avatarUrl = self.avatarUrl;
    clone.uid = self.uid;
    clone.createdDate = self.createdDate;
    clone.modifiedDate = self.modifiedDate;
    clone.incoming = self.incoming;
    clone.status = self.status;
    clone.orderId = self.orderId;
    return clone;
}

- (void)copyContentFromMessageFromSerer:(MessageClone *)clone {
    self.conversationId = self.conversationId;
    self.objectId = clone.objectId;
    self.content = clone.content;
    self.avatarUrl = clone.avatarUrl;
    self.uid = clone.uid;
    self.createdDate = clone.createdDate;
    self.modifiedDate = clone.modifiedDate;
    self.incoming = clone.incoming;
    self.status = clone.status;
}

- (instancetype) initWithCoreDataEntity:(Message *)_mo {
    self = [super init];
    if (self) {
        self.conversationId = _mo.conversationId;
        self.objectId = _mo.messageId;
        self.avatarUrl = _mo.avatarUrlString;
        self.incoming = _mo.incoming;
        self.content = _mo.content;
        self.status = _mo.status;
        self.localIdentifier = _mo.localIdentifier;
        self.uid = _mo.uid;
        self.createdDate = _mo.createdDate;
        self.modifiedDate = _mo.modifiedDate;
        self.orderId = _mo.orderId;
    }
    return self;
}


- (void) saveToCoreData {
    
}

+ (NSString *)localIdentifierForMid:(NSString *)mid {
    return @"";
}
@end
