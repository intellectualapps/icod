//
//  DhtNotificationWindow.m
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtNotificationWindow.h"
#import "DhtNotificationViewController.h"
#import "DhtNotificationView.h"
#import "DhtWatcher.h"
#import "DhtHandle.h"
#import "DhtActionControl.h"

@implementation DhtNotificationWindow

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        DhtNotificationViewController *controller = [[DhtNotificationViewController alloc] init];
        controller.weakWindow = self;
        self.rootViewController = controller;
    }
    return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    CGPoint localPoint = [((DhtNotificationViewController *)self.rootViewController).notificationView convertPoint:point fromView:self];
    UIView *result = [((DhtNotificationViewController *)self.rootViewController).notificationView hitTest:localPoint withEvent:event];
    if (result == ((DhtNotificationViewController *)self.rootViewController).notificationView || result == self.rootViewController.view) {
        return nil;
    }
    return result;
}

- (void)setContentView:(UIView *)view {
    [((DhtNotificationViewController *)self.rootViewController).notificationView setContentView:view];
}

- (UIView *)contentView {
    return [((DhtNotificationViewController *)self.rootViewController).notificationView contentView];
}

- (void)animateIn {
    [((DhtNotificationViewController *)self.rootViewController).notificationView animateIn];
}

- (void)animateOut {
    [((DhtNotificationViewController *)self.rootViewController).notificationView animateOut];
}

- (BOOL)_canBecomeKeyWindow {
    return false;
}

- (bool)isDismissed {
    return [((DhtNotificationViewController *)self.rootViewController).notificationView isDismissed];
}

- (void)performTapAction:(id<NotificationProtocol>)object {
    [self openViewControllerWithObject:object];
    if ([self isDismissed])
        return;
    [self animateOut];
}

- (void)openViewControllerWithObject:(id<NotificationProtocol>)object {
    
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
}

@end
