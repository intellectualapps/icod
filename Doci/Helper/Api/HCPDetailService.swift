//
//  HCPDetailService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class HCPDetailService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var doctor: Doctor?
        if let root = response as? [String: Any] {
            if let datas = root["data"] as? [[String: Any]] {
                if let data = datas.first  {
                    doctor = Doctor(dict: data)
                }
            }
        }
        super.onFinish(doctor, error: error, completion: completion)
    }

    class func getHCPDetail(id: Int, completion: @escaping NetworkServiceCompletion) {
        let path = Path.hcpDetail.path
        let params = RequestParams()
        params.setValue(id, forKey: "id")
        let service = HCPDetailService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
