//
//  ResetPasswordService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class ChangePasswordService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        super.onFinish(response, error: error, completion: completion)
    }

    class func resetPassword(email: String, old_password: String, new_password: String, completion: @escaping NetworkServiceCompletion) {
        let path = Path.changePassword.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(new_password, forKey: "new_password")
        params.setValue(old_password, forKey: "old_password")
        let service = ChangePasswordService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
