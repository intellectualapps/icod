//
//  LogoutService.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/10/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class LogoutService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        print(response)
        super.onFinish(response, error: error, completion: completion)
    }
}
