//
//  ResetPasswordService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class HCPAllService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results = [Doctor]()
        if let data = response as? Data {
            let str = try? String.init(data: data, encoding: .utf8)
            print(str)
        }
        if let root = response as? [String: Any] {
            if let datas = root["data"] as? [[String: Any]] {
                datas.forEach { (d) in
                    let info = Doctor(dict: d)
                    results.append(info)
                }
            }
        }
        super.onFinish(results, error: error, completion: completion)
    }

    class func allHCP( completion: @escaping NetworkServiceCompletion) {
        let path = Path.hcpAll.path
        let params = RequestParams()
        let service = HCPAllService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
