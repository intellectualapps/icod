//
//  LoginService.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/10/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
class LoginService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var user: User?
        if let root = response as? [String: Any] {
            user = User.userFromDict(dict: root)
        }
        super.onFinish(user, error: error, completion: completion)
    }

    class func login(email: String, password: String, completion: @escaping NetworkServiceCompletion) {
        let path = Path.login.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(password, forKey: "password")
        let service = LoginService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
