//
//  SignupService.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/10/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import Alamofire

class SignupService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var user: User?
        if let root = response as? [String: Any] {
            print(root)
            let token = root.stringOrEmptyForKey(key: "access_token")
            let refreshToken = root.stringOrEmptyForKey(key: "refresh_token")
            Keychains.accesstoken.set(value: token)
            user = User.userFromDict(dict: root)
            user?.save()
        }
        super.onFinish(user, error: error, completion: completion)
    }

    class func signup(name: String,
                      email: String,
                      password: String,
                      role: Int, completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(name, forKey: "name")
        params.setValue(email, forKey: "email")
        params.setValue(password, forKey: "password")
        params.setValue(password, forKey: "role_id")
        let path = Path.register.path
        let service = SignupService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .put), retryCount: 1)
        service.doExecute(completion)
    }
}
