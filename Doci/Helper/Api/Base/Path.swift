//
//  Path.swift
//  Mei_messaging
//
//  Created by MTQ on 7/30/18.
//  Copyright © 2018 TSpace. All rights reserved.
//

import Foundation

private struct Constants {

    //http://sakura.vj-soft.com/api/
    //"http://127.0.0.1:1791/0.1/"
    //remote http://128.199.172.12/
    static let baseUrl = "http://128.199.172.12/0.1/"
    //http://test.docihealthcare.com/v1/auth/login"
    //https://api.docihealthcare.com/v1/
    static let apiBaseUrl = "https://test.docihealthcare.com/v1/"
}

enum FakePath: String {
    case destinations = "https://www.mocky.io/v2/5cc6c2503200008219b94dc8"
}

enum Path {
    case getSet
    case setDetail
    case orderSet
    case register
    case updateContent
    case listUpdateSetDetail
    case totalData
    case buildRoute
    case login
    case resetpassword
    case changePassword
    case logout
    case listDestination
    case listSuggestion
    case generateOTP
    case verifyOTP
    case saveFCMToken
    case hcpAll
    case hcpNearBy
    case searchHCP
    case hcpDetail
    case listSpecialties
    case listAilments
    private var relativePath: String {
        switch self {
        case .listAilments:
            return "ailments"
        case .listSpecialties:
            return "specialties"
        case .hcpDetail:
            return "hcps/details"
        case .searchHCP:
            return "hcps/search"
        case .hcpNearBy:
            return "hcps/near"
        case .hcpAll:
            return "hcp/all"
        case .saveFCMToken:
            return "save_fcm_token"
        case .verifyOTP:
            return "user/verify_otp"
        case .generateOTP:
            return "user/generate_otp"
        case .changePassword:
            return "user/change_password"
        case .resetpassword:
            return "user/reset_password"
        case .listSuggestion:
            return "suggest-destinations"
        case .listDestination:
            return "destinations"
        case .logout:
            return "auth/logout"
        case .login:
            return "auth/login"
        case .buildRoute:
            return "tripplan/submit.json"
            //return "buildroute/routes.json"
        case .totalData:
            return "get_total_data"
        case .listUpdateSetDetail:
            return "set_update"
        case  .updateContent:
            return "update_content"
        case .register:
            return "auth/register"
        case .getSet:
            return "get_set"
        case .setDetail:
            return "set_detail"
        case .orderSet:
            return "order_set"
        }
    }
    
    var path: String {
        get {
            let url = Constants.apiBaseUrl
            let rpath = relativePath.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
            return (NSURL(string: url)?.appendingPathComponent(rpath)?.absoluteString) ?? ""
        }

    }

    var buildPlanPath: String {
        get {
            let url = Constants.baseUrl
            let rpath = relativePath.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
            return (NSURL(string: url)?.appendingPathComponent(rpath)?.absoluteString) ?? ""
        }
    }
}
