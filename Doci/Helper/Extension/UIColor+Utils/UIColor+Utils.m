//
//  UIColor+Utils.m
//  zipGo
//
//  Created by msahaydak on 3/31/16.
//  Copyright © 2016 zipGo. All rights reserved.
//

#import "UIColor+Utils.h"


@implementation UIColor (Utils)
+ (UIColor *)colorWithHex:(UInt32)col alpha:(CGFloat)alpha {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha: alpha];
}
// takes @"#123456"
+ (UIColor *)colorWithHexString:(NSString *)str {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:str];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

// takes 0x123456
+ (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}

@end
