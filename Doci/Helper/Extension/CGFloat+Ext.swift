//
//  CGFloat+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/17/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
