//
//  AutoSizeConstraint.swift
//  Whaley
//
//  Created by Nguyen Van Dung on 2/3/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class AutoSizeConstraint: NSLayoutConstraint {
    @IBInspectable internal var iphoneLessThanOrEqual5Size: CGFloat = 0 {
        didSet {
            self.updateSize()
        }
    }

    @IBInspectable internal var iphone67Size: CGFloat = 0 {
        didSet {
            self.updateSize()
        }
    }

    @IBInspectable internal var iphonePlusSize: CGFloat = 0 {
        didSet {
            self.updateSize()
        }
    }

    @IBInspectable internal var iphoneXSize: CGFloat = 0 {
        didSet {
            self.updateSize()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        updateSize()
    }

    func updateSize() {
        var size: CGFloat = iphoneLessThanOrEqual5Size
        if DeviceTarget.IS_IPHONE_6 {
            size = iphone67Size
        } else if DeviceTarget.IS_IPHONE_6_Plus {
            size = iphonePlusSize
        } else if DeviceTarget.IS_IPHONE_X {
            size = iphoneXSize
        }
        self.constant = size
    }
}


class AutoSizeConstraintIpad: AutoSizeConstraint {
    @IBInspectable internal var iphoneLessThanOrEqual4Size: CGFloat = 0 {
        didSet {
            self.updateSize()
        }
    }

    override func updateSize() {
        super.updateSize()
        var size: CGFloat = self.constant
        if DeviceTarget.IS_IPHONE_4 {
            size = iphoneLessThanOrEqual4Size
        }
        self.constant = size
    }
}
