//
//  UIStoryboard+Ext.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

extension UIStoryboard {
    var AuthenEmailVC: UIViewController? {
        return self.instantiateViewController(withIdentifier: "AuthenEmailVC")
    }
    
    var AllDoctorVC: UIViewController? {
        return self.instantiateViewController(withIdentifier: "AllDoctorVC")
    }

    var DoctorNearYouVC: UIViewController? {
        return self.instantiateViewController(withIdentifier: "DoctorNearYouVC")
    }

    var SearchDoctorsVC: UIViewController? {
        return self.instantiateViewController(withIdentifier: "SearchDoctorsVC")
    }

    var FilterGenderVC: UIViewController? {
        return self.instantiateViewController(withIdentifier: "FilterGenderVC")
    }

    var DoctorDetailVC: UIViewController? {
        return self.instantiateViewController(withIdentifier: "DoctorDetailVC")
    }

    class func viewController(storyboard: String, controller: String) -> UIViewController? {

        return UIStoryboard.doctor().instantiateViewController(withIdentifier: controller)
    }

    class func doctor() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: "doctor", bundle: nil)
        return storyboard
    }

    class func main() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard
    }

    class func authen() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: "Authen", bundle: nil)
        return storyboard
    }
}
