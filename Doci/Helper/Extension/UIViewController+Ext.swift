//
//  UIViewController+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/17/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import GoogleMaps

extension UIViewController: UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    func showTip(data: Any?, target: UIView) {
        if GUIHelper.shared.tipView != nil {
            return
        }
        print(NSStringFromClass(self.classForCoder) + "." + #function)
        guard let tipview = TipView.newInstance() else {
            return
        }
        GUIHelper.shared.tipView = tipview
        let frame = self.view.convert(target.frame, from: target)
        let popTip = PopTip()
        popTip.padding = 20
        tipview.popTip = popTip
        tipview.frame = CGRect(x: 0, y: 0, width: 250, height: 150)
        popTip.show(customView: tipview,
                    direction: .up,
                    in: self.view,
                    from: frame)
        popTip.shouldDismissOnTap = false
        popTip.tapOutsideHandler = {[weak self] (pop) in
            popTip.hide()
            GUIHelper.shared.tipView = nil
        }
        popTip.tapHandler = { [weak self] (pop) in

        }

    }

    func showLeftMenu(isShow: Bool = true) {
        if let menu = GUIHelper.shared.leftMenuController {
            if isShow {
                self.present(menu, animated: true, completion: nil)
            } else {
                menu.dismiss(animated: true, completion: nil)
            }
        }
    }

    func showNavigation(isShow: Bool) {
        self.navigationController?.setNavigationBarHidden(!isShow, animated: false)
        self.tabBarController?.navigationController?.setNavigationBarHidden(!isShow, animated: false)
    }


    func presentTransperant(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = .overCurrentContext
        viewControllerToPresent.modalTransitionStyle = .crossDissolve

        viewControllerToPresent.view.backgroundColor = UIColor(hex: 0x000000, alpha: 0.4)
        present(viewControllerToPresent, animated: animated, completion: completion)
    }

    func disableSwipeBack() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    func enableSwipeBack() {
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    func changeNavigationColor(tintcolor: UIColor, textColor: UIColor) {
        navigationController?.navigationBar.barTintColor = tintcolor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: textColor]

    }

    func showGenderFilter(lastOption: Int, completion: @escaping (_ newOption: Int) -> ()) {
        if let controller = UIStoryboard.main().FilterGenderVC as? FilterGenderVC {
            controller.lastFilterOption = lastOption
            controller.completion = completion
            let popup = PopupDialog(viewController: controller, buttonAlignment: NSLayoutConstraint.Axis.vertical, transitionStyle: .zoomIn, preferredWidth: 280, tapGestureDismissal: true, panGestureDismissal: true, hideStatusBar: true) {

            }
            self.present(popup, animated: true, completion: nil)
        }
    }
}
