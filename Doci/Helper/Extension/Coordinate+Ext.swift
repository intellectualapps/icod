//
//  Coordinate+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 5/19/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import CoreLocation
extension CLLocationCoordinate2D {
    func toIdentifier() -> String {
        return String(self.latitude) + "," + String(self.longitude)
    }
}
