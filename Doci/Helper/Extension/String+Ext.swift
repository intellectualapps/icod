//
//  String+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

extension String {
    func removeFileExtension() -> String {
        return (self as NSString).deletingPathExtension
    }

    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }

    static func documentFolder() -> String {
        if let documentsPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            return documentsPathString
        }
        return ""
    }

    static func cachedFolder() -> String {
        if let documentsPathString = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first {
            print(documentsPathString)
            return documentsPathString
        }
        return ""
    }

    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }

        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }

    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }

    static func udid() -> String {
        let uuid = CFUUIDCreateString(nil, CFUUIDCreate(nil))
        return (uuid as? String) ?? ""
    }

    func replace(with: String, inRange: NSRange) -> String {
        return (self as NSString).replacingCharacters(in: inRange, with: with)
    }

    /**
     * Calulate size for text.
     * boundSize: This is max bound text can display in
     */
    func sizeWithBoundSize(boundSize: CGSize,
                           option: NSStringDrawingOptions,
                           lineBreakMode: NSLineBreakMode = NSLineBreakMode.byTruncatingTail,
                           font: UIFont) -> CGSize {
        if self.isEmpty {
            return CGSize.zero
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = lineBreakMode
        return (self as NSString).boundingRect(with: boundSize,
                                               options: option,
                                               attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle],
                                               context: nil).size
    }

    /**
     * check the input email is valid or not. Self is email string
     */
    public func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    func format12h() -> String {
        let interval = self.toInterval()
        let date = Date().dateAtStartOf(.day).addingTimeInterval(TimeInterval(interval))
        let Date12 = date.toFormat("h:mm a", locale: Locales.english)
        return Date12
    }

    func toInterval() -> Int {
        var  array = self.components(separatedBy: ":")
        var hours = 0
        var mins = 0
        var second = 0

        func getmins(txts: [String]) -> Int {
            if txts.count == 2 {
                if let last = txts.last {
                    return Int(last) ?? 0
                }
            }
            return 0
        }

        func getHours(txts: [String]) -> Int {
            if let first = txts.first {
                return Int(first) ?? 0
            }
            return 0
        }

        if array.count == 3 {
            array.removeLast()
        }
        mins = getmins(txts: array)
        hours = getHours(txts: array)
        return hours * 3600 + mins * 60 
    }

    func validImageUrl() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed) ?? self
    }
}
