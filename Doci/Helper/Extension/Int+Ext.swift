//
//  Int+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 5/11/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

extension Int {
    func secondsToHoursMinutesSeconds () -> (Int, Int, Int) {
        //min 1 min to travel
        let temp = self > 60 ? self : 60

        let h = temp / 3600
        let m = (temp % 3600) / 60
        let s = temp - h*3600 - m*60
        return (h, m, s > 60 ? s : 60)
    }

    func toTime() -> String {
        let info = secondsToHoursMinutesSeconds()
        if info.0 > 12 {
            return String.init(format: "%02i:%02i", info.0 - 12, info.1) + " PM"
        } else {
            return String.init(format: "%02i:%02i", info.0, info.1) + " AM"
        }
    }

    func toTimeFormat() -> String {
        let info = secondsToHoursMinutesSeconds()
        var format = ""
        if info.0 > 0 {
            let interval = info.0 * 3600 + info.1 * 60
            format = String.init(format: "%0.1fhrs", Double(interval) / 3600.0)
        } else if info.1 > 0 {
            format = String.init(format: "%02imin", info.1)
        } else {
            if info.2 > 0 {
                format = String.init(format: "%02i second", info.1)
            } else {
                format = ""
            }
        }

        return format
    }

    func toKm() -> String {
        print(self)
        if self >= 1000 {
            return String.init(format: "%0.1f", Double(self) / 1000) + " km"
        } else {
            return String.init(format: "%d", self) + " m"
        }
    }
}

