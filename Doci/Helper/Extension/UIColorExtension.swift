//
//  UIColorExtension.swift
//  MobileWarehouse
//
//  Created by Nguyen Van Dung on 5/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

extension UIColor {
    static let primaryColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)

    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
    
    class func mainColor() -> UIColor {
        return UIColor(hex: 0x8c1b22)
    }
    
    class func sportsSouthBlue() -> UIColor {
        return UIColor(hex: 0x00345b)
    }

    class func backgroundSportsSouthBlue() -> UIColor {
        return UIColor(hex: 0x00345b)
    }

    class func defaultBackgroundColor() -> UIColor {
       // return UIColor(hex: 0x0d2643)
        return UIColor(hex: 0xf4f4f4)
    }

    //0x00456e 0xf4f4f4
    class func defaultTextColor() -> UIColor {
        return UIColor.black
    }

    class func defaultLabelColor() -> UIColor {
        return UIColor(hex: 0x2e507d)
    }
}
