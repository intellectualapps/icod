//
//  GUIHelper.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/23/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class GUIHelper: NSObject {

    static let shared = GUIHelper()
    var tipView: TipView?
    var leftMenuController: UISideMenuNavigationController?
    var navigationHeight: Variable<CGFloat> = Variable(64)

    var lastTabbarIndex = -1
    var mainTabbarIndex = Variable(0)

    override init() {
        super.init()
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        if let navi = storyboard.instantiateInitialViewController() as? UISideMenuNavigationController {
            self.leftMenuController = navi
        }
    }

    func setMainTabbarIndex(index: Int) {
        if mainTabbarIndex.value != index {
            mainTabbarIndex.value = index
        }
    }
    
    func setNavigationHeight(height: CGFloat) {
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let newheight = statusBarHeight + 44
        if newheight > navigationHeight.value {
            navigationHeight.value = newheight
        }
    }
}
