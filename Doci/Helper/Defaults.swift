//
//  Defaults.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/23/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
enum Defaults: String {
    case alertSettingCount = "alertSettingCount"
    case loggedEmail = "loggedEmail"
    case currentLanguage = "currentLanguage"
    case professional = "professional"

    func set(value: Any?) {
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }

    func get() -> Any? {
        return UserDefaults.standard.object(forKey: self.rawValue)
    }

    func getDouble() -> Double {
        let value = UserDefaults.standard.double(forKey: self.rawValue)
        return value
    }

    func getBool() -> Bool {
        return UserDefaults.standard.bool(forKey: self.rawValue)
    }

    func getString() -> String? {
        return UserDefaults.standard.string(forKey: self.rawValue)
    }

    func getStringOrEmpty() -> String {
        return self.getString() ?? ""
    }

    func getInt() -> Int {
        return UserDefaults.standard.integer(forKey: self.rawValue)
    }

    func getIntWithDefault() -> Int {
        return getInt()
    }

    static func clear() {
    }

    static func language() -> String {
        if let value = Defaults.currentLanguage.getString() {
            return value
        }
        let lg = Language.vietnam.rawValue
        Defaults.currentLanguage.set(value: lg)
        return lg
    }
}
