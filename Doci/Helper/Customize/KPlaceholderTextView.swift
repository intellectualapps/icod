//
//  KPlaceholderTextView.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/23/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
class KPlaceholderTextView: UITextView, UITextViewDelegate {

    struct Static {
        static let paddingTop : CGFloat = 7.0
        static let paddingX : CGFloat = 5.0
    }

    fileprivate var placeholderLbl: UILabel!
    var placeholder: String = "" {
        didSet {
            if placeholder.count == 0 {
                self.placeholderLbl.isHidden = true
            } else {
                self.placeholderLbl.isHidden = false
                self.placeholderLbl.text = placeholder
            }
        }
    }

    var placeholderColor: UIColor? {
        didSet {
            self.placeholderLbl.textColor = placeholderColor ?? UIColor.lightGray
        }
    }

    var placeholderFont: UIFont? {
        didSet {
            self.placeholderLbl.font = placeholderFont ??  UIFont.systemFont(ofSize: 13)
        }
    }

    //Opacity: default 1
    var placeholderOpacity: Float = 1 {
        didSet {

        }
    }

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    deinit {
        placeholderLbl.removeFromSuperview()
        NotificationCenter.default.removeObserver(self)
    }

    func setTextString(_ text: String) {
        self.text = text
        self.placeholderLbl?.isHidden = true
    }

    fileprivate func commonInit() {
        self.registerNotification()
        let placeholderWidth = self.frame.size.width - 2 * Static.paddingX
        placeholderLbl = UILabel(frame: CGRect(x: Static.paddingX, y: Static.paddingTop, width: placeholderWidth, height: 30))
        placeholderLbl.numberOfLines = 0
        placeholderLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.addSubview(placeholderLbl)
        self.placeholderColor = UIColor.lightGray
        self.placeholderFont = UIFont.systemFont(ofSize: 13)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let placeholderWidth = self.frame.size.width - 2 * Static.paddingX
        var frame = self.placeholderLbl.frame
        frame.size.width = placeholderWidth
        frame.size.height = self.frame.size.height
        placeholderLbl.frame = frame
        placeholderLbl.sizeToFit()
    }

    fileprivate func registerNotification() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(self.textDidChange(_:)),
                           name: UITextView.textDidChangeNotification,
                           object: self)
        center.addObserver(self, selector: #selector(self.textViewDidBeginEdittingNotify(_:)),
                           name: UITextView.textDidBeginEditingNotification,
                           object: self)
        center.addObserver(self, selector: #selector(self.textViewDidEndEdittingNotify(_:)),
                           name: UITextView.textDidEndEditingNotification,
                           object: self)
    }

    ///Handle notification callback
    @objc func textDidChange(_ notification: Notification) {
        if self.placeholder.count == 0 {
            placeholderLbl.isHidden = true
        }
        let lenght = self.text?.count ?? 0
        return placeholderLbl.isHidden = lenght > 0
    }

    @objc func textViewDidBeginEdittingNotify(_ notificaiton: Notification) {

    }

    @objc func textViewDidEndEdittingNotify(_ notification: Notification) {

    }
}
