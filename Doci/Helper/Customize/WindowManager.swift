//
//  WindowManager.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ARSLineProgress
import NVActivityIndicatorView

enum ShowDirection: Int {
    case none = 0
    case bottomToTop
    case leftToRight
    case rightToLeft
    case topBottom
}

typealias WindowShowCompletion = (_ isClosed: Bool) -> ()
let playbarHeight: CGFloat = 70

class KWindow: UIWindow {
    var direction: ShowDirection = ShowDirection.none
    var completion: WindowShowCompletion?
}


class ProgressViewController: UIViewController, NVActivityIndicatorViewable {
    override var prefersStatusBarHidden: Bool {
        return false
    }

    func startAnimate() {
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: NVActivityIndicatorType.ballScaleMultiple)
    }

    func stopAnimation() {
        stopAnimating()
    }
}

class WindowManager: NSObject {
    static let shared = WindowManager()
    var canClose = false
    var showingPlaybar = false
    var isFirstTimeShowplaybar = true
    var tabbarHeight: CGFloat = 0
    lazy var alertWindow: KWindow = {
        let window = KWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UIViewController()
        window.windowLevel = UIWindow.Level.alert
        return window
    }()
    
    lazy var progressWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let controller = self.progressController
        window.rootViewController = controller
        window.windowLevel = UIWindow.Level.alert + 0.1
        window.isOpaque = true
        return window
    }()

    lazy var progressController: ProgressViewController = {
        return ProgressViewController()
    }()

    override init() {
        super.init()
    }

    /**
     * Get window level of visible window displaying in app
     */
    class func currentWindowLevel() -> CGFloat {
        if let window = UIApplication.shared.keyWindow {
            return window.windowLevel.rawValue
        }
        return UIWindow.Level.statusBar.rawValue
    }


    /**
     * Call this to show waiting progress view overlay all screen in app.
     */
    func showProgressView(rootView: UIView? = nil) {
        if self.progressWindow.isHidden == false {
            return
        }
        DispatchQueue.main.async {
            self.progressController.startAnimate()
            let window = self.progressWindow
            window.makeKeyAndVisible()
            window.windowLevel = UIWindow.Level.alert + 1
            window.becomeKey()
            window.isHidden = false
            window.alpha = 1.0
        }
    }

    /**
     * Call anywhere when you want close waiting proress view
     */
    func hideProgressView(animate: Bool, isSuccess: Bool = true, completion: @escaping () -> ()) {
        let time = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            let window = self.progressWindow
            window.isHidden = true
            self.progressController.view?.removeFromSuperview()
            self.progressController.stopAnimation()
            completion()
        })
    }

    func hideProgressView(isSuccess: Bool = true) {
        self.hideProgressView(animate: true, isSuccess: isSuccess) {
            
        }
    }

    func animationShowOrCloseWindow(_ window: UIWindow, isClosing: Bool, animated: Bool, direction: ShowDirection, completion: (WindowShowCompletion)?) {
        let size = UIScreen.main.bounds.size
        (window as? KWindow)?.direction = direction
        (window as? KWindow)?.completion = completion
        var windowFrame = CGRect.zero
        var newPosition: CGPoint = CGPoint.zero
        windowFrame.size = size
        switch direction {
        case .bottomToTop:
            windowFrame.origin.y = windowFrame.height
            windowFrame.origin.x = 0
            newPosition.y = (isClosing == true) ? -windowFrame.height : 0
        case .leftToRight:
            windowFrame.origin.y = 0
            windowFrame.origin.x = -windowFrame.width
            newPosition.x = (isClosing == true) ? windowFrame.width : 0
        case .rightToLeft:
            windowFrame.origin.y = 0
            windowFrame.origin.x = windowFrame.width
            newPosition.x = (isClosing == true) ? -windowFrame.width : 0
        case .topBottom:
            windowFrame.origin.y = -windowFrame.height
            windowFrame.origin.x = 0
            newPosition.y = (isClosing == true) ? windowFrame.height : 0
        default: break
        }
        if (isClosing == false) {
            window.frame = windowFrame
        }
        let newFrame = CGRect(origin: newPosition, size: size)
        if (animated) {
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                window.frame = newFrame
            }, completion: { (complete) -> Void in
                completion?(isClosing)
            })
        } else {
            window.frame = newFrame
            completion?(isClosing)
        }
    }

    func closeWindow(_ window: UIWindow) {
        window.rootViewController?.willMove(toParent: nil)
        window.rootViewController?.removeFromParent()
        window.rootViewController = nil
        window.isHidden = true
        window.rootViewController = UIViewController()
    }
}
