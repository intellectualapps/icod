//
//  VRoundView.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/17/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
@IBDesignable
class VRoundView: UIView {
    /**
     *  Setup corner radius for layer
     */
    @IBInspectable internal var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }

    @IBInspectable internal var borderWidth: CGFloat = 0.5 {
        didSet {
            self.layer.borderWidth = borderWidth
            self.layer.masksToBounds = true
        }
    }

    @IBInspectable internal var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = (borderColor ).cgColor
            self.layer.masksToBounds = true
        }
    }
}
