//
//  ContentManager.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class ContentManager: NSObject {
    static let shared = ContentManager()
    var doctors = [Doctor]()
    var listAilments = BehaviorRelay(value: [Ailments]())
    var listSpecifies = BehaviorRelay(value: [Specialties]())

    override init() {
        super.init()
        let names = self.names()
        names.forEach { (name) in
            let doctor = Doctor()
            doctor.name = name
            let rd = arc4random() % 1000 + 500
            doctor.distance = Int(rd)
            let positions = possitions()
            var count = positions.count
            let rd2 = arc4random() % UInt32(count)
            doctor.position = possitions()[Int(rd2)]
            let lgenders = genders()
            count = lgenders.count
            let rd3 = arc4random() % UInt32(count)
            doctor.gender = Gender(rawValue: lgenders[Int(rd3)]) ?? Gender.Male
            self.doctors.append(doctor)
        }
    }

    private func genders() -> [String] {
        return [Gender.Male.rawValue, Gender.Female.rawValue]
    }

    private func possitions() -> [String] {
        return [
            "General Medicine",
            "Emergency Medicine",
            "Internal Medicine",
            "Pediatrics",
            "Psychiatry",
            "Family Medicine",
            "Orthopedics",
            "Haenmatology",
            "ENT"
        ]
    }

    private func names() -> [String] {
        return [
            "Dr Salawu Taofipat",
            "Emmanuel Essien",
            "Desire Ojie",
            "Olusina Ajidahun",
            "Mofeoluwa LAGUNJU",
            "Adeola Oluwatobi",
            "Emmanuel Ayodele",
            "Akinpelu Matthew",
            "Ayantoyinbo Babatunde Tosin",
            "Emmanuel Batatope",
            "Wale shittu",
            "michelle nwokorie",
            "Modestus lgwe",
            "Kehinde Muraina",
            "Ayodele Renner",
            "anthony ochin"
        ]
    }

    func getConfig(completion: @escaping NetworkServiceCompletion) {
        self.listAilments.accept([])
        self.listSpecifies.accept([])
        ListAilmentsService.getListAliments {[weak self] (response, error) in
            if let list = response as? [Ailments] {
                self?.listAilments.accept(list)
                ListSpecifiesService.getListSpecifies(completion: { (response, error) in
                    if let specifiesObjs = response as? [Specialties] {
                        self?.listSpecifies.accept(specifiesObjs)
                    }
                    completion(response, error)
                })
            } else {
                completion(response, error)
            }
        }
    }
}
