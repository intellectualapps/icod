//
//  FindDocterVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/19/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class FindDocterVC: PresentableSideMenuVC {

    @IBOutlet weak var selectSpecifyBtn: UIButton!
    @IBOutlet weak var searchTypeTF: UITextField!
    @IBOutlet weak var joinNetworkBtn: UIButton!
    
    var searchType = SearchTypeObject()

    override func viewDidLoad() {
        super.viewDidLoad()
        joinNetworkBtn?.underline(textColor: UIColor.blue)
        title = "title.find.doctors".localized()
        self.addLeftButtonWithImage(img: UIImage.init(named: "menu_ico"), action: #selector(showMenuAction), target: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if ContentManager.shared.listSpecifies.value.count == 0 {
            WindowManager.shared.showProgressView()
            ContentManager.shared.getConfig { (response, error) in
                WindowManager.shared.hideProgressView()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                }
            }
        }
    }

    @objc func showMenuAction() {
        showLeftMenu()
    }

    @IBAction func doctorNearBtnAction(_ sender: Any) {
        if let controller = UIStoryboard.doctor().DoctorNearYouVC {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    @IBAction func allDoctorsAction(_ sender: Any) {
        if let controller = UIStoryboard.doctor().AllDoctorVC {
        self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    @IBAction func searchBtnAction(_ sender: Any) {
        if let controller = UIStoryboard.doctor().SearchDoctorsVC as? SearchDoctorsVC {
            self.navigationController?.pushViewController(controller, animated: true)
            let model = SearchDoctorViewModel()
            controller.viewModel = model
            (controller.viewModel as? SearchDoctorViewModel)?.searchType = searchType
        }
    }

    @IBAction func selectSpecifyAction(_ sender: Any) {
        var datas = [SearchTypeObject]()
        datas.append(contentsOf: ContentManager.shared.listSpecifies.value)
        datas.append(contentsOf: ContentManager.shared.listAilments.value)
        let dropDown = DropDown()
        dropDown.anchorView = self.selectSpecifyBtn
        dropDown.dataSource = datas.map({$0.name})
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self?.searchTypeTF?.text = item
            self?.searchType = datas[index]
            dropDown.hide()
        }
        dropDown.direction = .top
        dropDown.width = self.selectSpecifyBtn.frame.width
        dropDown.show()
    }

    @IBAction func joinNetworkBtnAction(_ sender: Any) {
        AppDelegate.shared.showAuthorize()
    }
}
