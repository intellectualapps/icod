//
//  DoctorNearYouVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DoctorNearYouVC: DoctorListVC {
    @IBOutlet weak var noResultView: UIView!
    @IBOutlet weak var retryBtn: UIButton!

    @IBOutlet weak var emptyResultlbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Doctors Nearby".localized().capitalized
        noResultView?.isHidden = true
    }

    override func loadData() {
        guard let user = AppDelegate.shared.loggedUser.value else {
            return
        }

        func executeApi(location: VLocation) {

            HCPNearByService.hcpNearBy(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, offset: 20) {[weak self] (response, error) in
                WindowManager.shared.hideProgressView()
                guard let `self` = self else {
                    return
                }
                if let err = error {
                    DispatchQueue.main.async {
                        self.noResultView?.isHidden = false
                        self.emptyResultlbl?.text = err.message
                    }
                } else if let doctors = response as? [Doctor] {
                    self.noResultView?.isHidden = true
                    self.viewModel.setListDoctors(doctors: doctors)
                    self.reload()
                }
            }
        }
        WindowManager.shared.showProgressView()
        CoreLocation.shared.getCurrentLocation { (location) in
            if let location = location {
                AppDelegate.shared.loggedUser.value?.location = location
                executeApi(location: location)
            } else {
                WindowManager.shared.hideProgressView()
                Alert.showAlertWithErrorMessage("error.nolocation".localized())
            }
        }
    }

    @IBAction func retryAction(_ sender: Any) {
        loadData()
    }
}
