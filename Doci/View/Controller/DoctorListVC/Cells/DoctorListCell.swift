//
//  DoctorListCell.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DoctorListCell: UITableViewCell {

    @IBOutlet weak var avatarView: VImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var kmLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        avatarView?.image = UIImage.init(named: "doci_default_logo")
        nameLbl?.text = ""
        subTitleLbl?.text = nil
        starView?.rating = 0
        genderLbl?.text = nil
        kmLbl?.text = nil
        checkBtn?.isSelected = false
    }

    func config(doctor: Doctor) {
        checkBtn?.isSelected = doctor.verified
        nameLbl?.text = doctor.name
        subTitleLbl?.text = doctor.position
        genderLbl?.text = doctor.gender.rawValue
        kmLbl?.text = doctor.distance.toKm()
        if doctor.avartUrl.count > 0 {
            avatarView?.sd_setImage(with: URL.init(string: doctor.avartUrl), completed: nil)
        }
    }
}
