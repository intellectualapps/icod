//
//  AllDoctorVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class AllDoctorVC: DoctorListVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "All doctors".localized().capitalized
    }

    override func loadData() {
        WindowManager.shared.showProgressView()
        HCPAllService.allHCP {[weak self] (response, error) in
            guard let `self` = self else {
                return
            }
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let doctors = response as? [Doctor] {
                if doctors.count == 0 {
                    Alert.showAlertWithErrorMessage("message.no.result".localized())
                } else {
                    self.viewModel.setListDoctors(doctors: doctors)
                    self.reload()
                }
            }
        }
    }
}
