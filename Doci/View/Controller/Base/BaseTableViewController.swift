//
//  BaseTableViewController.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class BaseTableViewController: UITableViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.enableSwipeBack()
    }
}
