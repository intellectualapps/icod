//
//  BaseViewController.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit
import Foundation

class BaseViewController: UIViewController, GIDSignInUIDelegate {
    var allowsSwipeBack = true

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        GUIHelper.shared.setNavigationHeight(height: self.navigationController?.navigationBar.bounds.height ?? 0)
        print("VIEWCONTROLLER : \(NSStringFromClass(self.classForCoder))")
        changeNavigationColor(tintcolor: UIColor.init(hex: 0x9F2726),
                              textColor: UIColor.white)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        enableSwipeBack()
        GUIHelper.shared.setNavigationHeight(height: self.navigationController?.navigationBar.bounds.height ?? 0)
    }

    func addBackButton() {
        addLeftButtonWithImage(img: UIImage.init(named: "back"), action: #selector(backAction), target: self)
    }

    @objc func backAction() {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

    func loginFacebookAndGetInfo(completion: @escaping (_ name: String, _ email: String, _ id: String) -> ()) {
        //1. check current login status
        let loginManager = LoginManager()
        let permission = ["email", "public_profile"]
        loginManager.logIn(permissions: permission, from: self) {[weak self] (result, error) in
            if let token = result?.token?.tokenString {
                self?.requestFacebookInfo(token: token, completion: { (name, email, id) in
                    AppDelegate.shared.showMainScreen()
                })
            }
        }
    }

    func requestFacebookInfo(token: String, completion: @escaping (_ name: String, _ email: String, _ id: String) -> ()) {
        let req = GraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: token, version: nil, httpMethod: HTTPMethod(rawValue: "GET"))
        req.start(completionHandler: { (connection, result, error) in

            if let dict = result as? [String: Any] {
                print(dict)
                let name = dict.stringOrEmptyForKey(key: "name")
                let email = dict.stringOrEmptyForKey(key: "email")
                let id = dict.stringOrEmptyForKey(key: "id")
                completion(name, email, id)
            } else {
                completion("", "", "")
            }
        })
    }

    func loginGoogle() {
        let signin = GIDSignIn.sharedInstance()
        signin?.shouldFetchBasicProfile = true
        signin?.clientID = googleClientId
        signin?.scopes = ["profile"]
        signin?.delegate = AppDelegate.shared
        signin?.uiDelegate = self
        signin?.signIn()
    }

    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        if let data = signIn?.currentUser?.profile  {
            print(data.email)
        }
    }
//
//    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
//
//    }
//
//    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        
//    }


    func addLeftButtonWithImage(img: UIImage?, action: Selector, target: Any) {
        guard let img = img else {
            return
        }
        let btn = UIBarButtonItem(image: img, style: .plain, target: target, action: action)
        btn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = btn
    }

    func addRightButtonWithImage(img: UIImage?, action: Selector, target: Any) {
        guard let img = img else {
            return
        }
        let btn = UIBarButtonItem(image: img, style: .plain, target: target, action: action)
        btn.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = btn
    }
}
