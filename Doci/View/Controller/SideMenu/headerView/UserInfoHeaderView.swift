//
//  UserInfoHeaderView.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/29/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class UserInfoHeaderView: UITableViewHeaderFooterView {
    var dispose = DisposeBag()

    @IBOutlet var avartarView: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var actionBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
        self.displayUserInfo(user: AppDelegate.shared.loggedUser.value)
        AppDelegate.shared.loggedUser.asObservable()
            .subscribe(onNext: {[weak self] (user) in
                DispatchQueue.main.async {
                    self?.displayUserInfo(user: user)
                }
            }).disposed(by: rx.disposeBag)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        dispose = DisposeBag()
        clear()
    }

    func clear() {
        nameLbl?.text = nil
    }

    func displayUserInfo(user: User?) {
        guard let user = user else {
            return
        }
        nameLbl?.text = user.name.capitalized
    }
}
