//
//  SideMenuCell.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/28/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        titleLbl?.text = nil
    }

    func config(data: Any?) {
        guard let info = data as? SideMenuCellInfo else {
            return
        }
        titleLbl?.text = info.title
    }
}
