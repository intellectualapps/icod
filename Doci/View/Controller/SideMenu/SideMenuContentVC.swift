//
//  SideMenuContentVC.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/28/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class SideMenuContentVC: UITableViewController {
    let viewModel = SideMenuViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        setupTable()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView?.reloadData()
        self.showNavigation(isShow: false)
    }

    func setupTable() {
        self.tableView?.tableFooterView = UIView()
        self.tableView?.tableHeaderView = UIView()
        let xib = UINib(nibName: "SideMenuCell", bundle: nil)
        self.tableView?.register(xib, forCellReuseIdentifier: "SideMenuCell")
        let hdXib = UINib(nibName: "UserInfoHeaderView", bundle: nil)
        self.tableView?.register(hdXib, forHeaderFooterViewReuseIdentifier: "UserInfoHeaderView")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    ///MARK: --functions
    func showProfileScreen() {

    }

    func showFindDoctorsScreen() {
        self.dismiss(animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let s = viewModel.sections[section]
        return s.height
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let s = viewModel.sections[section]
        return s.cells.count
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let hd = tableView.dequeueReusableHeaderFooterView(withIdentifier: "UserInfoHeaderView") as? UserInfoHeaderView else {
            return nil
        }
        hd.actionBtn?.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] in
                self?.showProfileScreen()
            }).disposed(by: hd.dispose)
        return hd
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let s = viewModel.sections[indexPath.section]
        let row = s.cells[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: row.reuseIdentifier, for: indexPath) as? SideMenuCell else {
            return UITableViewCell()
        }
        cell.config(data: row.content)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let s = viewModel.sections[indexPath.section]
        let row = s.cells[indexPath.row]
        if row.id == SideMenuCellID.logout.rawValue {
            AppDelegate.shared.logout()
        } else if row.id == SideMenuCellID.findDoctors.rawValue {
            self.showFindDoctorsScreen()
        }
    }
}
