//
//  AuthenEmailVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class AuthenEmailVC: BaseViewController {

    @IBOutlet weak var closeBtn: VButton!

    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signupView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var resetView: UIView!
    @IBOutlet weak var lineView: UIView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectViewAtIndex(index: 0)
    }

    func selectViewAtIndex(index: Int) {
        let selectedColor = UIColor.black
        let normalColor = UIColor.lightGray
        let btns = [signUpBtn, loginBtn, resetBtn]
        let views = [signupView, loginView, resetView]
        btns.forEach { (btn) in
            btn?.setTitleColor(normalColor, for: .normal)
        }

        views.forEach { (view) in
            view?.isHidden = true
        }
        if index < btns.count {
            let btn = btns[index]
            btn?.setTitleColor(selectedColor, for: .normal)
            var frame = lineView?.frame ?? .zero
            frame.origin.x = btn?.frame.origin.x ?? 0
            frame.size.width = btn?.frame.size.width ?? 0
            lineView?.frame = frame
            if index < views.count {
                let view = views[index]
                view?.isHidden = false
            }
        }
    }

    @IBAction func loginAction(_ sender: Any) {
        selectViewAtIndex(index: 1)
    }

    @IBAction func resetAction(_ sender: Any) {
        selectViewAtIndex(index: 2)
    }
    @IBAction func signupAction(_ sender: Any) {
        selectViewAtIndex(index: 0)
        //AppDelegate.shared.showMainScreen()
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func signupVC() -> RegisterContentVC? {
        return self.children.filter({$0.isKind(of: RegisterContentVC.self)}).first as? RegisterContentVC
    }
}
