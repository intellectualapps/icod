//
//  AuthenVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class AuthenVC: BaseViewController {
    @IBOutlet weak var loginEmailBtn: VButton!
    @IBOutlet weak var loginFacebooBtn: FacebookButton!
    @IBOutlet weak var loginGoogleBtn: VButton!
    @IBOutlet weak var profressionalBtn: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        loginFacebooBtn?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavigation(isShow: false)
        Defaults.professional.set(value: profressionalBtn?.isOn ?? false)
    }

    @IBAction func loginEmailAction(_ sender: Any) {
        if let ctrl = UIStoryboard.authen().AuthenEmailVC as? AuthenEmailVC {
            self.navigationController?.pushViewController(ctrl, animated: true)
        }
    }

    @IBAction func loginFacebookAction(_ sender: Any) {
        AppDelegate.shared.showMainScreen()
    }

    @IBAction func loginGoogleAction(_ sender: Any) {
        AppDelegate.shared.showMainScreen()
    }

    @IBAction func professionalSwitchAction(_ sender: Any) {
        Defaults.professional.set(value: profressionalBtn?.isOn ?? false)
    }


    ///MARK: Google signin GUI delegate
    override func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {

    }

}
