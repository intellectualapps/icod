//
//  RegisterContentVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class RegisterContentVC: UITableViewController {
    @IBOutlet weak var nameTF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTF: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordAgainTF: SkyFloatingLabelTextField!

    @IBAction func signupAction(_ sender: Any) {
        let name = (nameTF?.text ?? "").trim()
        let pass = (passwordTF?.text ?? "").trim()
        let email = (emailTF?.text ?? "").trim()
        let confirmPass = (passwordAgainTF?.text ?? "").trim()
        var errMsg = ""
        if name.count == 0 {
            errMsg = "error.empty.name".localized()
        } else if email.count == 0 {
            errMsg = "error.empty.email".localized()
        } else if email.isValidEmail() == false {
            errMsg = "error.wrongformat.email".localized()
        } else if pass.count == 0 {
            errMsg = "error.empty.password".localized()
        } else if pass.count < 6 {
            errMsg = "error.wrongformat.password".localized()
        } else if confirmPass.count == 0 {
            errMsg = "error.empty.confirmpassword".localized()
        } else if confirmPass.count < 6 {
            errMsg = "error.wrongformat.confirmpassword".localized()
        } else if pass !=  confirmPass {
            errMsg = "error.passwordnotmatch".localized()
        }
        if errMsg.count > 0 {
            Alert.showAlertWithErrorMessage(errMsg)
            return
        }

        WindowManager.shared.showProgressView()
        let role = Defaults.professional.getBool() ? 2 : 1
        SignupService.signup(name: name, email: email, password: pass, role: role) { (response, error) in
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                AppDelegate.shared.window?.makeToast("Sign up success!.")
            }
        }
    }
}
