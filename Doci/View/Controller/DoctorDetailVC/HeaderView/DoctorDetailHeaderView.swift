//
//  DoctorDetailHeaderView.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DoctorDetailHeaderView: GSKStretchyHeaderView {
    var dispose = DisposeBag()
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var backBtn: UIButton!


    class func newInstance() -> DoctorDetailHeaderView? {
        return DoctorDetailHeaderView.fromNib()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        starView?.rating = 0
        nameLbl?.text = nil
    }

    func config(data: Any?) {
        if let doctor = data as? Doctor {
            nameLbl?.text = doctor.name
            starView.rating = doctor.rate
            if doctor.avartUrl.count > 0 {
                avatarView?.sd_setImage(with: URL.init(string: doctor.avartUrl), completed: nil)
            }
        }
    }
}
