//
//  DoctorDetailVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DoctorDetailVC: BaseViewController {
    
    @IBOutlet weak var tbView: UITableView!
    var headerView: DoctorDetailHeaderView?

    let viewModel = DoctorDetailViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        self.automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 11.0, *) {
            self.tbView?.contentInsetAdjustmentBehavior = .never;
        }
        var contentInset = self.tbView?.contentInset ?? .zero
        if self.navigationController != nil {
            contentInset.top = 64
        }
        if self.tabBarController != nil {
            contentInset.bottom = 44
        }
        self.tbView?.contentInset = contentInset

        if let hd = DoctorDetailHeaderView.newInstance() {

            tbView?.addSubview(hd)


            hd.backBtn?.rx.tap.asObservable()
                .subscribe(onNext: { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }).disposed(by: hd.dispose)
            self.headerView = hd
        }
        tbView?.estimatedRowHeight = 100
        tbView?.rowHeight = UITableView.automaticDimension
        var xib = UINib(nibName: "DoctorDetailCell", bundle: nil)
        tbView?.register(xib, forCellReuseIdentifier: "DoctorDetailCell")
        xib = UINib(nibName: "DetailLocationCell", bundle: nil)
        tbView?.register(xib, forCellReuseIdentifier: "DetailLocationCell")
        tbView?.contentInset = UIEdgeInsets(top: -64, left: 0, bottom: 50, right: 0)
        tbView?.allowsSelection = false
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavigation(isShow: false)
        headerView?.expansionMode = .immediate
        headerView?.minimumContentHeight = 0
        headerView?.maximumContentHeight = 280
        headerView?.contentAnchor = .bottom
        loadDetail()
    }

    func loadDetail() {
        if let doctor = viewModel.doctor {
            WindowManager.shared.showProgressView()
            HCPDetailService.getHCPDetail(id: doctor.id) {[weak self] (response, error) in
                guard let `self` = self else {
                    return
                }
                WindowManager.shared.hideProgressView()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                } else if let doctor = response as? Doctor {
                    DispatchQueue.main.async {
                        self.viewModel.doctor = doctor
                        self.viewModel.prepare()
                        self.tbView?.reloadData()
                        self.headerView?.config(data: self.viewModel.doctor)
                    }
                }
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showNavigation(isShow: true)
    }

    func openMap(location: VLocation) {
        let app = UIApplication.shared
        let urlTxt = "comgooglemaps://?saddr=&daddr=\(location.coordinate.latitude),\(location.coordinate.longitude)"
        if let url = URL.init(string: urlTxt) {
            if app.canOpenURL(url) {
                app.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}

extension DoctorDetailVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sInfo = viewModel.sections[section]
        return sInfo.cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sInfo = viewModel.sections[indexPath.section]
        let rInfo = sInfo.cells[indexPath.row]
        if rInfo.reuseIdentifier.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: rInfo.reuseIdentifier, for: indexPath)
            if let dcell = cell as? DoctorDetailCell {
                dcell.config(data: rInfo.content)
            }
            if let lcell = cell as? DetailLocationCell {
                lcell.config(data: rInfo.content)
                lcell.openMapView?.rx.tap.asObservable()
                    .subscribe(onNext: { [weak self] in
                        if let location = (rInfo.content as? DetailContent)?.content as? VLocation {
                            self?.openMap(location: location)
                        }
                    }).disposed(by: lcell.dispose)
            }
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let hd = view as? UITableViewHeaderFooterView {
            hd.contentView.backgroundColor = .clear
            hd.backgroundView?.backgroundColor = .clear
        }
    }

    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let hd = view as? UITableViewHeaderFooterView {
            hd.contentView.backgroundColor = .clear
            hd.backgroundView?.backgroundColor = .clear
        }
    }
}
