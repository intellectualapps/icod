//
//  Doctor.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

enum Gender: String {
    case None = ""
    case Male = "Male"
    case Female = "Female"
}

struct DetailContent {
    var title = ""
    var content: Any?
}

enum Role: Int {
    case user = 1
    case hcp = 2
}

class Doctor: VModel {
    var name = ""
    var id = 0
    var role = Role.user
    var position = ""
    var gender = Gender.Male
    var rate: Double = 0
    var distance = 1000 // metters
    var qualification = "MBBS"
    var languages = ""
    var experience = ""
    var schedule = "Sundays: 3:00 PM - 3:24 PM, \n Monday: 3:00 PM - 3:24 PM, \n Tuesday: 3:00 PM - 3:24 PM, \n Wednessday: 3:00 PM - 3:24 PM, \n Thursday: 3:00 PM - 3:24 PM, \n Friday: 3:00 PM - 3:24 PM, \n Saturday: 3:00 PM - 3:24 PM"
    var service = "General medical consiltation ($5000)"
    var address = "Abuja, Negiria"
    var location = VLocation(lat: 9.056997, lng: 7.383368)
    var avartUrl = ""
    var largeImageUrl = ""
    var verified = false

    var ailments_ids = ""
    var verifyMessage = ""
    var bank_account = ""
    var qualification_id = 0
    var qualification_year = 0
    var qualification_certificate = ""
    var mdcn_number = ""
    var ailments = ""
    
    override init() {
        super.init()
        self.location.fullAddress = "Abuja, Nigeria"
    }

    func toDetailContents() -> [DetailContent] {
        var results = [DetailContent]()
        results.append(DetailContent(title: "Specialty", content: position))
        results.append(DetailContent(title: "Qualification", content: qualification))
        results.append(DetailContent(title: "Experience", content: "\(experience)"))
        results.append(DetailContent(title: "Schedule", content: schedule))
        results.append(DetailContent(title: "Services", content: service))
        results.append(DetailContent(title: "Location", content: location))
        return results
    }

    override func parseDict(dict: [String : Any]) {
        let gender = dict.stringOrEmptyForKey(key: "gender")
        self.gender = gender.lowercased() == "male" ? Gender.Male : .Female
        id = dict.intForKey(key: "id")
        languages = dict.stringOrEmptyForKey(key: "languages")
        location.coordinate.latitude = dict.doubleForKey(key: "latitude")
        location.coordinate.longitude = dict.doubleForKey(key: "longitude")
        location.fullAddress = dict.stringOrEmptyForKey(key: "address")
        name = dict.stringOrEmptyForKey(key: "name")
        avartUrl = dict.stringOrEmptyForKey(key: "photo_url_small")
        largeImageUrl = dict.stringOrEmptyForKey(key: "photo_url_small")
        rate = dict.doubleForKey(key: "rating")
        position = dict.stringOrEmptyForKey(key: "specialty")
        verified = dict.boolForKey(key: "verification_status")

        //from detail
        experience = dict.stringOrEmptyForKey(key: "experience")
        ailments_ids = dict.stringOrEmptyForKey(key: "ailments_ids")
        verifyMessage = dict.stringOrEmptyForKey(key: "verifyMessage")
        bank_account = dict.stringOrEmptyForKey(key: "bank_account")
        qualification_id = dict.intForKey(key: "qualification_id")
        qualification_year = dict.intForKey(key: "qualification_year")
        qualification_certificate = dict.stringOrEmptyForKey(key: "qualification_year")
        mdcn_number = dict.stringOrEmptyForKey(key: "mdcn_number")
        ailments = dict.stringOrEmptyForKey(key: "ailments")
    }
}
