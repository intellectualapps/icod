//
//  User.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 5/30/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

struct  User {
    var name = ""
    var email = ""
    var password = ""
    var token = ""
    var avatarPath = ""
    var birthday = ""
    var isEmailVerified = false
    var id = -1
    var role = -1
    var address = ""
    //Cầu Giấy, Hanoi, Vietnam
    var location: VLocation?

    init(name: String,
         email: String,
         password: String,
         token: String) {
        self.name = name
        self.email = email
        self.password = password
        self.token = token
    }

    /*
     Invoked when login or signup success
     */
    static func userFromDict(dict: [String: Any]) -> User? {
        if let userDict = dict["user"] as? [String: Any] {
            var user = User(name: "", email: "", password: "", token: "")
            user.name = userDict.stringOrEmptyForKey(key: "name")
            user.id = userDict.intForKey(key: "id")
            user.role = userDict.intForKey(key: "role")
            user.isEmailVerified = userDict.boolForKey(key: "email_verified_at")
            user.email = userDict.stringOrEmptyForKey(key: "email")
            user.birthday = userDict.stringOrEmptyForKey(key: "birthday")
            user.avatarPath = userDict.stringOrEmptyForKey(key: "avatar")
            user.address = userDict.stringOrEmptyForKey(key: "address")
            user.token = dict.stringOrEmptyForKey(key: "access_token")
            AppDelegate.shared.loggedUser.value = user
            Defaults.loggedEmail.set(value: user.email)
            return user
        }
        return nil
    }

    func save() {
        
    }
}
