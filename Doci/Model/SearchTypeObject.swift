//
//  SearchTypeObject.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/1/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class SearchTypeObject: VModel {
    var id = -1
    var name = ""

    override func parseDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        name = dict.stringOrEmptyForKey(key: "name")
    }
}
