//
//  Location.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/21/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import CoreLocation

class VLocation: VModel {
    var coordinate = kCLLocationCoordinate2DInvalid
    var fullAddress = ""
    var formatedAddress = ""
    var city = ""
    var country = ""
    var countryCode = ""
    var state = ""
    var town = ""
    var name = ""
    var placeid = ""
    var searchResultHeight: CGFloat = 0
    var thorough = ""
    var area = ""

    class func defaultLocation() -> VLocation {
        let info = VLocation()
        info.coordinate.latitude = 6.500
        info.coordinate.longitude = 3.500
        info.fullAddress = "Mangun, Nigeria"
        return info
    }

    convenience init(lat: Double, lng: Double) {
        self.init()
        coordinate.latitude = lat
        coordinate.longitude = lng
    }

    func clone() -> VLocation {
        let info = VLocation()
        remoteId = self.remoteId
        localIdentifier = self.localIdentifier
        info.coordinate = self.coordinate
        info.fullAddress = self.fullAddress
        info.formatedAddress = self.formatedAddress
        info.city = self.city
        info.country = self.country
        info.countryCode = self.countryCode
        info.state = self.state
        info.town = self.town
        info.name = self.name
        info.placeid = self.placeid
        info.searchResultHeight = self.searchResultHeight
        info.thorough = self.thorough
        info.area = self.area
        return info
    }

    var identifier: String {
        get {
            return coordinate.toIdentifier()
        }
    }

    /**
     * from api
     */
    override func parseDict(dict: [String : Any]) {
        coordinate.latitude = dict.doubleForKey(key: "latidue")
        coordinate.longitude = dict.doubleForKey(key: "longtidue")
        city = dict.stringOrEmptyForKey(key: "city")
        country = dict.stringOrEmptyForKey(key: "country")
        fullAddress = dict.stringOrEmptyForKey(key: "address")
        name = dict.stringOrEmptyForKey(key: "name")
        remoteId = coordinate.toIdentifier()
        localIdentifier = coordinate.toIdentifier()
    }

    func isValid() -> Bool {
        return displayAddress().trim().count > 0
    }

    func displayAddress() -> String {
        if name.count > 0 {
            return name
        }
        if city.count > 0 {
            return city
        }

        return fullAddress
    }

    func calculateHeight(maxWidth: CGFloat) {
        
    }

    class func fromGeoCodeInfoDict(_ infoDict: NSDictionary) -> VLocation {
        let info = VLocation()
        info.city = (infoDict["city"] as? String) ?? ""
        info.town = (infoDict["town"] as? String) ?? ""
        info.name = (infoDict["name"] as? String) ?? ""
        info.formatedAddress = (infoDict["formattedAddress"] as? String) ?? ""
        var latidue : Double = 0
        var longtidue: Double = 0
        if let value = infoDict.object(forKey: "latitude"){
            latidue = (value as AnyObject).doubleValue
        }
        if let value = infoDict.object(forKey: "longitude"){
            longtidue = (value as AnyObject).doubleValue
        }
        info.coordinate = CLLocationCoordinate2D(latitude: latidue, longitude: longtidue)
        info.remoteId = info.coordinate.toIdentifier()
        info.localIdentifier = info.remoteId
        return info
    }
}
