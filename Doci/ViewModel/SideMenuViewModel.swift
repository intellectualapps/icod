//
//  SideMenuViewModel.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/28/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

enum SideMenuCellID: Int {
    case findDoctors = 0
    case tips
    case destinations
    case reminders
    case logout
}

struct SideMenuCellInfo {
    var title = ""
    var image = ""
    var id = 0
}

class SideMenuViewModel: NSObject {
    var sections = [VSection]()

    override init() {
        super.init()
        prepare()
    }

    func prepare() {
        sections.removeAll()
        let data = [
            SideMenuCellInfo(title: "menu.finddoctor".localized(), image: "", id: SideMenuCellID.findDoctors.rawValue),
            SideMenuCellInfo(title: "menu.profile".localized(), image: "", id: SideMenuCellID.tips.rawValue),
            SideMenuCellInfo(title: "menu.mybooking".localized(), image: "", id: SideMenuCellID.destinations.rawValue),
            SideMenuCellInfo(title: "menu.logout".localized(), image: "", id: SideMenuCellID.logout.rawValue)
        ]
        let s = VSection(reuseIdenfier: "", height: 80, sectionId: -1, contentObj: nil) { () -> ([VCellInfo]) in
            var cells = [VCellInfo]()
            for info in data {
                let cell = VCellInfo(reuseIdentifier: "SideMenuCell", id: info.id, height: 50, content:info )
                cells.append(cell)
            }
            return cells
        }
        self.sections.append(s)
    }
}
