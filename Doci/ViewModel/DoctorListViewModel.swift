//
//  DoctorListViewModel.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DoctorListViewModel: NSObject {
    var sections = [VSection]()
    var lastGenderFilterOption = 0
    var doctors = [Doctor]()
    var originListDoctors = [Doctor]()

    override init() {
        super.init()
    }

    func setListDoctors(doctors: [Doctor]) {
        self.doctors = doctors
        self.originListDoctors = doctors
        prepare()
    }
    
    func fake() {
        doctors = ContentManager.shared.doctors
        prepare()
    }

    func filter(gender: Int) {
        if gender == 0 {
            doctors = self.originListDoctors
        } else {
            let gender = gender == 1 ? Gender.Male : Gender.Female
            doctors = self.originListDoctors.filter({$0.gender == gender})
        }
        prepare()
    }

    func prepare() {
        sections.removeAll()
        let sInfo = VSection(reuseIdenfier: "", height: 0.1, sectionId: -1, contentObj: nil) { () -> ([VCellInfo]) in
            var cells = [VCellInfo]()
            doctors.forEach({ (doctor) in
                let info = VCellInfo(reuseIdentifier: "DoctorListCell", id: -1, height: 0, content: doctor)
                cells.append(info)
            })
            return cells
        }
        sections.append(sInfo)
    }
}
