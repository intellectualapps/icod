//
//  SearchDoctorViewModel.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/1/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class SearchDoctorViewModel: DoctorListViewModel {
    var searchType = SearchTypeObject()

    func searchHCP(completion: @escaping NetworkServiceCompletion) {
        let type = searchType.isKind(of: Ailments.self) ? 1 : 2
        SearchHCPService.searchHCP(search_type: type, search_id: searchType.id, offset: 0) {[weak self] (response, error) in
            if let datas = response as? [Doctor] {
                self?.setListDoctors(doctors: datas)
            }
            completion(response, error)
        }
    }
}
